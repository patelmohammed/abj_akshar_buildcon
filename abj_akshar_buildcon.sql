-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 23, 2021 at 09:08 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abj_akshar_buildcon`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

DROP TABLE IF EXISTS `about_us`;
CREATE TABLE IF NOT EXISTS `about_us` (
  `about_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `about_us_title_1` text NOT NULL,
  `about_us_title_2` text NOT NULL,
  `about_us_desc_1` text NOT NULL,
  `about_us_desc_2` text NOT NULL,
  `about_us_alt_sub_icon_1` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_title_1` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_desc_1` text,
  `about_us_alt_sub_icon_2` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_title_2` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_desc_2` text,
  `about_us_alt_sub_icon_3` varchar(250) NOT NULL,
  `about_us_alt_sub_title_3` varchar(250) NOT NULL,
  `about_us_alt_sub_desc_3` text NOT NULL,
  `about_us_alt_sub_icon_4` varchar(250) NOT NULL,
  `about_us_alt_sub_title_4` varchar(250) NOT NULL,
  `about_us_alt_sub_desc_4` text NOT NULL,
  `about_us_image_1` text NOT NULL,
  `about_us_image_2` text NOT NULL,
  `image_title` varchar(250) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`about_us_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`about_us_id`, `about_us_title_1`, `about_us_title_2`, `about_us_desc_1`, `about_us_desc_2`, `about_us_alt_sub_icon_1`, `about_us_alt_sub_title_1`, `about_us_alt_sub_desc_1`, `about_us_alt_sub_icon_2`, `about_us_alt_sub_title_2`, `about_us_alt_sub_desc_2`, `about_us_alt_sub_icon_3`, `about_us_alt_sub_title_3`, `about_us_alt_sub_desc_3`, `about_us_alt_sub_icon_4`, `about_us_alt_sub_title_4`, `about_us_alt_sub_desc_4`, `about_us_image_1`, `about_us_image_2`, `image_title`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'WELCOME TO Weborative', 'About Us <br> Our Company.', 'We craft beautifully useful marketing and digital products that grow <strong class=\"thm-clr d-block\">- Our Blubuild.</strong>', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim admini veniam, quis nostru.', 'flaticon-car-parts', 'Automotive <br> Manufacturing', 'Eiusmod tempor incididunt ut labore et dolore magna.', 'flaticon-helmet', 'Owner\'s <br> Representation', 'Eiusmod tempor incididunt ut labore et dolore magna.', 'flaticon-builder', 'Automotive <br> Manufacturing', 'Eiusmod tempor incididunt ut labore et dolore magna.', 'flaticon-architect', 'Owner\'s <br> Representation', 'Eiusmod tempor incididunt ut labore et dolore magna.', 'assets/images/resources/about-image1.jpg', 'assets/images/resources/about-image2.jpg', '18<sup>+</sup><i class=\"d-block\">Experience</i>', NULL, NULL, NULL, 1, '::1', '2021-02-21 17:13:50', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `all_cities`
--

DROP TABLE IF EXISTS `all_cities`;
CREATE TABLE IF NOT EXISTS `all_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` text,
  `city_code` text,
  `state_code` text,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=756 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_cities`
--

INSERT INTO `all_cities` (`id`, `city_name`, `city_code`, `state_code`, `del_status`) VALUES
(1, 'Alipur', '101', '1', 'Live'),
(2, 'Andaman Island', '102', '1', 'Live'),
(3, 'Anderson Island', '103', '1', 'Live'),
(4, 'Arainj-Laka-Punga', '104', '1', 'Live'),
(5, 'Austinabad', '105', '1', 'Live'),
(6, 'Bamboo Flat', '106', '1', 'Live'),
(7, 'Barren Island', '107', '1', 'Live'),
(8, 'Beadonabad', '108', '1', 'Live'),
(9, 'Betapur', '109', '1', 'Live'),
(10, 'Bindraban', '110', '1', 'Live'),
(11, 'Bonington', '111', '1', 'Live'),
(12, 'Brookesabad', '112', '1', 'Live'),
(13, 'Cadell Point', '113', '1', 'Live'),
(14, 'Calicut', '114', '1', 'Live'),
(15, 'Chetamale', '115', '1', 'Live'),
(16, 'Cinque Islands', '116', '1', 'Live'),
(17, 'Defence Island', '117', '1', 'Live'),
(18, 'Digilpur', '118', '1', 'Live'),
(19, 'Dolyganj', '119', '1', 'Live'),
(20, 'Flat Island', '120', '1', 'Live'),
(21, 'Geinyale', '121', '1', 'Live'),
(22, 'Great Coco Island', '122', '1', 'Live'),
(23, 'Haddo', '123', '1', 'Live'),
(24, 'Havelock Island', '124', '1', 'Live'),
(25, 'Henry Lawrence Island', '125', '1', 'Live'),
(26, 'Herbertabad', '126', '1', 'Live'),
(27, 'Hobdaypur', '127', '1', 'Live'),
(28, 'Ilichar', '128', '1', 'Live'),
(29, 'Ingoie', '128', '1', 'Live'),
(30, 'Inteview Island', '130', '1', 'Live'),
(31, 'Jangli Ghat', '131', '1', 'Live'),
(32, 'Jhon Lawrence Island', '132', '1', 'Live'),
(33, 'Karen', '133', '1', 'Live'),
(34, 'Kartara', '134', '1', 'Live'),
(35, 'KYD Islannd', '135', '1', 'Live'),
(36, 'Landfall Island', '136', '1', 'Live'),
(37, 'Little Andmand', '137', '1', 'Live'),
(38, 'Little Coco Island', '138', '1', 'Live'),
(39, 'Long Island', '138', '1', 'Live'),
(40, 'Maimyo', '140', '1', 'Live'),
(41, 'Malappuram', '141', '1', 'Live'),
(42, 'Manglutan', '142', '1', 'Live'),
(43, 'Manpur', '143', '1', 'Live'),
(44, 'Mitha Khari', '144', '1', 'Live'),
(45, 'Neill Island', '145', '1', 'Live'),
(46, 'Nicobar Island', '146', '1', 'Live'),
(47, 'North Brother Island', '147', '1', 'Live'),
(48, 'North Passage Island', '148', '1', 'Live'),
(49, 'North Sentinel Island', '149', '1', 'Live'),
(50, 'Nothen Reef Island', '150', '1', 'Live'),
(51, 'Outram Island', '151', '1', 'Live'),
(52, 'Pahlagaon', '152', '1', 'Live'),
(53, 'Palalankwe', '153', '1', 'Live'),
(54, 'Passage Island', '154', '1', 'Live'),
(55, 'Phaiapong', '155', '1', 'Live'),
(56, 'Phoenix Island', '156', '1', 'Live'),
(57, 'Port Blair', '157', '1', 'Live'),
(58, 'Preparis Island', '158', '1', 'Live'),
(59, 'Protheroepur', '159', '1', 'Live'),
(60, 'Rangachang', '160', '1', 'Live'),
(61, 'Rongat', '161', '1', 'Live'),
(62, 'Rutland Island', '162', '1', 'Live'),
(63, 'Sabari', '163', '1', 'Live'),
(64, 'Saddle Peak', '164', '1', 'Live'),
(65, 'Shadipur', '165', '1', 'Live'),
(66, 'Smith Island', '166', '1', 'Live'),
(67, 'Sound Island', '167', '1', 'Live'),
(68, 'South Sentinel Island', '168', '1', 'Live'),
(69, 'Spike Island', '169', '1', 'Live'),
(70, 'Tarmugli Island', '170', '1', 'Live'),
(71, 'Taylerabad', '171', '1', 'Live'),
(72, 'Titaije', '172', '1', 'Live'),
(73, 'Toibalawe', '173', '1', 'Live'),
(74, 'Tusonabad', '174', '1', 'Live'),
(75, 'West Island', '175', '1', 'Live'),
(76, 'Wimberleyganj', '176', '1', 'Live'),
(77, 'Yadita', '177', '1', 'Live'),
(78, 'Adilabad', '201', '2', 'Live'),
(79, 'Anantapur', '201', '2', 'Live'),
(80, 'Chittoor', '203', '2', 'Live'),
(81, 'Cuddapah', '204', '2', 'Live'),
(82, 'East Godavari', '205', '2', 'Live'),
(83, 'Guntur', '206', '2', 'Live'),
(84, 'Hyderabad', '207', '2', 'Live'),
(85, 'Karimnagar', '208', '2', 'Live'),
(86, 'Khammam', '209', '2', 'Live'),
(87, 'Krishna', '210', '2', 'Live'),
(88, 'Kurnool', '211', '2', 'Live'),
(89, 'Mahabubnagar', '212', '2', 'Live'),
(90, 'Medak', '213', '2', 'Live'),
(91, 'Nalgonda', '214', '2', 'Live'),
(92, 'Nellore', '215', '2', 'Live'),
(93, 'Nizamabad', '216', '2', 'Live'),
(94, 'Prakasam', '217', '2', 'Live'),
(95, 'Rangareddy', '218', '2', 'Live'),
(96, 'Srikakulam', '219', '2', 'Live'),
(97, 'Visakhapatnam', '220', '2', 'Live'),
(98, 'Vizianagaram', '221', '2', 'Live'),
(99, 'Warangal', '222', '2', 'Live'),
(100, 'West Godavari', '223', '2', 'Live'),
(101, 'Anjaw', '301', '3', 'Live'),
(102, 'Changlang', '302', '3', 'Live'),
(103, 'Dibang Valley', '303', '3', 'Live'),
(104, 'East Kameng', '304', '3', 'Live'),
(105, 'East Siang', '305', '3', 'Live'),
(106, 'Itanagar', '306', '3', 'Live'),
(107, 'Kurung Kumey', '307', '3', 'Live'),
(108, 'Lohit', '308', '3', 'Live'),
(109, 'Lower Dibang Valley', '309', '3', 'Live'),
(110, 'Lower Subansiri', '310', '3', 'Live'),
(111, 'Papum Pare', '311', '3', 'Live'),
(112, 'Tawang', '312', '3', 'Live'),
(113, 'Tirap', '313', '3', 'Live'),
(114, 'Upper Siang', '314', '3', 'Live'),
(115, 'Upper Subansiri', '315', '3', 'Live'),
(116, 'West Kameng', '316', '3', 'Live'),
(117, 'West Siang', '317', '3', 'Live'),
(118, 'Barpeta', '401', '4', 'Live'),
(119, 'Bongaigaon', '402', '4', 'Live'),
(120, 'Cachar', '403', '4', 'Live'),
(121, 'Darrang', '404', '4', 'Live'),
(122, 'Dhemaji', '405', '4', 'Live'),
(123, 'Dhubri', '406', '4', 'Live'),
(124, 'Dibrugarh', '407', '4', 'Live'),
(125, 'Goalpara', '408', '4', 'Live'),
(126, 'Golaghat', '409', '4', 'Live'),
(127, 'Guwahati', '410', '4', 'Live'),
(128, 'Hailakandi', '411', '4', 'Live'),
(129, 'Jorhat', '412', '4', 'Live'),
(130, 'Kamrup', '413', '4', 'Live'),
(131, 'Karbi Anglong', '414', '4', 'Live'),
(132, 'Karimganj', '415', '4', 'Live'),
(133, 'Kokrajhar', '416', '4', 'Live'),
(134, 'Lakhimpur', '417', '4', 'Live'),
(135, 'Marigaon', '418', '4', 'Live'),
(136, 'Nagaon', '419', '4', 'Live'),
(137, 'Nalbari', '420', '4', 'Live'),
(138, 'North Cachar Hills', '421', '4', 'Live'),
(139, 'Silchar', '422', '4', 'Live'),
(140, 'Sivasagar', '423', '4', 'Live'),
(141, 'Sonitpur', '424', '4', 'Live'),
(142, 'Tinsukia', '425', '4', 'Live'),
(143, 'Udalguri', '426', '4', 'Live'),
(144, 'Araria', '501', '5', 'Live'),
(145, 'Aurangabad', '502', '5', 'Live'),
(146, 'Banka', '503', '5', 'Live'),
(147, 'Begusarai', '504', '5', 'Live'),
(148, 'Bhagalpur', '505', '5', 'Live'),
(149, 'Bhojpur', '506', '5', 'Live'),
(150, 'Buxar', '507', '5', 'Live'),
(151, 'Darbhanga', '508', '5', 'Live'),
(152, 'East Champaran', '509', '5', 'Live'),
(153, 'Gaya', '510', '5', 'Live'),
(154, 'Gopalganj', '511', '5', 'Live'),
(155, 'Jamshedpur', '512', '5', 'Live'),
(156, 'Jamui', '513', '5', 'Live'),
(157, 'Jehanabad', '514', '5', 'Live'),
(158, 'Kaimur (Bhabua)', '515', '5', 'Live'),
(159, 'Katihar', '516', '5', 'Live'),
(160, 'Khagaria', '517', '5', 'Live'),
(161, 'Kishanganj', '518', '5', 'Live'),
(162, 'Lakhisarai', '519', '5', 'Live'),
(163, 'Madhepura', '520', '5', 'Live'),
(164, 'Madhubani', '521', '5', 'Live'),
(165, 'Munger', '522', '5', 'Live'),
(166, 'Muzaffarpur', '523', '5', 'Live'),
(167, 'Nalanda', '524', '5', 'Live'),
(168, 'Nawada', '525', '5', 'Live'),
(169, 'Patna', '526', '5', 'Live'),
(170, 'Purnia', '527', '5', 'Live'),
(171, 'Rohtas', '528', '5', 'Live'),
(172, 'Saharsa', '529', '5', 'Live'),
(173, 'Samastipur', '530', '5', 'Live'),
(174, 'Saran', '531', '5', 'Live'),
(175, 'Sheikhpura', '532', '5', 'Live'),
(176, 'Sheohar', '533', '5', 'Live'),
(177, 'Sitamarhi', '534', '5', 'Live'),
(178, 'Siwan', '535', '5', 'Live'),
(179, 'Supaul', '536', '5', 'Live'),
(180, 'Vaishali', '537', '5', 'Live'),
(181, 'West Champaran', '538', '5', 'Live'),
(182, 'Chandigarh', '601', '6', 'Live'),
(183, 'Mani Marja', '602', '6', 'Live'),
(184, 'Bastar', '701', '7', 'Live'),
(185, 'Bhilai', '702', '7', 'Live'),
(186, 'Bijapur', '703', '7', 'Live'),
(187, 'Bilaspur', '704', '7', 'Live'),
(188, 'Dhamtari', '705', '7', 'Live'),
(189, 'Durg', '706', '7', 'Live'),
(190, 'Janjgir-Champa', '707', '7', 'Live'),
(191, 'Jashpur', '708', '7', 'Live'),
(192, 'Kabirdham-Kawardha', '709', '7', 'Live'),
(193, 'Korba', '710', '7', 'Live'),
(194, 'Korea', '711', '7', 'Live'),
(195, 'Mahasamund', '712', '7', 'Live'),
(196, 'Narayanpur', '713', '7', 'Live'),
(197, 'Norh Bastar-Kanker', '714', '7', 'Live'),
(198, 'Raigarh', '715', '7', 'Live'),
(199, 'Raipur', '716', '7', 'Live'),
(200, 'Rajnandgaon', '717', '7', 'Live'),
(201, 'South Bastar-Dantewada', '718', '7', 'Live'),
(202, 'Surguja', '719', '7', 'Live'),
(203, 'Amal', '801', '8', 'Live'),
(204, 'Amli', '802', '8', 'Live'),
(205, 'Bedpa', '803', '8', 'Live'),
(206, 'Chikhli', '804', '8', 'Live'),
(207, 'Dadra & Nagar Haveli', '805', '8', 'Live'),
(208, 'Dahikhed', '806', '8', 'Live'),
(209, 'Dolara', '807', '8', 'Live'),
(210, 'Galonda', '808', '8', 'Live'),
(211, 'Kanadi', '809', '8', 'Live'),
(212, 'Karchond', '810', '8', 'Live'),
(213, 'Khadoli', '811', '8', 'Live'),
(214, 'Kharadpada', '812', '8', 'Live'),
(215, 'Kherabari', '813', '8', 'Live'),
(216, 'Kherdi', '814', '8', 'Live'),
(217, 'Kothar', '815', '8', 'Live'),
(218, 'Luari', '816', '8', 'Live'),
(219, 'Mashat', '817', '8', 'Live'),
(220, 'Rakholi', '818', '8', 'Live'),
(221, 'Rudana', '819', '8', 'Live'),
(222, 'Saili', '820', '8', 'Live'),
(223, 'Sili', '821', '8', 'Live'),
(224, 'Silvassa', '822', '8', 'Live'),
(225, 'Sindavni', '823', '8', 'Live'),
(226, 'Udva', '824', '8', 'Live'),
(227, 'Umbarkoi', '825', '8', 'Live'),
(228, 'Vansda', '826', '8', 'Live'),
(229, 'Vasona', '827', '8', 'Live'),
(230, 'Velugam', '828', '8', 'Live'),
(231, 'Brancavare', '901', '9', 'Live'),
(232, 'Dagasi', '902', '9', 'Live'),
(233, 'Daman', '903', '9', 'Live'),
(234, 'Diu', '904', '9', 'Live'),
(235, 'Magarvara', '905', '9', 'Live'),
(236, 'Nagwa', '906', '9', 'Live'),
(237, 'Pariali', '907', '9', 'Live'),
(238, 'Passo Covo', '908', '9', 'Live'),
(239, 'Central Delhi', '1001', '10', 'Live'),
(240, 'East Delhi', '1002', '10', 'Live'),
(241, 'New Delhi', '1003', '10', 'Live'),
(242, 'North Delhi', '1004', '10', 'Live'),
(243, 'North East Delhi', '1005', '10', 'Live'),
(244, 'North West Delhi', '1006', '10', 'Live'),
(245, 'Old Delhi', '1007', '10', 'Live'),
(246, 'South Delhi', '1008', '10', 'Live'),
(247, 'South West Delhi', '1009', '10', 'Live'),
(248, 'West Delhi', '1010', '10', 'Live'),
(249, 'Canacona', '1101', '11', 'Live'),
(250, 'Candolim', '1102', '11', 'Live'),
(251, 'Chinchinim', '1103', '11', 'Live'),
(252, 'Cortalim', '1104', '11', 'Live'),
(253, 'Goa', '1105', '11', 'Live'),
(254, 'Jua', '1106', '11', 'Live'),
(255, 'Madgaon', '1107', '11', 'Live'),
(256, 'Mahem', '1108', '11', 'Live'),
(257, 'Mapuca', '1109', '11', 'Live'),
(258, 'Marmagao', '1110', '11', 'Live'),
(259, 'Panji', '1111', '11', 'Live'),
(260, 'Ponda', '1112', '11', 'Live'),
(261, 'Sanvordem', '1113', '11', 'Live'),
(262, 'Terekhol', '1114', '11', 'Live'),
(263, 'Ahmedabad', '1201', '12', 'Live'),
(264, 'Amreli', '1202', '12', 'Live'),
(265, 'Anand', '1203', '12', 'Live'),
(266, 'Banaskantha', '1204', '12', 'Live'),
(267, 'Baroda', '1205', '12', 'Live'),
(268, 'Bharuch', '1206', '12', 'Live'),
(269, 'Bhavnagar', '1207', '12', 'Live'),
(270, 'Dahod', '1208', '12', 'Live'),
(271, 'Dang', '1209', '12', 'Live'),
(272, 'Dwarka', '1210', '12', 'Live'),
(273, 'Gandhinagar', '1211', '12', 'Live'),
(274, 'Jamnagar', '1212', '12', 'Live'),
(275, 'Junagadh', '1213', '12', 'Live'),
(276, 'Kheda', '1214', '12', 'Live'),
(277, 'Kutch', '1215', '12', 'Live'),
(278, 'Mehsana', '1216', '12', 'Live'),
(279, 'Nadiad', '1217', '12', 'Live'),
(280, 'Narmada', '1218', '12', 'Live'),
(281, 'Navsari', '1219', '12', 'Live'),
(282, 'Panchmahals', '1220', '12', 'Live'),
(283, 'Patan', '1221', '12', 'Live'),
(284, 'Porbandar', '1222', '12', 'Live'),
(285, 'Rajkot', '1223', '12', 'Live'),
(286, 'Sabarkantha', '1224', '12', 'Live'),
(287, 'Surat', '1225', '12', 'Live'),
(288, 'Surendranagar', '1226', '12', 'Live'),
(289, 'Vadodara', '1227', '12', 'Live'),
(290, 'Valsad', '1228', '12', 'Live'),
(291, 'Vapi', '1229', '12', 'Live'),
(292, 'Ambala', '1301', '13', 'Live'),
(293, 'Bhiwani', '1302', '13', 'Live'),
(294, 'Faridabad', '1303', '13', 'Live'),
(295, 'Fatehabad', '1304', '13', 'Live'),
(296, 'Gurgaon', '1305', '13', 'Live'),
(297, 'Hisar', '1306', '13', 'Live'),
(298, 'Jhajjar', '1307', '13', 'Live'),
(299, 'Jind', '1308', '13', 'Live'),
(300, 'Kaithal', '1309', '13', 'Live'),
(301, 'Karnal', '1310', '13', 'Live'),
(302, 'Kurukshetra', '1311', '13', 'Live'),
(303, 'Mahendragarh', '1312', '13', 'Live'),
(304, 'Mewat', '1313', '13', 'Live'),
(305, 'Panchkula', '1314', '13', 'Live'),
(306, 'Panipat', '1315', '13', 'Live'),
(307, 'Rewari', '1316', '13', 'Live'),
(308, 'Rohtak', '1317', '13', 'Live'),
(309, 'Sirsa', '1318', '13', 'Live'),
(310, 'Sonipat', '1319', '13', 'Live'),
(311, 'Yamunanagar', '1320', '13', 'Live'),
(312, 'Bilaspur', '1401', '14', 'Live'),
(313, 'Chamba', '1402', '14', 'Live'),
(314, 'Dalhousie', '1403', '14', 'Live'),
(315, 'Hamirpur', '1404', '14', 'Live'),
(316, 'Kangra', '1405', '14', 'Live'),
(317, 'Kinnaur', '1406', '14', 'Live'),
(318, 'Kullu', '1407', '14', 'Live'),
(319, 'Lahaul & Spiti', '1408', '14', 'Live'),
(320, 'Mandi', '1409', '14', 'Live'),
(321, 'Shimla', '1410', '14', 'Live'),
(322, 'Sirmaur', '1411', '14', 'Live'),
(323, 'Solan', '1412', '14', 'Live'),
(324, 'Una', '1413', '14', 'Live'),
(325, 'Anantnag', '1501', '15', 'Live'),
(326, 'Baramulla', '1502', '15', 'Live'),
(327, 'Budgam', '1503', '15', 'Live'),
(328, 'Doda', '1504', '15', 'Live'),
(329, 'Jammu', '1505', '15', 'Live'),
(330, 'Kargil', '1506', '15', 'Live'),
(331, 'Kathua', '1507', '15', 'Live'),
(332, 'Kupwara', '1508', '15', 'Live'),
(333, 'Leh', '1509', '15', 'Live'),
(334, 'Poonch', '1510', '15', 'Live'),
(335, 'Pulwama', '1511', '15', 'Live'),
(336, 'Rajauri', '1512', '15', 'Live'),
(337, 'Srinagar', '1513', '15', 'Live'),
(338, 'Udhampur', '1514', '15', 'Live'),
(339, 'Bokaro', '1601', '16', 'Live'),
(340, 'Chatra', '1602', '16', 'Live'),
(341, 'Deoghar', '1603', '16', 'Live'),
(342, 'Dhanbad', '1604', '16', 'Live'),
(343, 'Dumka', '1605', '16', 'Live'),
(344, 'East Singhbhum', '1606', '16', 'Live'),
(345, 'Garhwa', '1607', '16', 'Live'),
(346, 'Giridih', '1608', '16', 'Live'),
(347, 'Godda', '1609', '16', 'Live'),
(348, 'Gumla', '1610', '16', 'Live'),
(349, 'Hazaribag', '1611', '16', 'Live'),
(350, 'Jamtara', '1612', '16', 'Live'),
(351, 'Koderma', '1613', '16', 'Live'),
(352, 'Latehar', '1614', '16', 'Live'),
(353, 'Lohardaga', '1615', '16', 'Live'),
(354, 'Pakur', '1616', '16', 'Live'),
(355, 'Palamu', '1617', '16', 'Live'),
(356, 'Ranchi', '1618', '16', 'Live'),
(357, 'Sahibganj', '1619', '16', 'Live'),
(358, 'Seraikela', '1620', '16', 'Live'),
(359, 'Simdega', '1621', '16', 'Live'),
(360, 'West Singhbhum', '1622', '16', 'Live'),
(361, 'Bagalkot', '1701', '17', 'Live'),
(362, 'Bangalore', '1702', '17', 'Live'),
(363, 'Bangalore Rural', '1703', '17', 'Live'),
(364, 'Belgaum', '1704', '17', 'Live'),
(365, 'Bellary', '1705', '17', 'Live'),
(366, 'Bhatkal', '1706', '17', 'Live'),
(367, 'Bidar', '1707', '17', 'Live'),
(368, 'Bijapur', '1708', '17', 'Live'),
(369, 'Chamrajnagar', '1709', '17', 'Live'),
(370, 'Chickmagalur', '1710', '17', 'Live'),
(371, 'Chikballapur', '1711', '17', 'Live'),
(372, 'Chitradurga', '1712', '17', 'Live'),
(373, 'Dakshina Kannada', '1713', '17', 'Live'),
(374, 'Davanagere', '1714', '17', 'Live'),
(375, 'Dharwad', '1715', '17', 'Live'),
(376, 'Gadag', '1716', '17', 'Live'),
(377, 'Gulbarga', '1717', '17', 'Live'),
(378, 'Hampi', '1718', '17', 'Live'),
(379, 'Hassan', '1719', '17', 'Live'),
(380, 'Haveri', '1720', '17', 'Live'),
(381, 'Hospet', '1721', '17', 'Live'),
(382, 'Karwar', '1722', '17', 'Live'),
(383, 'Kodagu', '1723', '17', 'Live'),
(384, 'Kolar', '1724', '17', 'Live'),
(385, 'Koppal', '1725', '17', 'Live'),
(386, 'Madikeri', '1726', '17', 'Live'),
(387, 'Mandya', '1727', '17', 'Live'),
(388, 'Mangalore', '1728', '17', 'Live'),
(389, 'Manipal', '1729', '17', 'Live'),
(390, 'Mysore', '1730', '17', 'Live'),
(391, 'Raichur', '1731', '17', 'Live'),
(392, 'Shimoga', '1732', '17', 'Live'),
(393, 'Sirsi', '1733', '17', 'Live'),
(394, 'Sringeri', '1734', '17', 'Live'),
(395, 'Srirangapatna', '1735', '17', 'Live'),
(396, 'Tumkur', '1736', '17', 'Live'),
(397, 'Udupi', '1737', '17', 'Live'),
(398, 'Uttara Kannada', '1738', '17', 'Live'),
(399, 'Alappuzha', '1801', '18', 'Live'),
(400, 'Alleppey', '1802', '18', 'Live'),
(401, 'Alwaye', '1803', '18', 'Live'),
(402, 'Ernakulam', '1804', '18', 'Live'),
(403, 'Idukki', '1805', '18', 'Live'),
(404, 'Kannur', '1806', '18', 'Live'),
(405, 'Kasargod', '1807', '18', 'Live'),
(406, 'Kochi', '1808', '18', 'Live'),
(407, 'Kollam', '1809', '18', 'Live'),
(408, 'Kottayam', '1810', '18', 'Live'),
(409, 'Kovalam', '1811', '18', 'Live'),
(410, 'Kozhikode', '1812', '18', 'Live'),
(411, 'Malappuram', '1813', '18', 'Live'),
(412, 'Palakkad', '1814', '18', 'Live'),
(413, 'Pathanamthitta', '1815', '18', 'Live'),
(414, 'Perumbavoor', '1816', '18', 'Live'),
(415, 'Thiruvananthapuram', '1817', '18', 'Live'),
(416, 'Thrissur', '1818', '18', 'Live'),
(417, 'Trichur', '1819', '18', 'Live'),
(418, 'Trivandrum', '1820', '18', 'Live'),
(419, 'Wayanad', '1821', '18', 'Live'),
(420, 'Agatti Island', '1901', '19', 'Live'),
(421, 'Bingaram Island', '1902', '19', 'Live'),
(422, 'Bitra Island', '1903', '19', 'Live'),
(423, 'Chetlat Island', '1904', '19', 'Live'),
(424, 'Kadmat Island', '1905', '19', 'Live'),
(425, 'Kalpeni Island', '1906', '19', 'Live'),
(426, 'Kavaratti Island', '1907', '19', 'Live'),
(427, 'Kiltan Island', '1908', '19', 'Live'),
(428, 'Lakshadweep Sea', '1909', '19', 'Live'),
(429, 'Minicoy Island', '1910', '19', 'Live'),
(430, 'North Island', '1911', '19', 'Live'),
(431, 'South Island', '1912', '19', 'Live'),
(432, 'Anuppur', '2001', '20', 'Live'),
(433, 'Ashoknagar', '2002', '20', 'Live'),
(434, 'Balaghat', '2003', '20', 'Live'),
(435, 'Barwani', '2004', '20', 'Live'),
(436, 'Betul', '2005', '20', 'Live'),
(437, 'Bhind', '2006', '20', 'Live'),
(438, 'Bhopal', '2007', '20', 'Live'),
(439, 'Burhanpur', '2008', '20', 'Live'),
(440, 'Chhatarpur', '2009', '20', 'Live'),
(441, 'Chhindwara', '2010', '20', 'Live'),
(442, 'Damoh', '2011', '20', 'Live'),
(443, 'Datia', '2012', '20', 'Live'),
(444, 'Dewas', '2013', '20', 'Live'),
(445, 'Dhar', '2014', '20', 'Live'),
(446, 'Dindori', '2015', '20', 'Live'),
(447, 'Guna', '2016', '20', 'Live'),
(448, 'Gwalior', '2017', '20', 'Live'),
(449, 'Harda', '2018', '20', 'Live'),
(450, 'Hoshangabad', '2019', '20', 'Live'),
(451, 'Indore', '2020', '20', 'Live'),
(452, 'Jabalpur', '2021', '20', 'Live'),
(453, 'Jagdalpur', '2022', '20', 'Live'),
(454, 'Jhabua', '2023', '20', 'Live'),
(455, 'Katni', '2024', '20', 'Live'),
(456, 'Khandwa', '2025', '20', 'Live'),
(457, 'Khargone', '2026', '20', 'Live'),
(458, 'Mandla', '2027', '20', 'Live'),
(459, 'Mandsaur', '2028', '20', 'Live'),
(460, 'Morena', '2029', '20', 'Live'),
(461, 'Narsinghpur', '2030', '20', 'Live'),
(462, 'Neemuch', '2031', '20', 'Live'),
(463, 'Panna', '2032', '20', 'Live'),
(464, 'Raisen', '2033', '20', 'Live'),
(465, 'Rajgarh', '2034', '20', 'Live'),
(466, 'Ratlam', '2035', '20', 'Live'),
(467, 'Rewa', '2036', '20', 'Live'),
(468, 'Sagar', '2037', '20', 'Live'),
(469, 'Satna', '2038', '20', 'Live'),
(470, 'Sehore', '2039', '20', 'Live'),
(471, 'Seoni', '2040', '20', 'Live'),
(472, 'Shahdol', '2041', '20', 'Live'),
(473, 'Shajapur', '2042', '20', 'Live'),
(474, 'Sheopur', '2043', '20', 'Live'),
(475, 'Shivpuri', '2044', '20', 'Live'),
(476, 'Sidhi', '2045', '20', 'Live'),
(477, 'Tikamgarh', '2046', '20', 'Live'),
(478, 'Ujjain', '2047', '20', 'Live'),
(479, 'Umaria', '2048', '20', 'Live'),
(480, 'Vidisha', '2049', '20', 'Live'),
(481, 'Ahmednagar', '2101', '21', 'Live'),
(482, 'Akola', '2102', '21', 'Live'),
(483, 'Amravati', '2103', '21', 'Live'),
(484, 'Aurangabad', '2104', '21', 'Live'),
(485, 'Beed', '2105', '21', 'Live'),
(486, 'Bhandara', '2106', '21', 'Live'),
(487, 'Buldhana', '2107', '21', 'Live'),
(488, 'Chandrapur', '2108', '21', 'Live'),
(489, 'Dhule', '2109', '21', 'Live'),
(490, 'Gadchiroli', '2110', '21', 'Live'),
(491, 'Gondia', '2111', '21', 'Live'),
(492, 'Hingoli', '2112', '21', 'Live'),
(493, 'Jalgaon', '2113', '21', 'Live'),
(494, 'Jalna', '2114', '21', 'Live'),
(495, 'Kolhapur', '2115', '21', 'Live'),
(496, 'Latur', '2116', '21', 'Live'),
(497, 'Mahabaleshwar', '2117', '21', 'Live'),
(498, 'Mumbai', '2118', '21', 'Live'),
(499, 'Mumbai City', '2119', '21', 'Live'),
(500, 'Mumbai Suburban', '2120', '21', 'Live'),
(501, 'Nagpur', '2121', '21', 'Live'),
(502, 'Nanded', '2122', '21', 'Live'),
(503, 'Nandurbar', '2123', '21', 'Live'),
(504, 'Nashik', '2124', '21', 'Live'),
(505, 'Osmanabad', '2125', '21', 'Live'),
(506, 'Parbhani', '2126', '21', 'Live'),
(507, 'Pune', '2127', '21', 'Live'),
(508, 'Raigad', '2128', '21', 'Live'),
(509, 'Ratnagiri', '2129', '21', 'Live'),
(510, 'Sangli', '2130', '21', 'Live'),
(511, 'Satara', '2131', '21', 'Live'),
(512, 'Sholapur', '2132', '21', 'Live'),
(513, 'Sindhudurg', '2133', '21', 'Live'),
(514, 'Thane', '2134', '21', 'Live'),
(515, 'Wardha', '2135', '21', 'Live'),
(516, 'Washim', '2136', '21', 'Live'),
(517, 'Yavatmal', '2137', '21', 'Live'),
(518, 'Bishnupur', '2201', '22', 'Live'),
(519, 'Chandel', '2202', '22', 'Live'),
(520, 'Churachandpur', '2203', '22', 'Live'),
(521, 'Imphal East', '2204', '22', 'Live'),
(522, 'Imphal West', '2205', '22', 'Live'),
(523, 'Senapati', '2206', '22', 'Live'),
(524, 'Tamenglong', '2207', '22', 'Live'),
(525, 'Thoubal', '2208', '22', 'Live'),
(526, 'Ukhrul', '2209', '22', 'Live'),
(527, 'East Garo Hills', '2301', '23', 'Live'),
(528, 'East Khasi Hills', '2302', '23', 'Live'),
(529, 'Jaintia Hills', '2303', '23', 'Live'),
(530, 'Ri Bhoi', '2304', '23', 'Live'),
(531, 'Shillong', '2305', '23', 'Live'),
(532, 'South Garo Hills', '2306', '23', 'Live'),
(533, 'West Garo Hills', '2307', '23', 'Live'),
(534, 'West Khasi Hills', '2308', '23', 'Live'),
(535, 'Aizawl', '2401', '24', 'Live'),
(536, 'Champhai', '2402', '24', 'Live'),
(537, 'Kolasib', '2403', '24', 'Live'),
(538, 'Lawngtlai', '2404', '24', 'Live'),
(539, 'Lunglei', '2405', '24', 'Live'),
(540, 'Mamit', '2406', '24', 'Live'),
(541, 'Saiha', '2407', '24', 'Live'),
(542, 'Serchhip', '2408', '24', 'Live'),
(543, 'Dimapur', '2501', '25', 'Live'),
(544, 'Kohima', '2502', '25', 'Live'),
(545, 'Mokokchung', '2503', '25', 'Live'),
(546, 'Mon', '2504', '25', 'Live'),
(547, 'Phek', '2505', '25', 'Live'),
(548, 'Tuensang', '2506', '25', 'Live'),
(549, 'Wokha', '2507', '25', 'Live'),
(550, 'Zunheboto', '2508', '25', 'Live'),
(551, 'Angul', '2601', '26', 'Live'),
(552, 'Balangir', '2602', '26', 'Live'),
(553, 'Balasore', '2603', '26', 'Live'),
(554, 'Baleswar', '2604', '26', 'Live'),
(555, 'Bargarh', '2605', '26', 'Live'),
(556, 'Berhampur', '2606', '26', 'Live'),
(557, 'Bhadrak', '2607', '26', 'Live'),
(558, 'Bhubaneswar', '2608', '26', 'Live'),
(559, 'Boudh', '2609', '26', 'Live'),
(560, 'Cuttack', '2610', '26', 'Live'),
(561, 'Deogarh', '2611', '26', 'Live'),
(562, 'Dhenkanal', '2612', '26', 'Live'),
(563, 'Gajapati', '2613', '26', 'Live'),
(564, 'Ganjam', '2614', '26', 'Live'),
(565, 'Jagatsinghapur', '2615', '26', 'Live'),
(566, 'Jajpur', '2616', '26', 'Live'),
(567, 'Jharsuguda', '2617', '26', 'Live'),
(568, 'Kalahandi', '2618', '26', 'Live'),
(569, 'Kandhamal', '2619', '26', 'Live'),
(570, 'Kendrapara', '2620', '26', 'Live'),
(571, 'Kendujhar', '2621', '26', 'Live'),
(572, 'Khordha', '2622', '26', 'Live'),
(573, 'Koraput', '2623', '26', 'Live'),
(574, 'Malkangiri', '2624', '26', 'Live'),
(575, 'Mayurbhanj', '2625', '26', 'Live'),
(576, 'Nabarangapur', '2626', '26', 'Live'),
(577, 'Nayagarh', '2627', '26', 'Live'),
(578, 'Nuapada', '2628', '26', 'Live'),
(579, 'Puri', '2629', '26', 'Live'),
(580, 'Rayagada', '2630', '26', 'Live'),
(581, 'Rourkela', '2631', '26', 'Live'),
(582, 'Sambalpur', '2632', '26', 'Live'),
(583, 'Subarnapur', '2633', '26', 'Live'),
(584, 'Sundergarh', '2634', '26', 'Live'),
(585, 'Bahur', '2701', '27', 'Live'),
(586, 'Karaikal', '2701', '27', 'Live'),
(587, 'Mahe', '2701', '27', 'Live'),
(588, 'Pondicherry', '2701', '27', 'Live'),
(589, 'Purnankuppam', '2701', '27', 'Live'),
(590, 'Valudavur', '2701', '27', 'Live'),
(591, 'Villianur', '2701', '27', 'Live'),
(592, 'Yanam', '2701', '27', 'Live'),
(593, 'Amritsar', '2801', '28', 'Live'),
(594, 'Barnala', '2801', '28', 'Live'),
(595, 'Bathinda', '2801', '28', 'Live'),
(596, 'Faridkot', '2801', '28', 'Live'),
(597, 'Fatehgarh Sahib', '2801', '28', 'Live'),
(598, 'Ferozepur', '2801', '28', 'Live'),
(599, 'Gurdaspur', '2801', '28', 'Live'),
(600, 'Hoshiarpur', '2801', '28', 'Live'),
(601, 'Jalandhar', '2801', '28', 'Live'),
(602, 'Kapurthala', '2801', '28', 'Live'),
(603, 'Ludhiana', '2801', '28', 'Live'),
(604, 'Mansa', '2801', '28', 'Live'),
(605, 'Moga', '2801', '28', 'Live'),
(606, 'Muktsar', '2801', '28', 'Live'),
(607, 'Nawanshahr', '2801', '28', 'Live'),
(608, 'Pathankot', '2801', '28', 'Live'),
(609, 'Patiala', '2801', '28', 'Live'),
(610, 'Rupnagar', '2801', '28', 'Live'),
(611, 'Sangrur', '2801', '28', 'Live'),
(612, 'SAS Nagar', '2801', '28', 'Live'),
(613, 'Tarn Taran', '2801', '28', 'Live'),
(614, 'Ajmer', '2901', '29', 'Live'),
(615, 'Alwar', '2902', '29', 'Live'),
(616, 'Banswara', '2903', '29', 'Live'),
(617, 'Baran', '2904', '29', 'Live'),
(618, 'Barmer', '2905', '29', 'Live'),
(619, 'Bharatpur', '2906', '29', 'Live'),
(620, 'Bhilwara', '2907', '29', 'Live'),
(621, 'Bikaner', '2908', '29', 'Live'),
(622, 'Bundi', '2909', '29', 'Live'),
(623, 'Chittorgarh', '2910', '29', 'Live'),
(624, 'Churu', '2911', '29', 'Live'),
(625, 'Dausa', '2912', '29', 'Live'),
(626, 'Dholpur', '2913', '29', 'Live'),
(627, 'Dungarpur', '2914', '29', 'Live'),
(628, 'Hanumangarh', '2915', '29', 'Live'),
(629, 'Jaipur', '2916', '29', 'Live'),
(630, 'Jaisalmer', '2917', '29', 'Live'),
(631, 'Jalore', '2918', '29', 'Live'),
(632, 'Jhalawar', '2919', '29', 'Live'),
(633, 'Jhunjhunu', '2920', '29', 'Live'),
(634, 'Jodhpur', '2921', '29', 'Live'),
(635, 'Karauli', '2922', '29', 'Live'),
(636, 'Kota', '2923', '29', 'Live'),
(637, 'Nagaur', '2924', '29', 'Live'),
(638, 'Pali', '2925', '29', 'Live'),
(639, 'Pilani', '2926', '29', 'Live'),
(640, 'Rajsamand', '2927', '29', 'Live'),
(641, 'Sawai Madhopur', '2928', '29', 'Live'),
(642, 'Sikar', '2929', '29', 'Live'),
(643, 'Sirohi', '2930', '29', 'Live'),
(644, 'Sri Ganganagar', '2931', '29', 'Live'),
(645, 'Tonk', '2932', '29', 'Live'),
(646, 'Udaipur', '2933', '29', 'Live'),
(647, 'Barmiak', '3001', '30', 'Live'),
(648, 'Be', '3002', '30', 'Live'),
(649, 'Bhurtuk', '3003', '30', 'Live'),
(650, 'Chhubakha', '3004', '30', 'Live'),
(651, 'Chidam', '3005', '30', 'Live'),
(652, 'Chubha', '3006', '30', 'Live'),
(653, 'Chumikteng', '3007', '30', 'Live'),
(654, 'Dentam', '3008', '30', 'Live'),
(655, 'Dikchu', '3009', '30', 'Live'),
(656, 'Dzongri', '3010', '30', 'Live'),
(657, 'Gangtok', '3011', '30', 'Live'),
(658, 'Gauzing', '3012', '30', 'Live'),
(659, 'Gyalshing', '3013', '30', 'Live'),
(660, 'Hema', '3014', '30', 'Live'),
(661, 'Kerung', '3015', '30', 'Live'),
(662, 'Lachen', '3016', '30', 'Live'),
(663, 'Lachung', '3017', '30', 'Live'),
(664, 'Lema', '3018', '30', 'Live'),
(665, 'Lingtam', '3019', '30', 'Live'),
(666, 'Lungthu', '3020', '30', 'Live'),
(667, 'Mangan', '3021', '30', 'Live'),
(668, 'Namchi', '3022', '30', 'Live'),
(669, 'Namthang', '3023', '30', 'Live'),
(670, 'Nanga', '3024', '30', 'Live'),
(671, 'Nantang', '3025', '30', 'Live'),
(672, 'Naya Bazar', '3026', '30', 'Live'),
(673, 'Padamachen', '3027', '30', 'Live'),
(674, 'Pakhyong', '3028', '30', 'Live'),
(675, 'Pemayangtse', '3029', '30', 'Live'),
(676, 'Phensang', '3030', '30', 'Live'),
(677, 'Rangli', '3031', '30', 'Live'),
(678, 'Rinchingpong', '3032', '30', 'Live'),
(679, 'Sakyong', '3033', '30', 'Live'),
(680, 'Samdong', '3034', '30', 'Live'),
(681, 'Singtam', '3035', '30', 'Live'),
(682, 'Siniolchu', '3035', '30', 'Live'),
(683, 'Sombari', '3036', '30', 'Live'),
(684, 'Soreng', '3037', '30', 'Live'),
(685, 'Sosing', '3038', '30', 'Live'),
(686, 'Tekhug', '3039', '30', 'Live'),
(687, 'Temi', '3040', '30', 'Live'),
(688, 'Tsetang', '3041', '30', 'Live'),
(689, 'Tsomgo', '3042', '30', 'Live'),
(690, 'Tumlong', '3043', '30', 'Live'),
(691, 'Yangang', '3044', '30', 'Live'),
(692, 'Yumtang', '3045', '30', 'Live'),
(693, 'Chennai', '3101', '31', 'Live'),
(694, 'Chidambaram', '3102', '31', 'Live'),
(695, 'Chingleput', '3103', '31', 'Live'),
(696, 'Coimbatore', '3104', '31', 'Live'),
(697, 'Courtallam', '3105', '31', 'Live'),
(698, 'Cuddalore', '3106', '31', 'Live'),
(699, 'Dharmapuri', '3107', '31', 'Live'),
(700, 'Dindigul', '3108', '31', 'Live'),
(701, 'Erode', '3109', '31', 'Live'),
(702, 'Hosur', '3110', '31', 'Live'),
(703, 'Kanchipuram', '3111', '31', 'Live'),
(704, 'Kanyakumari', '3112', '31', 'Live'),
(705, 'Karaikudi', '3113', '31', 'Live'),
(706, 'Karur', '3114', '31', 'Live'),
(707, 'Kodaikanal', '3115', '31', 'Live'),
(708, 'Kovilpatti', '3116', '31', 'Live'),
(709, 'Krishnagiri', '3117', '31', 'Live'),
(710, 'Kumbakonam', '3118', '31', 'Live'),
(711, 'Madurai', '3119', '31', 'Live'),
(712, 'Mayiladuthurai', '3120', '31', 'Live'),
(713, 'Nagapattinam', '3121', '31', 'Live'),
(714, 'Nagarcoil', '3122', '31', 'Live'),
(715, 'Namakkal', '3123', '31', 'Live'),
(716, 'Neyveli', '3124', '31', 'Live'),
(717, 'Nilgiris', '3125', '31', 'Live'),
(718, 'Ooty', '3126', '31', 'Live'),
(719, 'Palani', '3127', '31', 'Live'),
(720, 'Perambalur', '3128', '31', 'Live'),
(721, 'Pollachi', '3129', '31', 'Live'),
(722, 'Pudukkottai', '3130', '31', 'Live'),
(723, 'Rajapalayam', '3131', '31', 'Live'),
(724, 'Ramanathapuram', '3132', '31', 'Live'),
(725, 'Salem', '3133', '31', 'Live'),
(726, 'Sivaganga', '3134', '31', 'Live'),
(727, 'Sivakasi', '3135', '31', 'Live'),
(728, 'Thanjavur', '3136', '31', 'Live'),
(729, 'Theni', '3137', '31', 'Live'),
(730, 'Thoothukudi', '3138', '31', 'Live'),
(731, 'Tiruchirappalli', '3139', '31', 'Live'),
(732, 'Tirunelveli', '3140', '31', 'Live'),
(733, 'Tirupur', '3141', '31', 'Live'),
(734, 'Tiruvallur', '3142', '31', 'Live'),
(735, 'Tiruvannamalai', '3143', '31', 'Live'),
(736, 'Tiruvarur', '3144', '31', 'Live'),
(737, 'Trichy', '3145', '31', 'Live'),
(738, 'Tuticorin', '3146', '31', 'Live'),
(739, 'Vellore', '3147', '31', 'Live'),
(740, 'Villupuram', '3148', '31', 'Live'),
(741, 'Virudhunagar', '3149', '31', 'Live'),
(742, 'Yercaud', '3150', '31', 'Live'),
(743, 'Agartala', '3201', '32', 'Live'),
(744, 'Ambasa', '3202', '32', 'Live'),
(745, 'Bampurbari', '3203', '32', 'Live'),
(746, 'Belonia', '3204', '32', 'Live'),
(747, 'Dhalai', '3205', '32', 'Live'),
(748, 'Dharam Nagar', '3206', '32', 'Live'),
(749, 'Kailashahar', '3207', '32', 'Live'),
(750, 'Kamal Krishnabari', '3208', '32', 'Live'),
(751, 'Khopaiyapara', '3209', '32', 'Live'),
(752, 'Khowai', '3210', '32', 'Live'),
(753, 'Phuldungsei', '3211', '32', 'Live'),
(754, 'Radha Kishore Pur', '3212', '32', 'Live'),
(755, 'Tripura', '3213', '32', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `all_states`
--

DROP TABLE IF EXISTS `all_states`;
CREATE TABLE IF NOT EXISTS `all_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` text,
  `state_name` text,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_states`
--

INSERT INTO `all_states` (`id`, `state_code`, `state_name`, `del_status`) VALUES
(1, '1', 'Andaman & Nicobar [AN]', 'Live'),
(2, '2', 'Andhra Pradesh [AP]', 'Live'),
(3, '3', 'Arunachal Pradesh [AR]', 'Live'),
(4, '4', 'Assam [AS]', 'Live'),
(5, '5', 'Bihar [BH]', 'Live'),
(6, '6', 'Chandigarh [CH]', 'Live'),
(7, '7', 'Chhattisgarh [CG]', 'Live'),
(8, '8', 'Dadra & Nagar Haveli [DN]', 'Live'),
(9, '9', 'Daman & Diu [DD]', 'Live'),
(10, '10', 'Delhi [DL]', 'Live'),
(11, '11', 'Goa [GO]', 'Live'),
(12, '12', 'Gujarat [GU]', 'Live'),
(13, '13', 'Haryana [HR]', 'Live'),
(14, '14', 'Himachal Pradesh [HP]', 'Live'),
(15, '15', 'Jammu & Kashmir [JK]', 'Live'),
(16, '16', 'Jharkhand [JH]', 'Live'),
(17, '17', 'Karnataka [KR]', 'Live'),
(18, '18', 'Kerala [KL]', 'Live'),
(19, '19', 'Lakshadweep [LD]', 'Live'),
(20, '20', 'Madhya Pradesh [MP]', 'Live'),
(21, '21', 'Maharashtra [MH]', 'Live'),
(22, '22', 'Manipur [MN]', 'Live'),
(23, '23', 'Meghalaya [ML]', 'Live'),
(24, '24', 'Mizoram [MM]', 'Live'),
(25, '25', 'Nagaland [NL]', 'Live'),
(26, '26', 'Orissa [OR]', 'Live'),
(27, '27', 'Pondicherry [PC]', 'Live'),
(28, '28', 'Punjab [PJ]', 'Live'),
(29, '29', 'Rajasthan [RJ]', 'Live'),
(30, '30', 'Sikkim [SK]', 'Live'),
(31, '31', 'Tamil Nadu [TN]', 'Live'),
(32, '32', 'Tripura [TR]', 'Live'),
(33, '33', 'Uttar Pradesh [UP]', 'Live'),
(34, '34', 'Uttaranchal [UT]', 'Live'),
(35, '35', 'West Bengal [WB]', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `map` text NOT NULL,
  `address` text NOT NULL,
  `email` text NOT NULL,
  `email_alt` text,
  `contact_number_1` varchar(50) NOT NULL,
  `contact_number_2` varchar(50) NOT NULL,
  `contact_us_title` varchar(300) NOT NULL,
  `contact_us_desc` text NOT NULL,
  `contact_us_sub_desc` text NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `map`, `address`, `email`, `email_alt`, `contact_number_1`, `contact_number_2`, `contact_us_title`, `contact_us_desc`, `contact_us_sub_desc`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.4076207142466!2d72.80569181493519!3d21.175960285918723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04ddff027cd05%3A0x2559fd2b2e6e96cb!2sCANOPUS%20GLOBAL%20EDUCATION!5e0!3m2!1sen!2sin!4v1599731082996!5m2!1sen!2sin', '121 King Street, Melbourne Victoria 3000, Australia', 'support@mail.com', 'info@mail.com', '+666 777 999 00', '+666 555 222 00', 'Contact Us', 'Al is no longer a futuristic notion, it\'s here right now', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'assets/images/pagetop-bg.jpg', 1, '::1', '2020-07-30 00:00:00', 1, '::1', '2021-02-21 17:24:24', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS `contact_us` (
  `contact_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(250) NOT NULL,
  `contact_email` text NOT NULL,
  `contact_phone` varchar(50) NOT NULL,
  `contact_message` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`contact_us_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer_setting`
--

DROP TABLE IF EXISTS `footer_setting`;
CREATE TABLE IF NOT EXISTS `footer_setting` (
  `footer_id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_image` text NOT NULL,
  `footer_desc` text NOT NULL,
  `address` text NOT NULL,
  `email` text NOT NULL,
  `contact_number` varchar(30) NOT NULL,
  `total_visitor` varchar(20) NOT NULL,
  `unique_visitor` varchar(20) NOT NULL,
  `footer_facebook` text,
  `footer_insta` text,
  `footer_twitter` text,
  `footer_google` text,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`footer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_setting`
--

INSERT INTO `footer_setting` (`footer_id`, `logo_image`, `footer_desc`, `address`, `email`, `contact_number`, `total_visitor`, `unique_visitor`, `footer_facebook`, `footer_insta`, `footer_twitter`, `footer_google`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'assets/images/logo2.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.', '27 Division, mirpur-12, pallbi.', 'info@weborative.com', '555 666 999 00', '2147483647', '2147483648', 'javascript:void(0);', 'javascript:void(0);', 'javascript:void(0);', 'javascript:void(0);', 1, '::1', '2020-07-31 00:00:00', 1, '::1', '2021-02-21 20:35:15', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter_email` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`newsletter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

DROP TABLE IF EXISTS `privacy_policy`;
CREATE TABLE IF NOT EXISTS `privacy_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` text NOT NULL,
  `page_desc` text NOT NULL,
  `page_id` varchar(250) NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `page_title`, `page_desc`, `page_id`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Privacy Policy', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">The Website and its Content is owned by (“Company”, “we”, or “us”). The term “you” refers to the user or viewer of </span><a href=\"http://canopusedu.com/\"><span style=\"font-family: Roboto;\">Canopus Global Education</span></a><span style=\"font-family: Roboto;\"> (“Website”).</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal;\"><span style=\"font-family: Roboto;\">Please read this Privacy Policy carefully. We reserve the right to change this Privacy Policy on the Website at any time without notice. Use of any information or contribution that you provide to us, or which is collected by us on or through our Website or its Content is governed by this Privacy Policy. By using our Website or its Content, you consent to this Privacy Policy, whether or not you have read it. If you do not agree with this Privacy Policy, please do not use our Website or its Content.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">SUBMISSION, STORAGE AND SHARING OF PERSONAL DATA</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">To use our Website or its Content, we may seek personal data including your name, e-mail address, street address, city, state, billing information, or other personally identifying information (“Confidential Information”), or you may offer or provide a comment, photo, image, video or any other submission to us when visiting or interacting with our Website and its Content (“Other Information”). By providing such Confidential Information or Other Information to us, you grant us permission to use and store such information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">Your Confidential Information is stored through by us internally or through a data management system. Your Confidential Information will only be accessed by those who help to obtain, manage or store that Information, or who have a legitimate need to know such Confidential Information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">There may be an occasion where we may ask for demographic information such as gender or age, but if you choose not to provide such data and information, you may still use the Website and its Content, but you may not be able to use those services where demographic information may be required.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CONFIDENTIALITY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">We aim to keep the Confidential Information that you share with us confidential. Please note that we may disclose such Confidential Information if required to do so by law or in the good-faith belief that: (1) such action is necessary to protect and defend our rights or property or those of our users or licensees, (2) to act as immediately necessary in order to protect the personal safety or rights of our users or the public, or (3) to investigate or respond to any real or perceived violation of this Privacy Policy or of our Disclaimer, Terms and Conditions, or any other terms of use or agreement with us.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">VIEWING BY OTHERS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">Note that whenever you voluntarily make your Confidential Information or Other Information available for viewing by others online through this Website or its Content, it may be seen, collected and used by others, and therefore, we cannot be responsible for any unauthorized or improper use of the Confidential Information or Other Information that you voluntarily share.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PASSWORDS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To use certain features of the Website or its Content, you may need a username and password. You are responsible for maintaining the confidentiality of the username and password, and you are responsible for all activities, whether by you or by others, that occur under your username or password and within your account. You agree to notify us immediately of any unauthorized or improper use of your username or password or any other breach of security. To help protect against unauthorized or improper use, make sure that you log out at the end of each session requiring your username and password.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">It is your responsibility to protect your own username and password from disclosure to others. We cannot and will not be liable for any loss or damage arising from your failure to protect your username, password or account information. If you share your username or password with others, they may be able to obtain access to your personal information at your own risk.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">By using our Website and its Content you agree to enter true and accurate information on the Website and its Content. If you enter a bogus email address we have the right to immediately inactivate your account.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We will use our best efforts to keep your username and password(s) private and will not otherwise share your password(s) without your consent, except as necessary when the law requires it or in the good faith belief that such action is necessary, particularly when disclosure is necessary to identify, contact or bring legal action against someone who may be causing injury to others or interfering with our rights or property.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">UNSUBSCRIBE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">You may unsubscribe to our e-newsletters or updates at any time through the unsubscribe link at the footer of all e-mail communications.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We manage e-mail lists through a list management system. Unsubscribing from one list managed by us will not necessarily remove you from all publication email lists. If you have questions or are experiencing problems unsubscribing, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANTI-SPAM POLICY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have a no spam policy and provide you with the ability to opt-out of our communications by selecting the unsubscribe link at the footer of all e-mails. We have taken the necessary steps to ensure that we are compliant with the CAN-SPAM Act of 2003 by never sending out misleading information. We will not sell, rent or share your email address.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CHILDREN’S ONLINE PRIVACY PROTECTION ACT COMPLIANCE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We do not collect any information from anyone under 13 years of age in compliance with COPPA (Children’s Online Privacy Protection Act), and our Website and its Content is directed to individuals who are at least 13 years old or older.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANONYMOUS DATA COLLECTION AND USE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To maintain our Website’s high quality, we may use your IP address to help diagnose problems with our server and to administer the Website by identifying which areas of the Website are most heavily used, and to display content according to your preferences. Your IP address is the number assigned to computers connected to the Internet. This is essentially “traffic data” which cannot personally identify you, but is helpful to us for marketing purposes and for improving our services. Traffic data collection does not follow a user’s activities on any other Websites in any way. Anonymous traffic data may also be shared with business partners and advertisers on an aggregate basis.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">USE OF “COOKIES”</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use the standard “cookies” feature of major web browsers. We do not set any personally identifiable information in cookies, nor do we employ any data-capture mechanisms on our Website other than cookies. You may choose to disable cookies through your own web browser’s settings. However, disabling this function may diminish your experience on the Website and some features may not work as intended. We have no access to or control over any information collected by other individuals, companies or entities whose website or materials may be linked to our Website or its Content.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PRIVACY POLICIES OF OTHER WEBSITES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have no responsibility or liability for the content and activities of any other individual, company or entity whose website or materials may be linked to our Website or its Content, and thus we cannot be held liable for the privacy of the information on their website or that you voluntarily share with their website. Please review their privacy policies for guidelines as to how they respectively store, use and protect the privacy of your Confidential Information and Other Information.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ASSIGNMENT OF RIGHTS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">In the event of an assignment, sale, joint venture, or other transfer of some or all of our assets, you agree we can assign, sell, license or transfer any information that you have provided to us. Please note, however, that any purchasing party is prohibited from using the Confidential Information or Other Information submitted to us under this Privacy Policy in a manner that is materially inconsistent with this Privacy Policy without your prior consent.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">NOTIFICATION OF CHANGES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use your contact information to inform you of changes to the Website or its Content, or, if requested, to send you additional information about us. We reserve the right, at our sole discretion, to change, modify or otherwise alter our Website, its Content and this Privacy Policy at any time. Such changes and/or modifications shall become effective immediately upon posting our updated Privacy Policy. Please review this Privacy Policy periodically. Continued use of any of information obtained through or on the Website or its Content following the posting of changes and/or modifications constitutes acceptance of the revised Privacy Policy.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">If you have any questions about this Privacy Policy, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">The Website and its Content is owned by (“Company”, “we”, or “us”). The term “you” refers to the user or viewer of </span><span style=\"font-family: Roboto;\"><a href=\"http://sweetnrush.weborative.com/\">C</a>anopus Global Education</span><span style=\"font-family: Roboto;\"> (“Website”).</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal;\"><span style=\"font-family: Roboto;\">Please read this Privacy Policy carefully. We reserve the right to change this Privacy Policy on the Website at any time without notice. Use of any information or contribution that you provide to us, or which is collected by us on or through our Website or its Content is governed by this Privacy Policy. By using our Website or its Content, you consent to this Privacy Policy, whether or not you have read it. If you do not agree with this Privacy Policy, please do not use our Website or its Content.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">SUBMISSION, STORAGE AND SHARING OF PERSONAL DATA</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">To use our Website or its Content, we may seek personal data including your name, e-mail address, street address, city, state, billing information, or other personally-identifying information (“Confidential Information”), or you may offer or provide a comment, photo, image, video or any other submission to us when visiting or interacting with our Website and its Content (“Other Information”). By providing such Confidential Information or Other Information to us, you grant us permission to use and store such information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">Your Confidential Information is stored through by us internally or through a data management system. Your Confidential Information will only be accessed by those who help to obtain, manage, or store that Information, or who have a legitimate need to know such Confidential Information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">There may be an occasion where we may ask for demographic information such as gender or age, but if you choose not to provide such data and information, you may still use the Website and its Content, but you may not be able to use those services where demographic information may be required.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CONFIDENTIALITY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">We aim to keep the Confidential Information that you share with us confidential. Please note that we may disclose such Confidential Information if required to do so by law or in the good-faith belief that: (1) such action is necessary to protect and defend our rights or property or those of our users or licensees, (2) to act as immediately necessary in order to protect the personal safety or rights of our users or the public, or (3) to investigate or respond to any real or perceived violation of this Privacy Policy or of our Disclaimer, Terms, and Conditions, or any other terms of use or agreement with us.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">VIEWING BY OTHERS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">Note that whenever you voluntarily make your Confidential Information or Other Information available for viewing by others online through this Website or its Content, it may be seen, collected and used by others, and therefore, we cannot be responsible for any unauthorized or improper use of the Confidential Information or Other Information that you voluntarily share.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PASSWORDS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To use certain features of the Website or its Content, you may need a username and password. You are responsible for maintaining the confidentiality of the username and password, and you are responsible for all activities, whether by you or by others, that occur under your username or password and within your account. You agree to notify us immediately of any unauthorized or improper use of your username or password or any other breach of security. To help protect against unauthorized or improper use, make sure that you log out at the end of each session requiring your username and password.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">It is your responsibility to protect your own username and password from disclosure to others. We cannot and will not be liable for any loss or damage arising from your failure to protect your username, password or account information. If you share your username or password with others, they may be able to obtain access to your personal information at your own risk.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">By using our Website and its Content you agree to enter true and accurate information on the Website and its Content. If you enter a bogus email address we have the right to immediately inactivate your account.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We will use our best efforts to keep your username and password(s) private and will not otherwise share your password(s) without your consent, except as necessary when the law requires it or in the good faith belief that such action is necessary, particularly when disclosure is necessary to identify, contact or bring legal action against someone who may be causing injury to others or interfering with our rights or property.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">UNSUBSCRIBE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">You may unsubscribe to our e-newsletters or updates at any time through the unsubscribe link at the footer of all e-mail communications.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We manage e-mail lists through a list management system. Unsubscribing from one list managed by us will not necessarily remove you from all publication email lists. If you have questions or are experiencing problems unsubscribing, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANTI-SPAM POLICY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have a no spam policy and provide you with the ability to opt-out of our communications by selecting the unsubscribe link at the footer of all e-mails. We have taken the necessary steps to ensure that we are compliant with the CAN-SPAM Act of 2003 by never sending out misleading information. We will not sell, rent, or share your email address.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CHILDREN’S ONLINE PRIVACY PROTECTION ACT COMPLIANCE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We do not collect any information from anyone under 13 years of age in compliance with COPPA (Children’s Online Privacy Protection Act), and our Website and its Content is directed to individuals who are at least 13 years old or older.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANONYMOUS DATA COLLECTION AND USE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To maintain our Website’s high quality, we may use your IP address to help diagnose problems with our server and to administer the Website by identifying which areas of the Website are most heavily used, and to display content according to your preferences. Your IP address is the number assigned to computers connected to the Internet. This is essentially “traffic data” which cannot personally identify you, but is helpful to us for marketing purposes and for improving our services. Traffic data collection does not follow a user’s activities on any other Websites in any way. Anonymous traffic data may also be shared with business partners and advertisers on an aggregate basis.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">USE OF “COOKIES”</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use the standard “cookies” feature of major web browsers. We do not set any personally identifiable information in cookies, nor do we employ any data-capture mechanisms on our Website other than cookies. You may choose to disable cookies through your own web browser’s settings. However, disabling this function may diminish your experience on the Website and some features may not work as intended. We have no access to or control over any information collected by other individuals, companies, or entities whose website or materials may be linked to our Website or its Content.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PRIVACY POLICIES OF OTHER WEBSITES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have no responsibility or liability for the content and activities of any other individual, company or entity whose website or materials may be linked to our Website or its Content, and thus we cannot be held liable for the privacy of the information on their website or that you voluntarily share with their website. Please review their privacy policies for guidelines as to how they respectively store, use and protect the privacy of your Confidential Information and Other Information.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ASSIGNMENT OF RIGHTS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">In the event of an assignment, sale, joint venture, or other transfer of some or all of our assets, you agree we can assign, sell, license or transfer any information that you have provided to us. Please note, however, that any purchasing party is prohibited from using the Confidential Information or Other Information submitted to us under this Privacy Policy in a manner that is materially inconsistent with this Privacy Policy without your prior consent.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">NOTIFICATION OF CHANGES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use your contact information to inform you of changes to the Website or its Content, or, if requested, to send you additional information about us. We reserve the right, at our sole discretion, to change, modify or otherwise alter our Website, it\'s Content, and this Privacy Policy at any time. Such changes and/or modifications shall become effective immediately upon posting our updated Privacy Policy. Please review this Privacy Policy periodically. Continued use of any of the information obtained through or on the Website or its Content following the posting of changes and/or modifications constitutes acceptance of the revised Privacy Policy.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">If you have any questions about this Privacy Policy, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p>', 'privacy', '', 1, '::1', '2020-08-06 00:00:00', 1, '::1', '2020-10-02 08:44:30', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_title` text NOT NULL,
  `slider_description` text,
  `slider_image` text NOT NULL,
  `slider_order_no` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_description`, `slider_image`, `slider_order_no`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `is_active`, `del_status`) VALUES
(1, 'Builders Merchant <br> Tumble Despite Safety <strong class=\"d-block thm-clr\">- Our Company.</strong>', 'There are many variations of passages of Lorem Ipsum available, but the ma Mority have suffered alteration in some form.', 'assets/images/resources/slide1.jpg', 1, 1, '::1', '2020-07-29 00:00:00', 1, '::1', '2021-02-12 08:35:20', 1, 'Live'),
(2, 'Builders Merchant <br> Tumble Despite Safety <strong class=\"d-block thm-clr\">- Our Company.</strong>', 'There are many variations of passages of Lorem Ipsum available, but the ma Mority have suffered alteration in some form.', 'assets/images/resources/slide2.jpg', 2, 1, '::1', '2020-07-29 00:00:00', 1, '::1', '2021-02-12 08:35:46', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `smtp_setting`
--

DROP TABLE IF EXISTS `smtp_setting`;
CREATE TABLE IF NOT EXISTS `smtp_setting` (
  `smtp_id` int(11) NOT NULL AUTO_INCREMENT,
  `smtp_protocol` varchar(250) NOT NULL,
  `smtp_host` text NOT NULL,
  `smtp_port` text NOT NULL,
  `smtp_user` text NOT NULL,
  `smtp_pass` text NOT NULL,
  `smtp_crypto` text NOT NULL,
  `mailtype` text NOT NULL,
  `smtp_timeout` text NOT NULL,
  `charset` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`smtp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smtp_setting`
--

INSERT INTO `smtp_setting` (`smtp_id`, `smtp_protocol`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `smtp_crypto`, `mailtype`, `smtp_timeout`, `charset`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'smtp', 'md-in-66.webhostbox.net', '465', 'info@aksharbuidcon.in', 'aksharbuildcon@123#', 'ssl', 'html', '100', 'UTF-8', 1, '::1', '2020-07-31 00:00:00', 1, '::1', '2021-02-23 08:57:58', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

DROP TABLE IF EXISTS `tbl_client`;
CREATE TABLE IF NOT EXISTS `tbl_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_logo` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

DROP TABLE IF EXISTS `tbl_faq`;
CREATE TABLE IF NOT EXISTS `tbl_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_title` varchar(250) NOT NULL,
  `faq_desc` text NOT NULL,
  `faq_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`faq_id`, `faq_title`, `faq_desc`, `faq_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'OUR FAQ', 'Freequently Ask Questions', 'assets/images/resources/faq-mockup.png', NULL, NULL, NULL, 1, '::1', '2021-02-21 09:43:23', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq_item`
--

DROP TABLE IF EXISTS `tbl_faq_item`;
CREATE TABLE IF NOT EXISTS `tbl_faq_item` (
  `faq_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_faq_id` int(11) NOT NULL,
  `faq_item_title` varchar(250) NOT NULL,
  `faq_item_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`faq_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_faq_item`
--

INSERT INTO `tbl_faq_item` (`faq_item_id`, `ref_faq_id`, `faq_item_title`, `faq_item_desc`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(28, 1, 'How can i buy this headphone?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', 1, 1, '::1', '2021-02-21 09:43:23', NULL, NULL, NULL, 'Live'),
(29, 1, 'This is Especially for listening to music?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', 1, 1, '::1', '2021-02-21 09:43:23', NULL, NULL, NULL, 'Live'),
(30, 1, 'How can i ordered it?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', 1, 1, '::1', '2021-02-21 09:43:23', NULL, NULL, NULL, 'Live'),
(31, 1, 'How can work this headphone?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', 1, 1, '::1', '2021-02-21 09:43:23', NULL, NULL, NULL, 'Live'),
(32, 1, 'This is Especially for listening to music?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', 1, 1, '::1', '2021-02-21 09:43:23', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_get_in_touch_setting`
--

DROP TABLE IF EXISTS `tbl_get_in_touch_setting`;
CREATE TABLE IF NOT EXISTS `tbl_get_in_touch_setting` (
  `get_in_touch_id` int(11) NOT NULL AUTO_INCREMENT,
  `get_in_touch_title` varchar(250) NOT NULL,
  `get_in_touch_desc` text NOT NULL,
  `get_in_touch_sub_desc` text NOT NULL,
  `get_in_touch_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`get_in_touch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_get_in_touch_setting`
--

INSERT INTO `tbl_get_in_touch_setting` (`get_in_touch_id`, `get_in_touch_title`, `get_in_touch_desc`, `get_in_touch_sub_desc`, `get_in_touch_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Ready to get started?', 'Get in touch, or create an account.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'assets/images/parallax2.jpg', NULL, NULL, NULL, 1, '::1', '2021-02-17 11:25:10', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_get_quote`
--

DROP TABLE IF EXISTS `tbl_get_quote`;
CREATE TABLE IF NOT EXISTS `tbl_get_quote` (
  `get_quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_name` varchar(250) NOT NULL,
  `recipient_mobile` varchar(50) NOT NULL,
  `recipient_email` text NOT NULL,
  `message_text` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`get_quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hot_deal`
--

DROP TABLE IF EXISTS `tbl_hot_deal`;
CREATE TABLE IF NOT EXISTS `tbl_hot_deal` (
  `hot_deal_id` int(11) NOT NULL AUTO_INCREMENT,
  `hot_deal_main_title` varchar(250) NOT NULL,
  `hot_deal_title` text NOT NULL,
  `hot_deal_desc` text NOT NULL,
  `hot_deal_sub_title_1` varchar(250) NOT NULL,
  `hot_deal_sub_desc_1` text NOT NULL,
  `hot_deal_sub_title_2` varchar(250) NOT NULL,
  `hot_deal_sub_desc_2` text NOT NULL,
  `hot_deal_sub_title_3` varchar(250) NOT NULL,
  `hot_deal_sub_desc_3` text NOT NULL,
  `hot_deal_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`hot_deal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_hot_deal`
--

INSERT INTO `tbl_hot_deal` (`hot_deal_id`, `hot_deal_main_title`, `hot_deal_title`, `hot_deal_desc`, `hot_deal_sub_title_1`, `hot_deal_sub_desc_1`, `hot_deal_sub_title_2`, `hot_deal_sub_desc_2`, `hot_deal_sub_title_3`, `hot_deal_sub_desc_3`, `hot_deal_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(2, 'TECHNICAL SOLUTIONS', 'Hot Deal Ship Buildup <i>15% <br> Off</i>', 'We are always ready to best solution for your problem.', 'Expart Experience Worker.', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.', '20-25 Years Warranty.', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.', 'Free 15% Buildup Construction.', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.', 'assets/images/resources/solutions-img.jpg', NULL, NULL, NULL, 1, '::1', '2021-02-23 05:40:57', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail_template`
--

DROP TABLE IF EXISTS `tbl_mail_template`;
CREATE TABLE IF NOT EXISTS `tbl_mail_template` (
  `mail_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` mediumtext NOT NULL,
  `subject` mediumtext NOT NULL,
  `message` text NOT NULL,
  `fromname` mediumtext NOT NULL,
  `fromemail` varchar(100) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`mail_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mail_template`
--

INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES
(1, 'contact_us_customer', 'Contac Us', '<h2><br />\r\nHiiii<br />\r\n<strong>{person_name} </strong>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</h2>\r\n\r\n<h2><strong>Thank you.</strong></h2>\r\n', 'Akshar Buildcon', 'info@aksharbuidcon.in', 1, 'Live'),
(7, 'contact_us_admin', 'Contac Us', '<h2><br />\r\nName : {contact_name}<br />\r\nEmail : {contact_email}<br />\r\nContact Number : {contact_phone}<br />\r\nMesssage : {contact_message}</h2>\r\n', 'Akshar Buildcon', 'info@aksharbuidcon.in', 1, 'Live'),
(8, 'get_quote_customer', 'Get Quote', '<h2><br />\r\nHiiii<br />\r\n<strong>{person_name} </strong>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</h2>\r\n\r\n<h2><strong>Thank you.</strong></h2>\r\n', 'Akshar Buildcon', 'info@aksharbuidcon.in', 1, 'Live'),
(9, 'get_quote_admin', 'Get Quote', '<h2><br />\r\nName : {contact_name}<br />\r\nEmail : {contact_email}<br />\r\nContact Number : {contact_phone}<br />\r\nMesssage : {contact_message}</h2>\r\n', 'Akshar Buildcon', 'info@aksharbuidcon.in', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_service`
--

DROP TABLE IF EXISTS `tbl_other_service`;
CREATE TABLE IF NOT EXISTS `tbl_other_service` (
  `other_sevice_id` int(11) NOT NULL AUTO_INCREMENT,
  `other_sevice_title` varchar(250) NOT NULL,
  `other_sevice_desc` text NOT NULL,
  `other_sevice_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`other_sevice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_other_service`
--

INSERT INTO `tbl_other_service` (`other_sevice_id`, `other_sevice_title`, `other_sevice_desc`, `other_sevice_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Bridging gap between Business and Technology', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since.', 'assets/images/parallax3.jpg', NULL, NULL, NULL, 1, '::1', '2021-02-20 08:31:08', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE IF NOT EXISTS `tbl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(250) NOT NULL,
  `project_location` varchar(250) NOT NULL,
  `project_desc` text NOT NULL,
  `main_desc` text NOT NULL,
  `project_video` text,
  `project_icon` varchar(250) NOT NULL,
  `project_poster_image` text NOT NULL,
  `project_widget` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_image`
--

DROP TABLE IF EXISTS `tbl_project_image`;
CREATE TABLE IF NOT EXISTS `tbl_project_image` (
  `project_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_project_id` int(11) NOT NULL,
  `project_image` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`project_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_setting`
--

DROP TABLE IF EXISTS `tbl_service_setting`;
CREATE TABLE IF NOT EXISTS `tbl_service_setting` (
  `service_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_title_1` varchar(250) NOT NULL,
  `service_title_2` varchar(250) NOT NULL,
  `service_desc` text NOT NULL,
  `sub_service_icon_1` varchar(250) NOT NULL,
  `sub_service_title_1` varchar(250) NOT NULL,
  `sub_service_desc_1` text NOT NULL,
  `sub_service_desc_1_1` text NOT NULL,
  `sub_service_icon_2` varchar(250) NOT NULL,
  `sub_service_title_2` varchar(250) NOT NULL,
  `sub_service_desc_2` text NOT NULL,
  `sub_service_desc_2_2` text NOT NULL,
  `sub_service_icon_3` varchar(250) NOT NULL,
  `sub_service_title_3` varchar(250) NOT NULL,
  `sub_service_desc_3` text NOT NULL,
  `sub_service_desc_3_3` text NOT NULL,
  `sub_service_icon_4` varchar(250) NOT NULL,
  `sub_service_title_4` varchar(205) NOT NULL,
  `sub_service_desc_4` text NOT NULL,
  `sub_service_desc_4_4` text NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`service_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service_setting`
--

INSERT INTO `tbl_service_setting` (`service_setting_id`, `service_title_1`, `service_title_2`, `service_desc`, `sub_service_icon_1`, `sub_service_title_1`, `sub_service_desc_1`, `sub_service_desc_1_1`, `sub_service_icon_2`, `sub_service_title_2`, `sub_service_desc_2`, `sub_service_desc_2_2`, `sub_service_icon_3`, `sub_service_title_3`, `sub_service_desc_3`, `sub_service_desc_3_3`, `sub_service_icon_4`, `sub_service_title_4`, `sub_service_desc_4`, `sub_service_desc_4_4`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Our Standard Services', 'Al is no longer a futuristic notion, it\'s here right now', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'flaticon-wheel-saw', 'Construction <br> And Engineering', 'Nunc laoreet, mi sed fermentum fringilla, eros metus.', '<i class=\"thm-clr\">1-</i> Builders merchant sales', 'flaticon-helmet', 'Industrial Equipments', 'Nunc laoreet, mi sed fermentum fringilla, eros metus.', '<i class=\"thm-clr\">2-</i> Builders merchant sales', 'flaticon-carpenter', 'Owner\'s <br> Representation', 'Nunc laoreet, mi sed fermentum fringilla, eros metus.', '<i class=\"thm-clr\">3-</i> Builders merchant sales', 'flaticon-jigsaw', 'Fuel Gas <br> Productions', 'Nunc laoreet, mi sed fermentum fringilla, eros metus.', '<i class=\"thm-clr\">4-</i> Builders merchant sales', 'assets/images/parallax1.jpg', NULL, NULL, NULL, 1, '::1', '2021-02-15 06:53:15', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_side_menu`
--

DROP TABLE IF EXISTS `tbl_side_menu`;
CREATE TABLE IF NOT EXISTS `tbl_side_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(250) NOT NULL,
  `menu_constant` varchar(250) NOT NULL,
  `menu_url` text NOT NULL,
  `ref_menu_id` int(11) DEFAULT NULL,
  `menu_icon` varchar(250) DEFAULT NULL,
  `menu_order_no` int(11) DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_side_menu`
--

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES
(1, 'Dashboard', 'DASHBOARD', 'admin/Dashboard', NULL, 'fal fa-info-circle', 1, 'Live'),
(2, 'Home', 'HOME', '#', NULL, 'fal fa-home-alt', 2, 'Live'),
(3, 'About', 'ABOUT', 'admin/About', NULL, 'fal fa-user', 3, 'Live'),
(5, 'Contact', 'CONTACT', 'admin/Contact', NULL, 'fal fa-address-book', 8, 'Live'),
(6, 'Mail', 'MAIL', '#', NULL, 'fal fa-envelope', 9, 'Live'),
(7, 'Setting', 'FOOTER', '#', NULL, 'fal fa-sliders-h', 10, 'Live'),
(8, 'User', 'USER', 'admin/Dashboard/user', NULL, 'fal fa-user', 11, 'Live'),
(9, 'Slider', 'SLIDER', 'admin/Slider', 2, NULL, 1, 'Live'),
(12, 'Testimonial', 'TESTIMONIAL', 'admin/Home/testimonial', 2, NULL, 2, 'Live'),
(18, 'SMTP Setting', 'SMTP_SETTING', 'admin/Mail', 6, NULL, 1, 'Live'),
(19, 'Newsletter', 'NEWSLETTER', 'admin/Mail/newsletter', 6, NULL, 2, 'Deleted'),
(20, 'Contact Us', 'CONTACT_US', 'admin/Mail/contact_us', 6, NULL, 3, 'Live'),
(21, 'Mail Template', 'MAIL_TEMPLATE', 'admin/Mail/mailTemplate', 6, NULL, 4, 'Live'),
(22, 'Footer Setting', 'FOOTER_SETTING', 'admin/Footer', 7, NULL, 1, 'Live'),
(23, 'Privacy Policy <br>Terms & Condition', 'PRIVACY_POLICY', 'admin/Footer/privacyPolicy', 7, NULL, 2, 'Deleted'),
(33, 'Service', 'SERVICE', '#', NULL, 'fal fa-cogs', 7, 'Live'),
(34, 'Service Setting', 'SERVICE_SETTING', 'admin/Service', 33, NULL, 3, 'Live'),
(39, 'Statistic', 'STATISTIC', 'admin/Footer/statistic', 7, NULL, 3, 'Live'),
(40, 'FAQ', 'FAQ', 'admin/Faq', NULL, 'fal fa-question', 4, 'Live'),
(41, 'Get In Touch', 'GET_IN_TOUCH', 'admin/Footer/getInTouchSetting', 7, NULL, 4, 'Live'),
(42, 'Project', 'PROJECT', 'admin/Project', NULL, 'fal fa-building', 5, 'Live'),
(43, 'Other Service', 'OTHER_SERVICE', 'admin/Footer/OtherServiceSetting', 7, NULL, 5, 'Live'),
(44, 'Client', 'CLIENT', 'admin/Client', NULL, 'fal fa-user-friends', 6, 'Live'),
(45, 'Hot Deal', 'HOT_DEAL', 'admin/Footer/hotDealSetting', 7, NULL, 6, 'Live'),
(46, 'Get Quote', 'GET_QUOTE', 'admin/Mail/get_quote', 6, NULL, 4, 'Live'),
(47, 'SMS Template', 'SMS_TEMPLATE', 'admin/Mail/smsTemplate', 6, NULL, 6, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_side_menu_rights`
--

DROP TABLE IF EXISTS `tbl_side_menu_rights`;
CREATE TABLE IF NOT EXISTS `tbl_side_menu_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `ref_menu_id` int(11) NOT NULL,
  `full_access` tinyint(4) NOT NULL,
  `view_right` tinyint(4) NOT NULL,
  `add_right` tinyint(4) NOT NULL,
  `edit_right` tinyint(4) NOT NULL,
  `delete_right` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sms_template`
--

DROP TABLE IF EXISTS `tbl_sms_template`;
CREATE TABLE IF NOT EXISTS `tbl_sms_template` (
  `sms_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`sms_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sms_template`
--

INSERT INTO `tbl_sms_template` (`sms_template_id`, `type`, `message`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'get_quote_customer', 'thank you {contact_name} {contact_phone} {contact_email} ', NULL, NULL, NULL, NULL, NULL, NULL, 'Live'),
(3, 'contact_us_customer', 'thank you {contact_name} {contact_phone} {contact_email} ', NULL, NULL, NULL, NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistic`
--

DROP TABLE IF EXISTS `tbl_statistic`;
CREATE TABLE IF NOT EXISTS `tbl_statistic` (
  `statistic_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_1` varchar(250) NOT NULL,
  `desc_1` varchar(250) NOT NULL,
  `title_2` varchar(250) NOT NULL,
  `desc_2` varchar(250) NOT NULL,
  `title_3` varchar(250) NOT NULL,
  `desc_3` varchar(250) NOT NULL,
  `title_4` varchar(250) NOT NULL,
  `desc_4` varchar(250) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`statistic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_statistic`
--

INSERT INTO `tbl_statistic` (`statistic_id`, `title_1`, `desc_1`, `title_2`, `desc_2`, `title_3`, `desc_3`, `title_4`, `desc_4`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, '18', 'Experience', '502', 'Complete Projects', '120', 'Works Employed', '12', 'Running Projects', NULL, NULL, NULL, 1, '::1', '2021-02-15 07:16:01', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistic_item`
--

DROP TABLE IF EXISTS `tbl_statistic_item`;
CREATE TABLE IF NOT EXISTS `tbl_statistic_item` (
  `statistic_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_statistic_id` int(11) NOT NULL,
  `statistic_item_title` varchar(250) NOT NULL,
  `statistic_item_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`statistic_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_statistic_item`
--

INSERT INTO `tbl_statistic_item` (`statistic_item_id`, `ref_statistic_id`, `statistic_item_title`, `statistic_item_desc`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(10, 1, 'Expert VISA Consultation', 'Canopus Global Education  providers of expert VISA Consultancy services and advice for Education, Employment, Immigration and Travel VISA to USA, UK, Canada, Australia, New Zealand and many other countries.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(11, 1, 'Continuous Support', 'We take care that our clients do not leave unsatisfied. That is all because of the staggering support we provide in all the International “services” desired. It’s not just our words, view people saying so!', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(12, 1, 'We Providing', 'Canopus Global Education efficient service providing quality makes it highly reliable.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(13, 1, 'Perfect Documentation', 'We pay keen attention to Perfect Documentation!', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(14, 1, '9+ years of experience', 'We are the pioneers of successful VISA consultation continually benefiting the customers for more than 8 years now.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(15, 1, 'Client Transparency', 'Unlike others, we believe in keeping Client Transparency.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(16, 1, 'Better understand of requirement', 'A very good understanding of the queries and assistance on every move, makes the clients contended of the services catered.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(17, 1, 'Fast and Reliable', 'Fast and Reliable aid is assured here at Agile Consultancy!', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonial`
--

DROP TABLE IF EXISTS `tbl_testimonial`;
CREATE TABLE IF NOT EXISTS `tbl_testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `testimonial_name` varchar(250) NOT NULL,
  `testimonial_description` text NOT NULL,
  `testimonial_image` text NOT NULL,
  `testimonial_rating` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_testimonial`
--

INSERT INTO `tbl_testimonial` (`testimonial_id`, `testimonial_name`, `testimonial_description`, `testimonial_image`, `testimonial_rating`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'â€œJonspond Mendelaâ€?', 'Donec scelerisque dolor id nunc dictum, nterdum mauris rhoncus. Aliquam at ultrices nunc. In sem fermentum at lorem in, porta mauris.', 'assets/images/resources/testi-img1-1.png', 3, 1, 1, '::1', '2020-09-25 07:18:19', 1, '::1', '2021-02-15 14:55:20', 'Live'),
(2, 'â€œBaris Jonsonâ€?', 'Donec scelerisque dolor id nunc dictum, nterdum mauris rhoncus. Aliquam at ultrices nunc. In sem fermentum at lorem in, porta mauris.', 'assets/images/resources/testi-img1-2.png', 4, 1, 1, '::1', '2021-02-15 14:58:28', NULL, NULL, NULL, 'Live'),
(3, 'â€œJonson Barisâ€?', 'Donec scelerisque dolor id nunc dictum, nterdum mauris rhoncus. Aliquam at ultrices nunc. In sem fermentum at lorem in, porta mauris.', 'assets/images/resources/testi-img1-3.png', 2, 1, 1, '::1', '2021-02-15 14:59:03', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `user_information`
--

DROP TABLE IF EXISTS `user_information`;
CREATE TABLE IF NOT EXISTS `user_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` text NOT NULL,
  `password` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `details` varchar(255) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  `InsUser` varchar(200) DEFAULT NULL,
  `InsTerminal` varchar(100) NOT NULL,
  `InsDateTime` datetime NOT NULL,
  `UpdUser` varchar(200) DEFAULT NULL,
  `UpdTerminal` varchar(100) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(20) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_information`
--

INSERT INTO `user_information` (`id`, `user_id`, `user_name`, `user_email`, `password`, `address`, `mobile`, `details`, `role`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, '1', 'Admin', 'admin@weborative.com', 'e1b4755403710e0deb7aa5d45e43996d', 'Surat', '1234567980', '', 'Admin', '1', '::1', '2020-06-20 00:00:00', '1', '::1', '2020-10-01 07:38:18', 'Live');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
