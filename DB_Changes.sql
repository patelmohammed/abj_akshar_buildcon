/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Acer
 * Created: 24-Feb-2021
 */

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Team', 'TEAM', '#', NULL, 'fal fa-user', '12', 'Live');

DROP TABLE IF EXISTS `tbl_team_category`;
CREATE TABLE IF NOT EXISTS `tbl_team_category` (
  `team_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_category_name` varchar(250) NOT NULL,
  `is_active` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`team_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_team`;
CREATE TABLE IF NOT EXISTS `tbl_team` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_team_category_id` int(11) NOT NULL,
  `team_name` varchar(250) NOT NULL,
  `tema_designation` varchar(250) NOT NULL,
  `team_image` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Team Category', 'TEAM_CATEGORY', 'admin/Team', '48', NULL, '1', 'Live'), (NULL, 'Team Member', 'TEAM_MEMBER', 'admin/Team/teamMember', '48', NULL, '2', 'Live');

ALTER TABLE `footer_setting`  ADD `footer_logo_image` TEXT NOT NULL  AFTER `logo_image`;

UPDATE `footer_setting` SET `footer_logo_image` = 'assets/images/logo2.png' WHERE `footer_setting`.`footer_id` = 1;
UPDATE `footer_setting` SET `logo_image` = 'assets/images/logo.png' WHERE `footer_setting`.`footer_id` = 1;


/**
 * Author:  Mohammed
 * Change: 27-02-2021
 */

DROP TABLE IF EXISTS `tbl_service_item`;
CREATE TABLE IF NOT EXISTS `tbl_service_item` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_title` varchar(250) NOT NULL,
  `service_icon` varchar(250) NOT NULL,
  `service_desc` text NOT NULL,
  `service_sub_desc` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Service', 'SERVICE_ITEM', 'admin/Service/serviceItem', '33', NULL, '2', 'Live');
UPDATE `tbl_side_menu` SET `menu_order_no` = '1' WHERE `tbl_side_menu`.`menu_id` = 34;

ALTER TABLE `tbl_service_setting`
  DROP `sub_service_icon_1`,
  DROP `sub_service_title_1`,
  DROP `sub_service_desc_1`,
  DROP `sub_service_desc_1_1`,
  DROP `sub_service_icon_2`,
  DROP `sub_service_title_2`,
  DROP `sub_service_desc_2`,
  DROP `sub_service_desc_2_2`,
  DROP `sub_service_icon_3`,
  DROP `sub_service_title_3`,
  DROP `sub_service_desc_3`,
  DROP `sub_service_desc_3_3`,
  DROP `sub_service_icon_4`,
  DROP `sub_service_title_4`,
  DROP `sub_service_desc_4`,
  DROP `sub_service_desc_4_4`;

/**
 * Author:  Mohammed
 * Change: 05-03-2021
 */


ALTER TABLE `footer_setting`  ADD `contact_title` VARCHAR(250) NOT NULL  AFTER `contact_number`;