<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function dateprint($dt = '') {
    if ($dt == '') {
        return date('d-m-Y');
    } else {
        return date('d-m-Y', strtotime($dt));
    }
}

function dbdate($dt = '') {
    if ($dt == '') {
        return date('Y-m-d');
    } else {
        $old = explode('-', $dt);
        return date('Y-m-d', strtotime($old[2] . '-' . $old[1] . '-' . $old[0]));
    }
}

function preprint($val, $flag = TRUE) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';

    if ($flag) {
        die;
    }
}

function cleanString($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
    return preg_replace('/-+/', '-', $string);
}

function set_selected($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' selected="selected"';
    }
}

function set_checked($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' checked="checked"';
    }
}

function getFuzzy($str_time) {
    $_time_formats = array(
        array(60, 'just now'),
        array(90, '1 minute'),
        array(3600, 'minutes', 60),
        array(5400, '1 hour'),
        array(86400, 'hours', 3600),
        array(129600, '1 day'),
        array(604800, 'days', 86400),
        array(907200, '1 week'),
        array(2628000, 'weeks', 604800),
        array(3942000, '1 month'),
        array(31536000, 'months', 2628000),
        array(47304000, '1 year'),
        array(3153600000, 'years', 31536000)
    );
    $diff = time() - $str_time;
    $val = '';
    if ($str_time <= 0) {
        $val = 'a long time ago';
    } else if ($diff < 0) {
        $val = 'in the future';
    } else {
        foreach ($_time_formats as $format) {
            if ($diff < $format[0]) {
                if (count($format) == 2) {
                    $val = $format[1] . ($format[0] === 60 ? '' : ' ago');
                    break;
                } else {
                    $val = ceil($diff / $format[2]) . ' ' . $format[1] . ' ago';
                    break;
                }
            }
        }
    }
    return $val;
}

function getUserNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT user_name 
            FROM user_information si 
            WHERE user_id = '$id' ";
    $user_name = $CI->db->query($sql)->row();
    return isset($user_name->user_name) && !empty($user_name->user_name) ? $user_name->user_name : '';
}

function allowProfileType($id) {
    $CI = & get_instance();
    $sql = "SELECT IFNULL(p.is_allow_profile_type, 0) AS is_allow_profile_type
            FROM tbl_plan p 
            WHERE plan_id = '$id' ";
    $user_name = $CI->db->query($sql)->row();
    return isset($user_name->is_allow_profile_type) && !empty($user_name->is_allow_profile_type) ? $user_name->is_allow_profile_type : 0;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function getPlanName($id) {
    $CI = & get_instance();
    $sql = "SELECT plan_name 
            FROM tbl_plan p 
            WHERE plan_id = '$id' ";
    $PlanData = $CI->db->query($sql)->row();
    return isset($PlanData->plan_name) && !empty($PlanData->plan_name) ? $PlanData->plan_name : '';
}

function getPlanServiceLimit($id) {
    $CI = & get_instance();
    $sql = "SELECT IFNULL(service_limit, 0) AS service_limit
            FROM tbl_plan p 
            WHERE plan_id = '$id' ";
    $PlanData = $CI->db->query($sql)->row();
    return isset($PlanData->service_limit) && !empty($PlanData->service_limit) ? $PlanData->service_limit : 0;
}

function isAllowInsertClient($id) {
    $CI = & get_instance();
    $sql = "SELECT IFNULL(is_allow_insert_client, 0) AS is_allow_insert_client
            FROM tbl_plan p 
            WHERE plan_id = '$id' ";
    $PlanData = $CI->db->query($sql)->row();
    return isset($PlanData->is_allow_insert_client) && !empty($PlanData->is_allow_insert_client) ? $PlanData->is_allow_insert_client : 0;
}

function getPlanData($id) {
    $CI = & get_instance();
    $sql = "SELECT IFNULL(product_limit, 0) AS product_limit, IFNULL(address_limit, 0) AS address_limit, IFNULL(service_limit, 0) AS service_limit, IFNULL(payment_info_limit, 0) AS payment_info_limit, IFNULL(item_limit, 0) AS item_limit, IFNULL(client_limit, 0) AS client_limit, IFNULL(gallery_category_limit, 0) AS gallery_category_limit 
            FROM tbl_plan p 
            WHERE plan_id = '$id' ";
    $PlanData = $CI->db->query($sql)->row();
    return isset($PlanData) && !empty($PlanData) ? $PlanData : 0;
}

function getCountryCode($id) {
    $CI = & get_instance();
    $sql = "SELECT IFNULL(phonecode, 0) AS phonecode
            FROM tbl_country c 
            WHERE c.id = '$id' ";
    $phonecode = $CI->db->query($sql)->row();
    return isset($phonecode->phonecode) && !empty($phonecode->phonecode) ? $phonecode->phonecode : 0;
}

function encodeId($id) {
    $num = mt_rand(10, 99) . $id . mt_rand(10, 99);
    $result = $num;

//    $num = $id * 148;
//    $num = 'A' . $num . 'Y';
//    $result = $num;
    return $result;
}

function decodeId($id) {
    $num = substr($id, 2);
    $num = substr($num, 0, -2);
    $result = $num;

//    $num = str_replace('A', '', $id);
//    $num = str_replace('Y', '', $num);
//    $num = $num / 148;
//    $result = $num;
    return $result;
}

function getTeamCategoryName($id) {
    $CI = & get_instance();
    $sql = "SELECT team_category_name 
            FROM tbl_team_category 
            WHERE team_category_id = '$id' ";
    $teamCategory = $CI->db->query($sql)->row();
    return isset($teamCategory->team_category_name) && !empty($teamCategory->team_category_name) ? $teamCategory->team_category_name : '';
}
