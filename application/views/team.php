<section class="pb-50">
    <div class="w-100 pt-170 pb-150 dark-layer3 opc7 position-relative">
        <div class="fixed-bg" style="background-image: url(assets/images/pagetop-bg.jpg);"></div>
        <div class="container">
            <div class="page-top-wrap w-100">
                <h1 class="mb-0">Team Page</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>" title="">Home</a></li>
                    <li class="breadcrumb-item active">Team</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<?php
if (isset($team_data) && !empty($team_data)) {
    $cnt = 0;
    foreach ($team_data as $k1 => $v1) {
        if (isset($v1->team_member_data) && !empty($v1->team_member_data)) {

            $section_bg = ($cnt % 2) != 0 ? 'gray-layer' : '';
            ?>
            <section class="<?= $section_bg ?>">
                <div class="w-100 pt-50 position-relative">
                    <div class="container">

                        <h2 class="mb-0 thm-clr" style="padding-bottom: 2.8125rem;text-align: center;"><?= isset($v1->team_category_name) && !empty($v1->team_category_name) ? $v1->team_category_name : '' ?></h2>
                        <ul class="team-wrap d-flex flex-wrap justify-content-center w-100 list-unstyled mb-0">
                            <?php
                            foreach ($v1->team_member_data as $k2 => $v2) {
                                if (isset($v2->team_image) && !empty($v2->team_image) && file_exists($v2->team_image)) {
                                    ?>
                                    <li class="team-box text-center mb-45 w-100">
                                        <div class="team-img rounded-circle position-relative overflow-hidden">
                                            <img class="rounded-circle img-fluid" src="<?= base_url() . $v2->team_image ?>" alt="Team Image 1">
                                        </div>
                                        <div class="team-info w-100">
                                            <h3 class="mb-0"><?= isset($v2->team_name) && !empty($v2->team_name) ? $v2->team_name : '' ?></h3>
                                            <span class="d-block"><?= isset($v2->tema_designation) && !empty($v2->tema_designation) ? $v2->tema_designation : '' ?></span>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </section>
            <?php
        }
        $cnt++;
    }
}
/*
  if (isset($testimonial_data) && !empty($testimonial_data) && isset($testimonial_data->testimonial_item_data) && !empty($testimonial_data->testimonial_item_data)) {
  ?>
  <section>
  <div class="w-100 gray-layer pt-100 pb-100 opc1 overflow-hidden position-relative">
  <div class="fixed-bg zoom-anim back-blend-screen patern-bg gray-bg" style="background-image: url(assets/images/pattern-bg2.jpg);"></div>
  <div class="container">
  <div class="testi-wrap position-relative w-100">
  <h2 class="mb-0">WHAT CLIENTS SAYS?</h2>
  <div class="testi-caro">
  <?php foreach ($testimonial_data->testimonial_item_data as $k2 => $v2) { ?>
  <div class="testi-box-wrap">
  <div class="testi-box">
  <div class="testi-img">
  <img class="img-fluid" src="<?= base_url() . (isset($v2->testimonial_image) && !empty($v2->testimonial_image) && file_exists($v2->testimonial_image) ? $v2->testimonial_image : '') ?>" alt="Testimonial Image 1">
  </div>
  <div class="testi-info">
  <h3 class="mb-0"><?= isset($v2->testimonial_name) && !empty($v2->testimonial_name) ? $v2->testimonial_name : '' ?></h3>
  <p class="mb-0"><?= isset($v2->testimonial_description) && !empty($v2->testimonial_description) ? $v2->testimonial_description : '' ?></p>
  <span class="d-inline-block text-color3">
  <?php
  if (isset($v2->testimonial_rating) && !empty($v2->testimonial_rating)) {
  for ($i = 1; $i <= $v2->testimonial_rating; $i++) {
  ?>
  <i class="fas fa-star"></i>
  <?php
  }
  for ($j = 1; $j <= (5 - $v2->testimonial_rating); $j++) {
  ?>
  <i class="far fa-star"></i>
  <?php
  }
  }
  ?>
  </span>
  </div>
  </div>
  </div>
  <?php } ?>
  </div>
  </div>
  </div>
  </div>
  </section>
  <?php
  } */
if (isset($client_data) && !empty($client_data)) {
    ?>
    <section>
        <div class="w-100 pb-50 pt-150 position-relative">
            <div class="container">
                <div class="clients-wrap w-100">
                    <div class="team-inner team-caro2">
                        <?php
                        if (isset($client_data) && !empty($client_data)) {
                            foreach ($client_data as $k4 => $v4) {
                                if (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo)) {
                                    ?>
                                    <div class="col-md-3 col-sm-4 col-lg-2">
                                        <div class="client-box w-100">
                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?= base_url() . (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo) ? $v4->client_logo : '') ?>" alt="Client Image <?= $k4 ?>"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
}
if (isset($get_in_touch_data) && !empty($get_in_touch_data)) {
    ?>
    <section>
        <div class="w-100 position-relative">
            <div class="container">
                <div class="getin-touch-wrap overlap-99 brd-rd5 style2 thm-layer opc8 w-100 overflow-hidden position-relative">
                    <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($get_in_touch_data->get_in_touch_image) && !empty($get_in_touch_data->get_in_touch_image) && file_exists($get_in_touch_data->get_in_touch_image) ? $get_in_touch_data->get_in_touch_image : '') ?>);"></div>
                    <div class="row align-items-center justify-content-between">
                        <div class="col-md-7 col-sm-12 col-lg-5">
                            <div class="getin-touch-title w-100">
                                <span class="d-block"><?= isset($get_in_touch_data->get_in_touch_title) && !empty($get_in_touch_data->get_in_touch_title) ? $get_in_touch_data->get_in_touch_title : '' ?></span>
                                <h2 class="mb-0"><?= isset($get_in_touch_data->get_in_touch_desc) && !empty($get_in_touch_data->get_in_touch_desc) ? $get_in_touch_data->get_in_touch_desc : '' ?></h2>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-lg-4">
                            <div class="getin-touch-btn text-right">
                                <a class="thm-btn bg-color1" href="<?= base_url() ?>Contact" title="">Book An Appointment<i class="flaticon-arrow-pointing-to-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>