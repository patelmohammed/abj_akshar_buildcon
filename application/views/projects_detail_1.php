<section>
    <div class="w-100 pt-170 pb-150 dark-layer3 opc7 position-relative">
        <div class="fixed-bg" style="background-image: url(<?= base_url() ?>assets/images/pagetop-bg.jpg);"></div>
        <div class="container">
            <div class="page-top-wrap w-100">
                <h1 class="mb-0"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>" title="">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('Projects') ?>" title="">Projects</a></li>
                    <li class="breadcrumb-item active"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="w-100 pt-100 pb-100 position-relative">
        <div class="container">
            <div class="post-detail-wrap w-100">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-8">
                        <div class="post-detail w-100">
                            <?= isset($project_data->main_desc) && !empty($project_data->main_desc) ? $project_data->main_desc : '' ?>
                            <?php if (isset($project_data->project_image) && !empty($project_data->project_image)) { ?>
                                <div class="post-detail-gallery-video-box position-relative w-100">
                                    <style>
                                        .hello {
                                            opacity: 1 !important;
                                        }
                                        .full {
                                            position: fixed;
                                            left: 0;
                                            top: 0;
                                            width: 100%;
                                            height: 100%;
                                            z-index: 1;
                                        }
                                        .full .content {
                                            background-color: rgba(0,0,0,0.75) !important;
                                            height: 100%;
                                            width: 100%;
                                            display: grid;
                                        }
                                        .full .content img {
                                            left: 50%;
                                            transform: translate3d(0, 0, 0);
                                            animation: zoomin 1s ease;
                                            max-width: 100%;
                                            max-height: 100%;
                                            margin: auto;
                                        }
                                        .byebye {
                                            opacity: 0;
                                        }
                                        .byebye:hover {
                                            transform: scale(0.2) !important;
                                        }
                                        .gallery {
                                            display: grid;
                                            grid-column-gap: 8px;
                                            grid-row-gap: 8px;
                                            grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
                                            grid-auto-rows: 8px;
                                        }
                                        .gallery img {
                                            max-width: 100%;
                                            border-radius: 8px;
                                            box-shadow: 0 0 16px #333;
                                            transition: all 1.5s ease;
                                        }
                                        .gallery img:hover {
                                            box-shadow: 0 0 32px #333;
                                        }
                                        .gallery .content {
                                            padding: 4px;
                                        }
                                        .gallery .gallery-item {
                                            transition: grid-row-start 300ms linear;
                                            transition: transform 300ms ease;
                                            transition: all 0.5s ease;
                                            cursor: pointer;
                                        }
                                        .gallery .gallery-item:hover {
                                            transform: scale(1.025);
                                        }
                                        @media (max-width: 600px) {
                                            .gallery {
                                                grid-template-columns: repeat(auto-fill, minmax(30%, 1fr));
                                            }
                                        }
                                        @media (max-width: 400px) {
                                            .gallery {
                                                grid-template-columns: repeat(auto-fill, minmax(50%, 1fr));
                                            }
                                        }
                                        @-moz-keyframes zoomin {
                                            0% {
                                                max-width: 50%;
                                                transform: rotate(-30deg);
                                                filter: blur(4px);
                                            }
                                            30% {
                                                filter: blur(4px);
                                                transform: rotate(-80deg);
                                            }
                                            70% {
                                                max-width: 50%;
                                                transform: rotate(45deg);
                                            }
                                            100% {
                                                max-width: 100%;
                                                transform: rotate(0deg);
                                            }
                                        }
                                        @-webkit-keyframes zoomin {
                                            0% {
                                                max-width: 50%;
                                                transform: rotate(-30deg);
                                                filter: blur(4px);
                                            }
                                            30% {
                                                filter: blur(4px);
                                                transform: rotate(-80deg);
                                            }
                                            70% {
                                                max-width: 50%;
                                                transform: rotate(45deg);
                                            }
                                            100% {
                                                max-width: 100%;
                                                transform: rotate(0deg);
                                            }
                                        }
                                        @-o-keyframes zoomin {
                                            0% {
                                                max-width: 50%;
                                                transform: rotate(-30deg);
                                                filter: blur(4px);
                                            }
                                            30% {
                                                filter: blur(4px);
                                                transform: rotate(-80deg);
                                            }
                                            70% {
                                                max-width: 50%;
                                                transform: rotate(45deg);
                                            }
                                            100% {
                                                max-width: 100%;
                                                transform: rotate(0deg);
                                            }
                                        }
                                        @keyframes zoomin {
                                            0% {
                                                max-width: 50%;
                                                transform: rotate(-30deg);
                                                filter: blur(4px);
                                            }
                                            30% {
                                                filter: blur(4px);
                                                transform: rotate(-80deg);
                                            }
                                            70% {
                                                max-width: 50%;
                                                transform: rotate(45deg);
                                            }
                                            100% {
                                                max-width: 100%;
                                                transform: rotate(0deg);
                                            }
                                        }
                                    </style>
                                    <div class="gallery" id="gallery">
                                        <?php
                                        foreach ($project_data->project_image as $k6 => $v6) {
                                            if (isset($v6->project_image) && !empty($v6->project_image) && file_exists($v6->project_image)) {
                                                ?>
                                                <div class="gallery-item">
                                                    <div class="content"><img src="<?= base_url() . $v6->project_image ?>" alt=""></div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4">
                        <aside class="sidebar-wrap w-100">
                            <div class="widget2 brd-rd5 w-100">
                                <div class="about-widget text-center w-100">
                                    <div class="about-widget-img d-inline-block">
                                        <?php if (isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);') { ?>
                                            <a class="thm-bg rounded-circle" href="<?= $footer_data->footer_facebook ?>" target="_blank" title=""><i class="fab fa-facebook-f"></i></a>
                                        <?php } ?>
                                        <img class="img-fluid rounded-circle" src="<?= base_url() . (isset($project_data->project_widget) && !empty($project_data->project_widget) ? $project_data->project_widget : '') ?>" alt="Project Widget">
                                    </div>
                                    <h4 class="mb-0"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></h4>
                                    <p class="mb-0"><?= isset($project_data->project_desc) && !empty($project_data->project_desc) ? $project_data->project_desc : '' ?></p>
                                    <span class="d-block"><img class="img-fluid" src="<?= base_url() ?>assets/images/resources/about-widget-icon1.png" alt="About Widget Icon 1"><?= isset($project_data->project_location) && !empty($project_data->project_location) ? $project_data->project_location : '' ?></span>
                                </div>
                            </div>
                            <?php if (isset($all_project_data) && !empty($all_project_data)) { ?>
                                <div class="widget2 category_widget brd-rd5 w-100">
                                    <h3>Projects</h3>
                                    <ul class="mb-0 w-100" style="">
                                        <?php
                                        foreach ($all_project_data as $k3 => $v3) {
                                            $k3++;
                                            if ($v3->project_id != (isset($project_data->project_id) && !empty($project_data->project_id) ? $project_data->project_id : '') && $k3 < 8) {
                                                ?>
                                                <li><a href="<?= base_url('Projects/projectDetail/') . $v3->project_id ?>" title=""><?= isset($v3->project_name) && !empty($v3->project_name) ? $v3->project_name : '' ?></a></li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                            }
                            if (isset($project_data->project_video) && !empty($project_data->project_video)) {
                                ?>
                                <div class="widget2 p-0 video-widget brd-rd5 w-100" >
                                    <div class="widget-video-box w-100 position-relative">
                                        <img class="img-fluid w-100" src="<?= base_url() ?>assets/images/resources/widget-video-img.jpg" alt="Widget Video Image" style="height: 300px;">
                                        <a class="spinner" href="<?= isset($project_data->project_video) && !empty($project_data->project_video) ? $project_data->project_video : '' ?>" data-fancybox title=""><i class="fas fa-plus"></i></a>
                                    </div>
                                </div>
                            <?php } ?>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (isset($client_data) && !empty($client_data)) {
    ?>
    <section>
        <div class="w-100 pb-50 pt-100 position-relative">
            <div class="container">
                <div class="clients-wrap w-100">
                    <div class="row">
                        <?php
                        if (isset($client_data) && !empty($client_data)) {
                            foreach ($client_data as $k4 => $v4) {
                                if (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo)) {
                                    ?>
                                    <div class="col-md-3 col-sm-4 col-lg-2">
                                        <div class="client-box w-100">
                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?= base_url() . ($v4->client_logo) ?>" alt="Client Image <?= $k4 ?>"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<section>
    <div class="w-100 position-relative">
        <div class="container">
            <div class="getin-touch-wrap overlap-99 brd-rd5 style2 thm-layer opc8 w-100 overflow-hidden position-relative">
                <div class="fixed-bg" style="background-image: url(<?= base_url() ?>assets/images/parallax2.jpg);"></div>
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-7 col-sm-12 col-lg-5">
                        <div class="getin-touch-title w-100">
                            <span class="d-block"><?= isset($get_in_touch_data->get_in_touch_title) && !empty($get_in_touch_data->get_in_touch_title) ? $get_in_touch_data->get_in_touch_title : '' ?></span>
                            <h2 class="mb-0"><?= isset($get_in_touch_data->get_in_touch_desc) && !empty($get_in_touch_data->get_in_touch_desc) ? $get_in_touch_data->get_in_touch_desc : '' ?></h2>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-lg-4">
                        <div class="getin-touch-btn text-right">
                            <a class="thm-btn bg-color1" href="<?= base_url() ?>Contact" title="">Book Visit Now<i class="flaticon-arrow-pointing-to-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var gallery = document.querySelector('#gallery');
    var getVal = function (elem, style) {
        return parseInt(window.getComputedStyle(elem).getPropertyValue(style));
    };
    var getHeight = function (item) {
        return item.querySelector('.content').getBoundingClientRect().height;
    };
    var resizeAll = function () {
        var altura = getVal(gallery, 'grid-auto-rows');
        var gap = getVal(gallery, 'grid-row-gap');
        gallery.querySelectorAll('.gallery-item').forEach(function (item) {
            var el = item;
            el.style.gridRowEnd = "span " + Math.ceil((getHeight(item) + gap) / (altura + gap));
        });
    };
    gallery.querySelectorAll('img').forEach(function (item) {
        item.classList.add('byebye');
        if (item.complete) {
            console.log(item.src);
        } else {
            item.addEventListener('load', function () {
                var altura = getVal(gallery, 'grid-auto-rows');
                var gap = getVal(gallery, 'grid-row-gap');
                var gitem = item.parentElement.parentElement;
                gitem.style.gridRowEnd = "span " + Math.ceil((getHeight(gitem) + gap) / (altura + gap));
                item.classList.remove('byebye');
            });
        }
    });
    window.addEventListener('resize', resizeAll);
    gallery.querySelectorAll('.gallery-item').forEach(function (item) {
        item.addEventListener('click', function () {
            item.classList.toggle('full');
        });
    });
</script>