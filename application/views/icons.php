<section class="pb-50">
    <div class="w-100 pt-170 pb-150 dark-layer3 opc7 position-relative">
        <div class="fixed-bg" style="background-image: url(assets/images/pagetop-bg.jpg);"></div>
        <div class="container">
            <div class="page-top-wrap w-100">
                <h1 class="mb-0">Icons</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>" title="">Home</a></li>
                    <li class="breadcrumb-item active">Icons</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="gray-layer">
    <div class="w-100 pt-100 pb-100 position-relative">
        <div class="container">
            <div class="proj-wrap w-100">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-headset"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-headset</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-twitter"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-twitter</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-wheel-saw"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-wheel-saw</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-oil-drum"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-oil-drum</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-shopping-basket"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-shopping-basket</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-google-plus-logo"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-google-plus-logo</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-back"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-back</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-phone-call"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-phone-call</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-mail"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-mail</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-work"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-work</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-worker-1"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-worker-1</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-carpenter"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-carpenter</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-arrow-pointing-to-left"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-arrow-pointing-to-left</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-arrow-pointing-to-right"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-arrow-pointing-to-right</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-car-parts"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-car-parts</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-electric-tower"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-electric-tower</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-tools"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-tools</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-placeholder"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-placeholder</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-facebook"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-facebook</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-play"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-play</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-jigsaw"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-jigsaw</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-file"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-file</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-builder"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-builder</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-quote-hand-drawn-symbol"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-quote-hand-drawn-symbol</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-engineer"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-engineer</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-engineer-1"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-engineer-1</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-engineer-2"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-engineer-2</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-construction"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-construction</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-chat"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-chat</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-next"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-next</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-quote"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-quote</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-worker"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-worker</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-architect"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-architect</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-lg-3" style="padding: 0 0 50px 10px">
                        <div class="proj-box position-relative w-100">
                            <div class="proj-info position-absolute" style="height: 80px;">
                                <i class="flaticon-helmet"></i>
                                <h3 class="mb-0"><a href="javascript:void(0);" title="">flaticon-helmet</a></h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
