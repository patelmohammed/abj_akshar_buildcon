<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-building'></i> Add Project
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Project">Projects</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Project/addEditProject/' . $encrypted_id, $arrayName = array('id' => 'addEditProject', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="project_name">Project Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="project_name" id="project_name" placeholder="Project Name" required value="<?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?>">
                                <div class="invalid-feedback">
                                    Project Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="project_location">Project Location <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="project_location" id="project_location" placeholder="Project Location" required value="<?= isset($project_data->project_location) && !empty($project_data->project_location) ? $project_data->project_location : '' ?>">
                                <div class="invalid-feedback">
                                    Project Location Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="project_video">Project Video URL</label>
                                <input tabindex="2" type="text" class="form-control" name="project_video" id="project_video" placeholder="Project Video URL" value="<?= isset($project_data->project_video) && !empty($project_data->project_video) ? $project_data->project_video : '' ?>">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="project_icon">Icon</label>
                                <input tabindex="2" type="text" class="form-control" name="project_icon" id="project_icon" placeholder="Icon" value="<?= isset($project_data->project_icon) && !empty($project_data->project_icon) ? $project_data->project_icon : 'flaticon-builder' ?>" required="">
                                <div class="invalid-feedback">
                                    Icon Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="project_desc">Project Description <span class="text-danger">*</span></label>
                                <textarea class="form-control address" name="project_desc" id="project_desc" placeholder="Project Description" required rows="5"><?= isset($project_data->project_desc) && !empty($project_data->project_desc) ? $project_data->project_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Project Description Required
                                </div>
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Project Poster Image <i class="text-danger">(File in JPG,PNG) File Size 270x300 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="project_poster_image" class="custom-file-input" id="project_poster_image" <?= isset($project_data->project_poster_image) && !empty($project_data->project_poster_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="project_poster_image">Choose file</label>
                                </div>
                            </div>   
                            <div class="col-md-6 mb-3">
                                <img src="<?= base_url() . (isset($project_data->project_poster_image) && !empty($project_data->project_poster_image) && file_exists($project_data->project_poster_image) ? $project_data->project_poster_image : '') ?>" height="235px" width="150px">
                                <input type="hidden" name="hidden_project_poster_image" id="hidden_project_poster_image" value="<?= isset($project_data->project_poster_image) && !empty($project_data->project_poster_image) ? $project_data->project_poster_image : '' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Project Widget<i class="text-danger">(File in JPG,PNG) File Size 105x105 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="project_widget" class="custom-file-input" id="project_widget" <?= isset($project_data->project_widget) && !empty($project_data->project_widget) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="project_widget">Choose file</label>
                                </div>
                            </div>   
                            <div class="col-md-6 mb-3">
                                <img src="<?= base_url() . (isset($project_data->project_widget) && !empty($project_data->project_widget) && file_exists($project_data->project_widget) ? $project_data->project_widget : '') ?>" height="105px" width="105px">
                                <input type="hidden" name="hidden_project_widget" id="hidden_project_widget" value="<?= isset($project_data->project_widget) && !empty($project_data->project_widget) ? $project_data->project_widget : '' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="main_desc">Project Main Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="main_desc" id="main_desc" placeholder="Project Description" required rows="5"><?= isset($project_data->main_desc) && !empty($project_data->main_desc) ? $project_data->main_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Project Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($project_data->is_active) && !empty($project_data->is_active) ? set_checked($project_data->is_active, 1) : '' ?>>
                                    <label class="custom-control-label" for="is_active">Project Active</label>
                                </div>
                            </div>
                        </div>
                        <hr style="border-bottom: 1px dashed #886ab5;">
                        <div id="project_images_div">
                            <?php
                            if (isset($project_data->project_image) && !empty($project_data->project_image)) {
                                foreach ($project_data->project_image as $key => $value) {
                                    ?>
                                    <div class="form-row project_image_row" id="row_<?= $key ?>">
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label">Project Image <i class="text-danger">(File in JPG,PNG)</i></label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" name="project_image[<?= $key ?>]" class="custom-file-input" id="project_image_<?= $key ?>" <?= isset($value->project_image) && !empty($value->project_image) && file_exists($value->project_image) ? '' : 'required' ?>>
                                                    <label class="custom-file-label" for="project_image_<?= $key ?>"><?= isset($value->project_image) && !empty($value->project_image) && file_exists($value->project_image) ? $value->project_image : 'Choose file' ?></label>
                                                </div>
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_project_image<?= $key ?>" name="is_active_project_image[<?= $key ?>]" type="checkbox" class="custom-control-input" <?= isset($value->is_active) && !empty($value->is_active) && $value->is_active == 1 ? 'checked' : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_project_image<?= $key ?>"></label>
                                                    </div>
                                                </div>
                                                <div class="input-group-append">
                                                    <?php
                                                    if ($key == 0) {
                                                        ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="project_image()" title="Add" data-toggle="tooltip">
                                                            <i class="fal fa-plus"></i>
                                                        </a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_project_image" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                            <i class="fal fa-minus"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <img src="<?= base_url() . (isset($value->project_image) && !empty($value->project_image) && file_exists($value->project_image) ? $value->project_image : '') ?>" height="105px" width="105px">
                                            <input type="hidden" name="hidden_project_image[<?= $key ?>]" id="hidden_project_image" value="<?= isset($value->project_image) && !empty($value->project_image) ? $value->project_image : '' ?>">
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($project_data->project_image) && !empty($project_data->project_image) ? count($project_data->project_image) : 0 ?>';
    $(document).ready(function () {
        CKEDITOR.replace('main_desc', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
        });
        if (suffix == 0) {
            project_image();
        }

        $('#addEditProject').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });

        $(document).on('change', '.check_full_access', function () {
            var i = $(this).data('id');
            if ($(this).is(':checked')) {
                $("#view_" + i).prop("checked", true);
                $("#add_" + i).prop("checked", true);
                $("#edit_" + i).prop("checked", true);
                $("#delete_" + i).prop("checked", true);
            } else {
                $("#view_" + i).prop("checked", false);
                $("#add_" + i).prop("checked", false);
                $("#edit_" + i).prop("checked", false);
                $("#delete_" + i).prop("checked", false);
            }
        });

        $(document).on('change', '#checkall_side_menu', function () {
            if ($(this).is(':checked')) {
                $(".check_all").prop("checked", true);
                $(".check_child").prop("checked", true);
            } else {
                $(".check_all").prop("checked", false);
                $(".check_child").prop("checked", false);
            }
        });

        $(document).on('change', '.check_child', function () {
            var i = $(this).data('id');
            if ($("#view_" + i).is(':checked') && $("#add_" + i).is(':checked') && $("#edit_" + i).is(':checked') && $("#delete_" + i).is(':checked')) {
                $("#full_access_" + i).prop("checked", true);
            } else {
                $("#full_access_" + i).prop("checked", false);
            }
        });
    });


    function project_image() {
        suffix++;
        var row = '';
        row += '<div class="form-row project_image_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label">Project Image <i class="text-danger">(File in JPG,PNG)</i></label>';
        row += '<div class="input-group">';
        row += '<div class="custom-file">';
        row += '<input type="file" name="project_image[' + suffix + ']" class="custom-file-input" id="project_image_' + suffix + '">';
        row += '<label class="custom-file-label" for="project_image_' + suffix + '">Choose file</label>';
        row += '</div>';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_project_image' + suffix + '" name="is_active_project_image[' + suffix + ']" type="checkbox" class="custom-control-input">';
        row += '<label class="custom-control-label fw-500" for="is_active_project_image' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="project_image()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_project_image" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#project_images_div').append(row);
    }

    $(document).on('click', '.remove_project_image', function () {
        var id = $(this).data('id');
        if ($('.project_image_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>