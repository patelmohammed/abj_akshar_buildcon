<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Contact
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Contact', $arrayName = array('id' => 'addEditContact', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_number_1">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="contact_number_1" id="contact_number_1" placeholder="Contact Number" required value="<?= isset($contact_data->contact_number_1) && !empty($contact_data->contact_number_1) ? $contact_data->contact_number_1 : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_number_2">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="contact_number_2" id="contact_number_2" placeholder="Contact Number" required value="<?= isset($contact_data->contact_number_2) && !empty($contact_data->contact_number_2) ? $contact_data->contact_number_2 : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email">Email <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="email" id="email" placeholder="Email" required value="<?= isset($contact_data->email) && !empty($contact_data->email) ? $contact_data->email : '' ?>">
                                <div class="invalid-feedback">
                                    Email Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email_alt">Support Email</label>
                                <input tabindex="2" type="text" class="form-control textonly" name="email_alt" id="email_alt" placeholder="Support Email" value="<?= isset($contact_data->email_alt) && !empty($contact_data->email_alt) ? $contact_data->email_alt : '' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                <textarea class="form-control address" rows="5" name="address" id="address" required=""><?= isset($contact_data->address) && !empty($contact_data->address) ? $contact_data->address : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Address Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="map">Map <span class="text-danger">*</span></label>
                                <textarea class="form-control address" rows="5" name="map" id="map" required=""><?= isset($contact_data->map) && !empty($contact_data->map) ? $contact_data->map : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Map Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_us_title">Contact Us Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="contact_us_title" id="contact_us_title" placeholder="Contact Us Title" required value="<?= isset($contact_data->contact_us_title) && !empty($contact_data->contact_us_title) ? $contact_data->contact_us_title : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Us Title Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_us_desc">Contact Us Description <span class="text-danger">*</span></label>
                                <textarea rows="3" type="text" class="form-control textonly" name="contact_us_desc" id="contact_us_desc" placeholder="Contact Us Description" required><?= isset($contact_data->contact_us_desc) && !empty($contact_data->contact_us_desc) ? $contact_data->contact_us_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Contact Us Description Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_us_sub_desc">Contact Us Sub Description <span class="text-danger">*</span></label>
                                <textarea rows="3" type="text" class="form-control textonly" name="contact_us_sub_desc" id="contact_us_sub_desc" placeholder="Contact US Sub Description" required><?= isset($contact_data->contact_us_sub_desc) && !empty($contact_data->contact_us_sub_desc) ? $contact_data->contact_us_sub_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Contact Us Sub Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x410 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($contact_data->background_image) && !empty($contact_data->background_image) ? $contact_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($contact_data->background_image) && !empty($contact_data->background_image) ? $contact_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditContact').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>