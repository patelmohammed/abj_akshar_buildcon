<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Service Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Service', $arrayName = array('id' => 'addEditServiceSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_title_1">Service Title <span class="text-danger">*</span></label>
                                <input type="text" name="service_title_1" id="service_title_1" class="form-control" required="" placeholder="Service Title" value="<?= isset($service_setting_data->service_title_1) && !empty($service_setting_data->service_title_1) ? $service_setting_data->service_title_1 : '' ?>">
                                <div class="invalid-feedback">
                                    Service Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_title_2">Service Title 2 <span class="text-danger">*</span></label>
                                <input type="text" name="service_title_2" id="service_title_2" class="form-control" required="" placeholder="Service Title" value="<?= isset($service_setting_data->service_title_2) && !empty($service_setting_data->service_title_2) ? $service_setting_data->service_title_2 : '' ?>">
                                <div class="invalid-feedback">
                                    Service Title Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_desc">Service Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="service_desc" id="service_desc" class="form-control" required="" placeholder="Service Description"><?= isset($service_setting_data->service_desc) && !empty($service_setting_data->service_desc) ? $service_setting_data->service_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Service Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x630 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($service_setting_data->background_image) && !empty($service_setting_data->background_image) && file_exists($service_setting_data->background_image) ? $service_setting_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($service_setting_data->background_image) && !empty($service_setting_data->background_image) ? $service_setting_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditServiceSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>