<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-building'></i> Add Service
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Service/serviceItem">Service</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Service/addEditServiceItem', $arrayName = array('id' => 'addEditServiceItem', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_title">Service Name <span class="text-danger">*</span></label>
                                <textarea rows="1" class="form-control textonly" name="service_title" id="service_title" placeholder="Service Name" required></textarea>
                                <div class="invalid-feedback">
                                    Service Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_icon">Icon</label>
                                <input tabindex="2" type="text" class="form-control textonly" name="service_icon" id="service_icon" placeholder="Icon" value="flaticon-carpenter" required="">
                                <div class="invalid-feedback">
                                    Icon Required
                                </div>
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_desc">Service Description <span class="text-danger">*</span></label>
                                <textarea class="form-control address" name="service_desc" id="service_desc" placeholder="Service Description" required rows="3"></textarea>
                                <div class="invalid-feedback">
                                    Service Description Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_sub_desc">Service Sub Description <span class="text-danger">*</span></label>
                                <textarea class="form-control address" name="service_sub_desc" id="service_sub_desc" placeholder="Service Sub Description" required rows="3"></textarea>
                                <div class="invalid-feedback">
                                    Service Sub Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                    <label class="custom-control-label" for="is_active">Service Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = 0;
    $(document).ready(function () {
        $('#addEditServiceItem').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>