<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Faq
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Faq', $arrayName = array('id' => 'addEditFaq', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="faq_title">FAQ Title <span class="text-danger">*</span></label>
                                <input type="text" name="faq_title" id="faq_title" class="form-control textonly" required="" value="<?= isset($faq_data->faq_title) && !empty($faq_data->faq_title) ? $faq_data->faq_title : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="faq_desc">FAQ Description <span class="text-danger">*</span></label>
                                <input type="text" name="faq_desc" id="faq_desc" class="form-control" required="" value="<?= isset($faq_data->faq_desc) && !empty($faq_data->faq_desc) ? $faq_data->faq_desc : '' ?>">
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in PNG) File Size 568x519 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="faq_image" class="custom-file-input" id="faq_image" <?= isset($faq_data->faq_image) && !empty($faq_data->faq_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="faq_image">Choose file</label>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <img src="<?= base_url() . (isset($faq_data->faq_image) && !empty($faq_data->faq_image) && file_exists($faq_data->faq_image) ? $faq_data->faq_image : '') ?>" height="100px" width="120px" class="m-2">
                                <input type="hidden" name="hidden_faq_image" id="hidden_faq_image" value="<?= isset($faq_data->faq_image) && !empty($faq_data->faq_image) ? $faq_data->faq_image : '' ?>">
                            </div>
                        </div>
                        <hr style="border-bottom: 1px dashed #886ab5;">
                        <div id="faq_item_div">
                            <?php
                            if (isset($faq_data->faq_item_data) && !empty($faq_data->faq_item_data)) {
                                foreach ($faq_data->faq_item_data as $key => $value) {
                                    $key++;
                                    ?>
                                    <div class="form-row faq_item_row" id="row_<?= $key ?>">
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="faq_item_title_<?= $key ?>">FAQ Item Title <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <input tabindex="2" type="text" class="form-control textonly" name="faq_item_title[<?= $key ?>]" id="faq_item_title_<?= $key ?>" placeholder="FAQ Title" required value="<?= isset($value->faq_item_title) && !empty($value->faq_item_title) ? $value->faq_item_title : '' ?>">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_faq_item_<?= $key ?>" name="is_active_faq_item[<?= $key ?>]" type="checkbox" class="custom-control-input" <?= isset($value->is_active) && !empty($value->is_active) ? (set_checked($value->is_active, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_faq_item_<?= $key ?>"></label>
                                                    </div>
                                                </div>
                                                <div class="input-group-append">
                                                    <?php if ($key == 1) { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="faq_item()" title="Add" data-toggle="tooltip">
                                                            <i class="fal fa-plus"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_faq_item" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                            <i class="fal fa-minus"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="invalid-feedback">
                                                FAQ Item Title Required
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="faq_item_desc_<?= $key ?>">FAQ Item Description <span class="text-danger">*</span></label>
                                            <textarea class="form-control textonly" rows="3" name="faq_item_desc[<?= $key ?>]" id="faq_item_desc_<?= $key ?>" placeholder="FAQ Description" required><?= isset($value->faq_item_desc) && !empty($value->faq_item_desc) ? $value->faq_item_desc : '' ?></textarea>
                                            <div class="invalid-feedback">
                                                FAQ Item Description Required
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($faq_data->faq_item_data) && !empty($faq_data->faq_item_data) ? count($faq_data->faq_item_data) : 0 ?>';
    $(document).ready(function () {
        if (suffix == 0) {
            faq_item();
        }

        $('#addEditFaq').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function faq_item() {
        suffix++;
        var row = '';
        row += '<div class="form-row faq_item_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="faq_item_title_' + suffix + '">FAQ Item Title <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="faq_item_title[' + suffix + ']" id="faq_item_title_' + suffix + '" placeholder="FAQ Title" required value="">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_faq_item_' + suffix + '" name="is_active_faq_item[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_faq_item_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="faq_item()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_faq_item" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '<div class="invalid-feedback">';
        row += 'FAQ Item Title Required';
        row += '</div>';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="faq_item_desc_' + suffix + '">FAQ Item Description <span class="text-danger">*</span></label>';
        row += '<textarea class="form-control textonly" rows="3" name="faq_item_desc[' + suffix + ']" id="faq_item_desc_' + suffix + '" placeholder="FAQ Description" required></textarea>';
        row += '<div class="invalid-feedback">';
        row += 'FAQ Item Description Required';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#faq_item_div').append(row);
    }

    $(document).on('click', '.remove_faq_item', function () {
        var id = $(this).data('id');
        if ($('.faq_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>