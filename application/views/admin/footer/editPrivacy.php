<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Privacy Policy
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Footer/privacyPolicy">Privacy Setting</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Footer/addEditprivacyPolicy/' . $encrypted_id, $arrayName = array('id' => 'addEditprivacyPolicy', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="title">Page Title <span class="text-danger">*</span></label>
                                <input type="text" name="title" id="title" class="form-control textonly" required="" value="<?= isset($privacy_policy_data->page_title) && !empty($privacy_policy_data->page_title) ? $privacy_policy_data->page_title : '' ?>">
                                <div class="invalid-feedback">
                                    Page Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($privacy_policy_data->background_image) && !empty($privacy_policy_data->background_image) ? $privacy_policy_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($privacy_policy_data->background_image) && !empty($privacy_policy_data->background_image) ? $privacy_policy_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="page_desc">Page Description <span class="text-danger">*</span></label>
                                <textarea class="form-control js-summernote" name="page_desc" id="page_desc" required=""><?= isset($privacy_policy_data->page_desc) && !empty($privacy_policy_data->page_desc) ? $privacy_policy_data->page_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Page Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditprivacyPolicy').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>