<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Get In Touch
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Footer/getInTouchSetting', $arrayName = array('id' => 'addEditStatistic', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="get_in_touch_title">Title <span class="text-danger">*</span></label>
                                <input type="text" name="get_in_touch_title" id="get_in_touch_title" class="form-control address" required="" value="<?= isset($setting_data->get_in_touch_title) && !empty($setting_data->get_in_touch_title) ? $setting_data->get_in_touch_title : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="get_in_touch_desc">Description <span class="text-danger">*</span></label>
                                <input type="text" name="get_in_touch_desc" id="get_in_touch_desc" class="form-control address" required="" value="<?= isset($setting_data->get_in_touch_desc) && !empty($setting_data->get_in_touch_desc) ? $setting_data->get_in_touch_desc : '' ?>">
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="get_in_touch_sub_desc">Sub Description <span class="text-danger">*</span></label>
                                <input type="text" name="get_in_touch_sub_desc" id="get_in_touch_sub_desc" class="form-control address" required="" value="<?= isset($setting_data->get_in_touch_sub_desc) && !empty($setting_data->get_in_touch_sub_desc) ? $setting_data->get_in_touch_sub_desc : '' ?>">
                                <div class="invalid-feedback">
                                    Sub Description Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in PNG) File Size 1920x370 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="get_in_touch_image" class="custom-file-input" id="get_in_touch_image" <?= isset($setting_data->get_in_touch_image) && !empty($setting_data->get_in_touch_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="get_in_touch_image">Choose file</label>
                                </div>
                                <img src="<?= base_url() . (isset($setting_data->get_in_touch_image) && !empty($setting_data->get_in_touch_image) ? $setting_data->get_in_touch_image : '') ?>" height="100px" width="120px" class="m-2">
                                <input type="hidden" name="hidden_get_in_touch_image" id="hidden_get_in_touch_image" value="<?= isset($setting_data->get_in_touch_image) && !empty($setting_data->get_in_touch_image) ? $setting_data->get_in_touch_image : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($setting_data->statistic_item_data) && !empty($setting_data->statistic_item_data) ? count($setting_data->statistic_item_data) : 0 ?>';
    $(document).ready(function () {
        if (suffix == 0) {
            statistic_item();
        }

        CKEDITOR.replace('main_desc', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
        });

        $('#addEditStatistic').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function statistic_item() {
        suffix++;
        var row = '';
        row += '<div class="form-row statistic_item_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="statistic_item_title_' + suffix + '">Statistic Item Title <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="statistic_item_title[' + suffix + ']" id="statistic_item_title_' + suffix + '" placeholder="Statistic Item Title" required value="">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_service_item_' + suffix + '" name="is_active_service_item[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_service_item_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="statistic_item()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_statistic_item" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '<div class="invalid-feedback">';
        row += 'Statistic Item Title Required';
        row += '</div>';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="statistic_item_desc_' + suffix + '">Statistic Item Description <span class="text-danger">*</span></label>';
        row += '<textarea class="form-control textonly" rows="3" name="statistic_item_desc[' + suffix + ']" id="statistic_item_desc_' + suffix + '" placeholder="Statistic Item Description" required></textarea>';
        row += '<div class="invalid-feedback">';
        row += 'Statistic Item Description Required';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#statistic_item_div').append(row);
    }

    $(document).on('click', '.remove_statistic_item', function () {
        var id = $(this).data('id');
        if ($('.statistic_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>