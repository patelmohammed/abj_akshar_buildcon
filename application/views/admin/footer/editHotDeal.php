<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Hot Deal
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Footer/hotDealSetting', $arrayName = array('id' => 'addEditHotDeal', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_title">Title <span class="text-danger">*</span></label>
                                <textarea rows="1" name="hot_deal_title" id="hot_deal_title" class="form-control address" required=""><?= isset($setting_data->hot_deal_title) && !empty($setting_data->hot_deal_title) ? $setting_data->hot_deal_title : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_desc">Description <span class="text-danger">*</span></label>
                                <textarea rows="1" name="hot_deal_desc" id="hot_deal_desc" class="form-control address" required=""><?= isset($setting_data->hot_deal_desc) && !empty($setting_data->hot_deal_desc) ? $setting_data->hot_deal_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_sub_title_1">Title <span class="text-danger">*</span></label>
                                <input type="text" name="hot_deal_sub_title_1" id="hot_deal_sub_title_1" class="form-control address" required="" value="<?= isset($setting_data->hot_deal_sub_title_1) && !empty($setting_data->hot_deal_sub_title_1) ? $setting_data->hot_deal_sub_title_1 : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_sub_desc_1">Description <span class="text-danger">*</span></label>
                                <textarea rows="1" name="hot_deal_sub_desc_1" id="hot_deal_sub_desc_1" class="form-control address" required=""><?= isset($setting_data->hot_deal_sub_desc_1) && !empty($setting_data->hot_deal_sub_desc_1) ? $setting_data->hot_deal_sub_desc_1 : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_sub_title_2">Title <span class="text-danger">*</span></label>
                                <input type="text" name="hot_deal_sub_title_2" id="hot_deal_sub_title_2" class="form-control address" required="" value="<?= isset($setting_data->hot_deal_sub_title_2) && !empty($setting_data->hot_deal_sub_title_2) ? $setting_data->hot_deal_sub_title_2 : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_sub_desc_2">Description <span class="text-danger">*</span></label>
                                <textarea rows="1" name="hot_deal_sub_desc_2" id="hot_deal_sub_desc_2" class="form-control address" required=""><?= isset($setting_data->hot_deal_sub_desc_2) && !empty($setting_data->hot_deal_sub_desc_2) ? $setting_data->hot_deal_sub_desc_2 : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_sub_title_3">Title <span class="text-danger">*</span></label>
                                <input type="text" name="hot_deal_sub_title_3" id="hot_deal_sub_title_3" class="form-control address" required="" value="<?= isset($setting_data->hot_deal_sub_title_3) && !empty($setting_data->hot_deal_sub_title_3) ? $setting_data->hot_deal_sub_title_3 : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_sub_desc_3">Description <span class="text-danger">*</span></label>
                                <textarea rows="1" name="hot_deal_sub_desc_3" id="hot_deal_sub_desc_3" class="form-control address" required=""><?= isset($setting_data->hot_deal_sub_desc_3) && !empty($setting_data->hot_deal_sub_desc_3) ? $setting_data->hot_deal_sub_desc_3 : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="hot_deal_main_title">Mail Title <span class="text-danger">*</span></label>
                                <input type="text" name="hot_deal_main_title" id="hot_deal_main_title" class="form-control address" required="" value="<?= isset($setting_data->hot_deal_main_title) && !empty($setting_data->hot_deal_main_title) ? $setting_data->hot_deal_main_title : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in PNG) File Size 570x434 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="hot_deal_image" class="custom-file-input" id="hot_deal_image" <?= isset($setting_data->hot_deal_image) && !empty($setting_data->hot_deal_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="hot_deal_image">Choose file</label>
                                </div>
                                <img src="<?= base_url() . (isset($setting_data->hot_deal_image) && !empty($setting_data->hot_deal_image) ? $setting_data->hot_deal_image : '') ?>" height="100px" width="120px" class="m-2">
                                <input type="hidden" name="hidden_hot_deal_image" id="hidden_hot_deal_image" value="<?= isset($setting_data->hot_deal_image) && !empty($setting_data->hot_deal_image) ? $setting_data->hot_deal_image : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditHotDeal').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>