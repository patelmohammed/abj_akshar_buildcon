<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Footer Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Footer', $arrayName = array('id' => 'addEditFooterSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_desc">Description <span class="text-danger">*</span></label>
                                <textarea rows="1" name="footer_desc" id="footer_desc" class="form-control textonly" required=""><?= isset($footer_data->footer_desc) && !empty($footer_data->footer_desc) ? $footer_data->footer_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address">Address <span class="text-danger">*</span></label>
                                <textarea name="address" id="address" rows="1" maxlength="200" class="form-control address" required=""><?= isset($footer_data->address) && !empty($footer_data->address) ? $footer_data->address : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Address Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email">Email <span class="text-danger">*</span></label>
                                <input type="email" name="email" id="email" class="form-control" value="<?= isset($footer_data->email) && !empty($footer_data->email) ? $footer_data->email : '' ?>" required="">
                                <div class="invalid-feedback">
                                    Email Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_title">Header Contact Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="contact_title" id="contact_title" placeholder="Contact Title" required value="<?= isset($footer_data->contact_title) && !empty($footer_data->contact_title) ? $footer_data->contact_title : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_number">Header Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="contact_number" id="contact_number" placeholder="Contact Number" required value="<?= isset($footer_data->contact_number) && !empty($footer_data->contact_number) ? $footer_data->contact_number : '' ?>">
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="total_visitor">Total Visitor</label>
                                <input type="text" name="total_visitor" id="total_visitor" class="form-control numbersonly" value="<?= isset($footer_data->total_visitor) && !empty($footer_data->total_visitor) ? $footer_data->total_visitor : '' ?>">
                                <div class="invalid-feedback">
                                    Total Visitor Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="unique_visitor">Unique Visitor</label>
                                <input type="text" name="unique_visitor" id="unique_visitor" class="form-control numbersonly" value="<?= isset($footer_data->unique_visitor) && !empty($footer_data->unique_visitor) ? $footer_data->unique_visitor : '' ?>">
                                <div class="invalid-feedback">
                                    Unique Visitor Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_facebook">Facebook</label>
                                <input type="text" name="footer_facebook" id="footer_facebook" class="form-control" value="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? $footer_data->footer_facebook : '' ?>">
                                <div class="invalid-feedback">
                                    Facebook Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_twitter">Twitter</label>
                                <input type="text" name="footer_twitter" id="footer_twitter" class="form-control" value="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? $footer_data->footer_twitter : '' ?>">
                                <div class="invalid-feedback">
                                    Twitter Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_insta">Instagram</label>
                                <input type="text" name="footer_insta" id="footer_insta" class="form-control" value="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? $footer_data->footer_insta : '' ?>">
                                <div class="invalid-feedback">
                                    Instagram Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_google">Google</label>
                                <input type="text" name="footer_google" id="footer_google" class="form-control" value="<?= isset($footer_data->footer_google) && !empty($footer_data->footer_google) && $footer_data->footer_google != 'javascript:void(0);' ? $footer_data->footer_google : '' ?>">
                                <div class="invalid-feedback">
                                    Google Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Logo <i class="text-danger">(File in PNG) File Size 148x50 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="logo" class="custom-file-input" id="logo" <?= isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="logo">Choose file</label>
                                </div>
                                <img src="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? $footer_data->logo_image : '') ?>" height="100px" width="120px" class="m-2">
                                <input type="hidden" name="hidden_logo" id="hidden_logo" value="<?= isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? $footer_data->logo_image : '' ?>">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Footer Logo <i class="text-danger">(File in PNG) File Size 148x50 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="footer_logo" class="custom-file-input" id="footer_logo" <?= isset($footer_data->footer_logo_image) && !empty($footer_data->footer_logo_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="footer_logo">Choose file</label>
                                </div>
                                <img src="<?= base_url() . (isset($footer_data->footer_logo_image) && !empty($footer_data->footer_logo_image) ? $footer_data->footer_logo_image : '') ?>" height="100px" width="120px" class="m-2">
                                <input type="hidden" name="hidden_footer_logo" id="hidden_footer_logo" value="<?= isset($footer_data->footer_logo_image) && !empty($footer_data->footer_logo_image) ? $footer_data->footer_logo_image : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditFooterSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>