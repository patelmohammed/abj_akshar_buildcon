<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Slider
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Slider">Slider</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Slider/addEditSlider', $arrayName = array('id' => 'addEditSlider', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="slider_title">Slider Title <span class="text-danger">*</span></label>
                                <textarea class="form-control address2" name="slider_title" id="slider_title" placeholder="Slider Title" required=""></textarea>
                                <div class="invalid-feedback">
                                    Slider Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="slider_description">Slider Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="slider_description" id="slider_description" placeholder="Slider Description" required value="">
                                <div class="invalid-feedback">
                                    Slider Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Slider Image <i class="text-danger">(File in JPG,PNG) File Size 1920x690px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="slider_image" class="custom-file-input" id="slider_image" required="">
                                    <label class="custom-file-label" for="slider_image">Choose file</label>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="order_number">Order Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="order_number" id="order_number" placeholder="Slider Order Number" required value="">
                                <div class="invalid-feedback">
                                    Order Number Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditSlider').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>