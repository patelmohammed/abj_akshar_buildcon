<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit About
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/About', $arrayName = array('id' => 'addEditAboutUs', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="tab-content py-3">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_title_1">Title <span class="text-danger">*</span></label>
                                    <textarea class="form-control textonly" rows="1" name="about_us_title_1" id="about_us_title_1" placeholder="Title" required><?= isset($about_us_data->about_us_title_1) && !empty($about_us_data->about_us_title_1) ? $about_us_data->about_us_title_1 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Title Required
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_title_2">Title 2<span class="text-danger">*</span></label>
                                    <textarea class="form-control textonly" rows="1" name="about_us_title_2" id="about_us_title_2" placeholder="Title" required><?= isset($about_us_data->about_us_title_2) && !empty($about_us_data->about_us_title_2) ? $about_us_data->about_us_title_2 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Title Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_desc_1">Description <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" rows="2" name="about_us_desc_1" id="about_us_desc_1" required=""><?= isset($about_us_data->about_us_desc_1) && !empty($about_us_data->about_us_desc_1) ? $about_us_data->about_us_desc_1 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_desc_2">Description <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" rows="2" name="about_us_desc_2" id="about_us_desc_2" required=""><?= isset($about_us_data->about_us_desc_2) && !empty($about_us_data->about_us_desc_2) ? $about_us_data->about_us_desc_2 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-2 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_icon_1">Icon <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control textonly" name="about_us_alt_sub_icon_1" id="about_us_alt_sub_icon_1" placeholder="About Us Title" required value="<?= isset($about_us_data->about_us_alt_sub_icon_1) && !empty($about_us_data->about_us_alt_sub_icon_1) ? $about_us_data->about_us_alt_sub_icon_1 : '' ?>">
                                    <div class="invalid-feedback">
                                        Icon Required
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_title_1">Title <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" name="about_us_alt_sub_title_1" id="about_us_alt_sub_title_1" rows="1" required="" placeholder="Title"><?= isset($about_us_data->about_us_alt_sub_title_1) && !empty($about_us_data->about_us_alt_sub_title_1) ? $about_us_data->about_us_alt_sub_title_1 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Title Required
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_desc_1">Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control address" name="about_us_alt_sub_desc_1" id="about_us_alt_sub_desc_1" required="" placeholder="Description" value="<?= isset($about_us_data->about_us_alt_sub_desc_1) && !empty($about_us_data->about_us_alt_sub_desc_1) ? $about_us_data->about_us_alt_sub_desc_1 : '' ?>">
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-2 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_icon_2">Icon <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control textonly" name="about_us_alt_sub_icon_2" id="about_us_alt_sub_icon_2" placeholder="Icon" required value="<?= isset($about_us_data->about_us_alt_sub_icon_2) && !empty($about_us_data->about_us_alt_sub_icon_2) ? $about_us_data->about_us_alt_sub_icon_2 : '' ?>">
                                    <div class="invalid-feedback">
                                        Icon Required
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_title_2">Title <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" name="about_us_alt_sub_title_2" rows="1" id="about_us_alt_sub_title_2" required="" placeholder="Title"><?= isset($about_us_data->about_us_alt_sub_title_2) && !empty($about_us_data->about_us_alt_sub_title_2) ? $about_us_data->about_us_alt_sub_title_2 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Title Required
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_desc_2">Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control address" name="about_us_alt_sub_desc_2" id="about_us_alt_sub_desc_2" required="" placeholder="Description" value="<?= isset($about_us_data->about_us_alt_sub_desc_2) && !empty($about_us_data->about_us_alt_sub_desc_2) ? $about_us_data->about_us_alt_sub_desc_2 : '' ?>">
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-2 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_icon_3">Icon <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control textonly" name="about_us_alt_sub_icon_3" id="about_us_alt_sub_icon_3" placeholder="Icon" required value="<?= isset($about_us_data->about_us_alt_sub_icon_3) && !empty($about_us_data->about_us_alt_sub_icon_3) ? $about_us_data->about_us_alt_sub_icon_3 : '' ?>">
                                    <div class="invalid-feedback">
                                        Icon Required
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_title_3">Title <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" name="about_us_alt_sub_title_3" rows="1" id="about_us_alt_sub_title_3" required="" placeholder="Title"><?= isset($about_us_data->about_us_alt_sub_title_3) && !empty($about_us_data->about_us_alt_sub_title_3) ? $about_us_data->about_us_alt_sub_title_3 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Title Required
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_desc_3">Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control address" name="about_us_alt_sub_desc_3" id="about_us_alt_sub_desc_3" required="" placeholder="Description" value="<?= isset($about_us_data->about_us_alt_sub_desc_3) && !empty($about_us_data->about_us_alt_sub_desc_3) ? $about_us_data->about_us_alt_sub_desc_3 : '' ?>">
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-2 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_icon_4">Icon <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control textonly" name="about_us_alt_sub_icon_4" id="about_us_alt_sub_icon_4" placeholder="Icon" required value="<?= isset($about_us_data->about_us_alt_sub_icon_4) && !empty($about_us_data->about_us_alt_sub_icon_4) ? $about_us_data->about_us_alt_sub_icon_4 : '' ?>">
                                    <div class="invalid-feedback">
                                        Icon Required
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_title_4">Title <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" name="about_us_alt_sub_title_4" rows="1" id="about_us_alt_sub_title_4" required="" placeholder="Title"><?= isset($about_us_data->about_us_alt_sub_title_4) && !empty($about_us_data->about_us_alt_sub_title_4) ? $about_us_data->about_us_alt_sub_title_4 : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Title Required
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="about_us_alt_sub_desc_4">Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control address" name="about_us_alt_sub_desc_4" id="about_us_alt_sub_desc_4" required="" placeholder="Description" value="<?= isset($about_us_data->about_us_alt_sub_desc_4) && !empty($about_us_data->about_us_alt_sub_desc_4) ? $about_us_data->about_us_alt_sub_desc_4 : '' ?>">
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label" for="image_title">Image Title <span class="text-danger">*</span></label>
                                    <textarea class="form-control textonly" name="image_title" rows="1" id="image_title" placeholder="Image Title" required><?= isset($about_us_data->image_title) && !empty($about_us_data->image_title) ? $about_us_data->image_title : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Image Title Required
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label">Image <i class="text-danger">(File in PNG) File Size 970x376 px</i></label>
                                    <div class="custom-file">
                                        <input type="file" name="about_us_image_1" class="custom-file-input" id="about_us_image_1" <?= isset($about_us_data->about_us_image_1) && !empty($about_us_data->about_us_image_1) ? '' : 'required=""' ?>>
                                        <label class="custom-file-label" for="about_us_image_1">Choose file</label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <img src="<?= base_url() . (isset($about_us_data->about_us_image_1) && !empty($about_us_data->about_us_image_1) && file_exists($about_us_data->about_us_image_1) ? $about_us_data->about_us_image_1 : '') ?>" height="100px" width="120px" class="m-2">
                                    <input type="hidden" name="hidden_about_us_image_1" id="hidden_about_us_image_1" value="<?= isset($about_us_data->about_us_image_1) && !empty($about_us_data->about_us_image_1) ? $about_us_data->about_us_image_1 : '' ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label class="form-label">Image <i class="text-danger">(File in PNG) File Size 470x530 px</i></label>
                                    <div class="custom-file">
                                        <input type="file" name="about_us_image_2" class="custom-file-input" id="about_us_image_2" <?= isset($about_us_data->about_us_image_2) && !empty($about_us_data->about_us_image_2) ? '' : 'required=""' ?>>
                                        <label class="custom-file-label" for="about_us_image_2">Choose file</label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <img src="<?= base_url() . (isset($about_us_data->about_us_image_2) && !empty($about_us_data->about_us_image_2) && file_exists($about_us_data->about_us_image_2) ? $about_us_data->about_us_image_2 : '') ?>" height="100px" width="120px" class="m-2">
                                    <input type="hidden" name="hidden_about_us_image_2" id="hidden_about_us_image_2" value="<?= isset($about_us_data->about_us_image_2) && !empty($about_us_data->about_us_image_2) ? $about_us_data->about_us_image_2 : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditAboutUs').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>