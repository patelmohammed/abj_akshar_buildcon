<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user-friends'></i> Add Team Member
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Team/teamMember">Team Member</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Team/addEditTeamMemeber', $arrayName = array('id' => 'addEditClient', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_team_category_id">Team Category <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="ref_team_category_id" id="ref_team_category_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($team_category_data) && !empty($team_category_data)) {
                                        foreach ($team_category_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->team_category_id) && !empty($v1->team_category_id) ? $v1->team_category_id : '' ?>"><?= isset($v1->team_category_name) && !empty($v1->team_category_name) ? $v1->team_category_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Team Category Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="team_name">Team Member Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control textonly" name="team_name" id="team_category_name" placeholder="Team Member Name" required>
                                <div class="invalid-feedback">
                                    Team Member Name Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="tema_designation">Team Member Designation <span class="text-danger">*</span></label>
                                <input type="text" class="form-control textonly" name="tema_designation" id="tema_designation" placeholder="Team Member Designation" required>
                                <div class="invalid-feedback">
                                    Team Member Designation Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                    <label class="custom-control-label" for="is_active">Team Member Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Member Logo <i class="text-danger">(File in JPG,PNG) File Size 170x170 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="team_image" class="custom-file-input" id="team_image" required="">
                                    <label class="custom-file-label" for="team_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $("#ref_team_category_id").select2({
            placeholder: "Select team category",
            allowClear: true,
            width: '100%'
        });

        $('#addEditClient').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>