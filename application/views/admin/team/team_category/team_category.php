<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user-friends'></i> Team Category
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Team/addEditTeamCategory">Add Team Category</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Team Category Name</th>
                                    <th>Active / Deactive</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($team_categrooy_data) && !empty($team_categrooy_data)) {
                                    $sn = 0;
                                    foreach ($team_categrooy_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->team_category_name) && !empty($value->team_category_name) ? $value->team_category_name : '' ?></td> 
                                            <td style="text-align: center;">
                                                <?php if ($value->is_active == 1) { ?>
                                                    <button type="button" class="btn btn-xs btn-success waves-effect waves-themed deactivate_team_category" data-id="<?= $value->team_category_id ?>">Active</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-xs btn-danger waves-effect waves-themed activate_team_category" data-id="<?= $value->team_category_id ?>">Deactivated</button>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Team/addEditTeamCategory/<?= $value->team_category_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-id="<?= $value->team_category_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_team_category' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).on('click', '.delete_team_category', function () {
        var id = $(this).attr('data-id');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Team/deleteTeamCategory') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    ``
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });

    $(document).on('click', '.deactivate_team_category', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Team/deactiveTeamCategory') ?>',
            data: {team_category_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                } else {
                    swalWithBootstrapButtons.fire("Something Wrong", data.message, "error");
                    return false;
                }
            }
        });
    });

    $(document).on('click', '.activate_team_category', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Team/activeTeamCategory') ?>',
            data: {team_category_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });
</script>