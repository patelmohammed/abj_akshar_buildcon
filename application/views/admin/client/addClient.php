<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user-friends'></i> Add Client
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Client">Client</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Client/addEditClient', $arrayName = array('id' => 'addEditClient', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Client Logo <i class="text-danger">(File in JPG,PNG) File Size 117x50 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="client_logo" class="custom-file-input" id="client_logo" required="">
                                    <label class="custom-file-label" for="client_logo">Choose file</label>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                    <label class="custom-control-label" for="is_active">Client Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditClient').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                user_name: {
                    remote: {
                        url: "<?= base_url('/admin/Dashboard/checkUserName') ?>",
                        type: "get"
                    }
                },
                user_email: {
                    remote: {
                        url: "<?= base_url('/admin/Dashboard/checkUserEmail') ?>",
                        type: "get"
                    }
                }
            },
            messages: {
                user_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                user_email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });

        $(document).on('change', '.check_full_access', function () {
            var i = $(this).data('id');
            if ($(this).is(':checked')) {
                $("#view_" + i).prop("checked", true);
                $("#add_" + i).prop("checked", true);
                $("#edit_" + i).prop("checked", true);
                $("#delete_" + i).prop("checked", true);
            } else {
                $("#view_" + i).prop("checked", false);
                $("#add_" + i).prop("checked", false);
                $("#edit_" + i).prop("checked", false);
                $("#delete_" + i).prop("checked", false);
            }
        });

        $(document).on('change', '#checkall_side_menu', function () {
            if ($(this).is(':checked')) {
                $(".check_all").prop("checked", true);
                $(".check_child").prop("checked", true);
            } else {
                $(".check_all").prop("checked", false);
                $(".check_child").prop("checked", false);
            }
        });

        $(document).on('change', '.check_child', function () {
            var i = $(this).data('id');
            if ($("#view_" + i).is(':checked') && $("#add_" + i).is(':checked') && $("#edit_" + i).is(':checked') && $("#delete_" + i).is(':checked')) {
                $("#full_access_" + i).prop("checked", true);
            } else {
                $("#full_access_" + i).prop("checked", false);
            }
        });
    });
</script>