<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Testimonial
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Home/testimonial">Testimonial</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Home/addEditTestimonial/' . $encrypted_id, $arrayName = array('id' => 'addEditTestimonial', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="testimonial_name">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control textonly" name="testimonial_name" id="testimonial_name" placeholder="Name" required value="<?= isset($testimonial_data->testimonial_name) && !empty($testimonial_data->testimonial_name) ? $testimonial_data->testimonial_name : '' ?>">
                                <div class="invalid-feedback">
                                    Name Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="testimonial_description">Description <span class="text-danger">*</span></label>
                                <textarea class="form-control textonly" rows="5" name="testimonial_description" id="testimonial_description" placeholder="Description" required><?= isset($testimonial_data->testimonial_description) && !empty($testimonial_data->testimonial_description) ? $testimonial_data->testimonial_description : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in JPG,PNG) File Size 430x580px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="testimonial_image" class="custom-file-input" id="testimonial_image" <?= isset($testimonial_data->testimonial_image) && !empty($testimonial_data->testimonial_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="testimonial_image">Choose file</label>
                                </div>
                                <img src="<?= base_url() . $testimonial_data->testimonial_image ?>" height="100px" width="150px">
                                <input type="hidden" name="hidden_testimonial_image" id="hidden_testimonial_image" value="<?= $testimonial_data->testimonial_image ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="testimonial_rating">Rating <span class="text-danger">*</span></label>
                                <div class="frame-wrap">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="testimonial_rating_1" name="testimonial_rating" required="" value="1" <?= isset($testimonial_data->testimonial_rating) && !empty($testimonial_data->testimonial_rating) ? set_checked($testimonial_data->testimonial_rating, 1) : '' ?>>
                                        <label class="custom-control-label" for="testimonial_rating_1">1</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="testimonial_rating_2" name="testimonial_rating" value="2" <?= isset($testimonial_data->testimonial_rating) && !empty($testimonial_data->testimonial_rating) ? set_checked($testimonial_data->testimonial_rating, 2) : '' ?>>
                                        <label class="custom-control-label" for="testimonial_rating_2">2</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="testimonial_rating_3" name="testimonial_rating" value="3" <?= isset($testimonial_data->testimonial_rating) && !empty($testimonial_data->testimonial_rating) ? set_checked($testimonial_data->testimonial_rating, 3) : '' ?>>
                                        <label class="custom-control-label" for="testimonial_rating_3">3</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="testimonial_rating_4" name="testimonial_rating" value="4" <?= isset($testimonial_data->testimonial_rating) && !empty($testimonial_data->testimonial_rating) ? set_checked($testimonial_data->testimonial_rating, 4) : '' ?>>
                                        <label class="custom-control-label" for="testimonial_rating_4">4</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="testimonial_rating_5" name="testimonial_rating" value="5" <?= isset($testimonial_data->testimonial_rating) && !empty($testimonial_data->testimonial_rating) ? set_checked($testimonial_data->testimonial_rating, 5) : '' ?>>
                                        <label class="custom-control-label" for="testimonial_rating_5">5</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditTestimonial').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>