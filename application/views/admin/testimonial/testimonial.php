<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Testimonial
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Home/addEditTestimonial">Add Testimonial</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Designation</th>
                                    <th>Rating</th>
                                    <th>Image</th>
                                    <th>Active / Deactive</th>
                                    <th>Added By</th>
                                    <th class="notexport no-sort">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($testimonial_data) && !empty($testimonial_data)) {
                                    $sn = 0;
                                    foreach ($testimonial_data as $key => $value) {
                                        $sn++;
                                        ?>                       
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->testimonial_name) && !empty($value->testimonial_name) ? $value->testimonial_name : '' ?></td>
                                            <td><?= isset($value->testimonial_description) && !empty($value->testimonial_description) ? $value->testimonial_description : '' ?></td>
                                            <td><?= isset($value->testimonial_designation) && !empty($value->testimonial_designation) ? $value->testimonial_designation : '' ?></td>
                                            <td><?= isset($value->testimonial_rating) && !empty($value->testimonial_rating) ? $value->testimonial_rating : '' ?></td>
                                            <td><img src="<?= base_url() . $value->testimonial_image ?>" height="100px" width="200px"></td> 
                                            <td style="text-align: center;">
                                                <?php if ($value->is_active == 1) { ?>
                                                    <button type="button" class="btn btn-xs btn-success waves-effect waves-themed deactivate_country" data-id="<?= $value->testimonial_id ?>">Active</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-xs btn-danger waves-effect waves-themed activate_country" data-id="<?= $value->testimonial_id ?>">Deactivated</button>
                                                <?php } ?>
                                            </td>
                                            <td><?= isset($value->InsUser) && !empty($value->InsUser) ? getUserNameById($value->InsUser) : '' ?></td>   
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Home/addEditTestimonial/<?= $value->testimonial_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-url="<?= base_url('admin/Home/deleteTestimonial') ?>" data-id="<?= $value->testimonial_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_record' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).on('click', '.deactivate_country', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Home/deactiveTestimonial') ?>',
            data: {testimonial_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });

    $(document).on('click', '.activate_country', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Home/activeTestimonial') ?>',
            data: {testimonial_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });
</script>