<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-envelope'></i> Mail Template
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>type</th>
                                    <th>fromname</th>
                                    <th>fromemail</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($template_data) && !empty($template_data)) {
                                    $sn = 0;
                                    foreach ($template_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><a href="javascript:void(0);" class="view_mail_template" data-url="<?= base_url() ?>admin/Mail/mailTemplateById" data-id="<?= $value->mail_template_id ?>"><?= $value->type ?></a></td>
                                            <td><?= $value->fromname ?></td>
                                            <td><?= $value->fromemail ?></td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Mail/addEditMailTemplate/<?= $value->mail_template_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="mailTemplate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body" id="mailTemplateDetail">
            </div>
        </div>
    </div>
</div>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    $(document).on('click', '.view_mail_template', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        $.ajax({
            type: "POST",
            url: url,
            data: {id: id},
            cache: false,
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true && data.template.message != '') {
                    $('#mailTemplateDetail').html(data.template.message);
                    $('#mailTemplate').modal('show');
                }
            }
        });
    });
</script>