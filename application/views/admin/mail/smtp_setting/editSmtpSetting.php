<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit SMTP Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Mail', $arrayName = array('id' => 'addEditSmtpSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="smtp_host">SMTP Host <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="smtp_host" id="smtp_host" placeholder="SMTP Host" required value="<?= isset($smtp_data->smtp_host) && !empty($smtp_data->smtp_host) ? $smtp_data->smtp_host : '' ?>">
                                <div class="invalid-feedback">
                                    Product Name Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="smtp_user">SMTP User <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="smtp_user" id="smtp_user" placeholder="SMTP User" required value="<?= isset($smtp_data->smtp_user) && !empty($smtp_data->smtp_user) ? $smtp_data->smtp_user : '' ?>">
                                <div class="invalid-feedback">
                                    Product Name Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="smtp_pass">SMTP Password <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="smtp_pass" id="smtp_pass" placeholder="SMTP Password" required value="<?= isset($smtp_data->smtp_pass) && !empty($smtp_data->smtp_pass) ? $smtp_data->smtp_pass : '' ?>">
                                <div class="invalid-feedback">
                                    Product Name Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="smtp_port">SMTP Port <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="smtp_port" id="smtp_port" placeholder="SMTP Port" required value="<?= isset($smtp_data->smtp_port) && !empty($smtp_data->smtp_port) ? $smtp_data->smtp_port : '' ?>">
                                <div class="invalid-feedback">
                                    Product Name Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="smtp_crypto">SMTP Encryption <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="smtp_crypto" id="smtp_crypto" placeholder="SMTP Crypto" required value="<?= isset($smtp_data->smtp_crypto) && !empty($smtp_data->smtp_crypto) ? $smtp_data->smtp_crypto : '' ?>">
                                <div class="invalid-feedback">
                                    Product Name Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditSmtpSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>