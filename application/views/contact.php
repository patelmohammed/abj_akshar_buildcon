<section>
    <div class="w-100 pt-170 pb-150 dark-layer3 opc7 position-relative">
        <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($contact_setting_data->background_image) && !empty($contact_setting_data->background_image) && file_exists($contact_setting_data->background_image) ? $contact_setting_data->background_image : '') ?>);"></div>
        <div class="container">
            <div class="page-top-wrap w-100">
                <h1 class="mb-0">Contact Us</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>" title="">Home</a></li>
                    <li class="breadcrumb-item active">Contact</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="w-100 pt-100 pb-100 position-relative">
        <div class="container">
            <div class="contact-wrap position-relative w-100">
                <div class="contact-map w-100" id="contact-map">
                    <iframe src="<?= isset($contact_setting_data->map) && !empty($contact_setting_data->map) ? $contact_setting_data->map : '' ?>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <div class="contact-info-wrap text-center position-absolute">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-lg-4">
                            <div class="contact-info-box w-100">
                                <i class="thm-clr flaticon-headset"></i>
                                <strong>Our Phone</strong>
                                <span class="d-block"><?= isset($contact_setting_data->contact_number_1) && !empty($contact_setting_data->contact_number_1) ? $contact_setting_data->contact_number_1 : '' ?></span>
                                <span class="d-block"><?= isset($contact_setting_data->contact_number_2) && !empty($contact_setting_data->contact_number_2) ? $contact_setting_data->contact_number_2 : '' ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-lg-4">
                            <div class="contact-info-box w-100">
                                <i class="thm-clr flaticon-mail"></i>
                                <strong>Our Mail Box</strong>
                                <a class="d-block" href="mailto:<?= isset($contact_setting_data->email) && !empty($contact_setting_data->email) ? $contact_setting_data->email : '' ?>" title=""><?= isset($contact_setting_data->email) && !empty($contact_setting_data->email) ? $contact_setting_data->email : '' ?></a>
                                <a class="d-block" href="mailto:<?= isset($contact_setting_data->email_alt) && !empty($contact_setting_data->email_alt) ? $contact_setting_data->email_alt : '' ?>" title=""><?= isset($contact_setting_data->email_alt) && !empty($contact_setting_data->email_alt) ? $contact_setting_data->email_alt : '' ?></a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-lg-4">
                            <div class="contact-info-box w-100">
                                <i class="thm-clr flaticon-placeholder"></i>
                                <strong>Our Location</strong>
                                <p class="mb-0"><?= isset($contact_setting_data->address) && !empty($contact_setting_data->address) ? $contact_setting_data->address : '' ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="w-100 pb-100 position-relative">
        <div class="container">
            <div class="sec-title v2 text-center w-100">
                <div class="sec-title-inner d-inline-block">
                    <span class="thm-clr d-block"><?= isset($contact_setting_data->contact_us_title) && !empty($contact_setting_data->contact_us_title) ? $contact_setting_data->contact_us_title : '' ?></span>
                    <h2 class="mb-0"><?= isset($contact_setting_data->contact_us_desc) && !empty($contact_setting_data->contact_us_desc) ? $contact_setting_data->contact_us_desc : '' ?></h2>
                    <p class="mb-0"><?= isset($contact_setting_data->contact_us_sub_desc) && !empty($contact_setting_data->contact_us_sub_desc) ? $contact_setting_data->contact_us_sub_desc : '' ?></p>
                </div>
            </div>
            <form class="contact-form text-center w-100" action="#" method="post" id="email-form">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="form-group w-100">
                            <div class="response w-100"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <label class="d-block">Name :</label>
                        <input class="fname form-control" type="text" name="fname">
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <label class="d-block">Email Address :</label>
                        <input class="email form-control" type="email" name="email">
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <label class="d-block">Contact Number :</label>
                        <input class="phone form-control" type="phone" name="phone">
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <label class="d-block">Write a message :</label>
                        <textarea class="contact_message form-control" name="contact_message"></textarea>
                        <button class="thm-btn thm-bg" id="submit" type="button">Submit Now Next<i class="flaticon-arrow-pointing-to-right"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<?php
if (isset($other_service_data) && !empty($other_service_data)) {
    ?>
    <section>
        <div class="w-100 pt-155 pb-155 blue-layer opc85 position-relative">
            <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($other_service_data->other_sevice_image) && !empty($other_service_data->other_sevice_image) && file_exists($other_service_data->other_sevice_image) ? $other_service_data->other_sevice_image : '' ) ?>);"></div>
            <div class="particles-js" id="prtcl2"></div>
            <div class="container">
                <div class="banner-wrap position-relative text-center w-100">
                    <div class="banner-inner d-inline-block">
                        <h2 class="mb-0"><?= isset($other_service_data->other_sevice_title) && !empty($other_service_data->other_sevice_title) ? $other_service_data->other_sevice_title : '' ?></h2>
                        <p class="mb-0"><?= isset($other_service_data->other_sevice_desc) && !empty($other_service_data->other_sevice_desc) ? $other_service_data->other_sevice_desc : '' ?></p>
                        <a class="thm-btn thm-bg" href="<?= base_url() ?>About" title="">Learn More<i class="flaticon-arrow-pointing-to-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($client_data) && !empty($client_data)) {
    ?>
    <section>
        <div class="w-100 pb-50 pt-150 position-relative">
            <div class="container">
                <div class="clients-wrap w-100">
                    <div class="team-inner team-caro2">
                        <?php
                        if (isset($client_data) && !empty($client_data)) {
                            foreach ($client_data as $k4 => $v4) {
                                if (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo)) {
                                    ?>
                                    <div class="col-md-3 col-sm-4 col-lg-2">
                                        <div class="client-box w-100">
                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?= base_url() . (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo) ? $v4->client_logo : '') ?>" alt="Client Image <?= $k4 ?>"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
}
if (isset($get_in_touch_data) && !empty($get_in_touch_data)) {
    ?>
    <section>
        <div class="w-100 position-relative">
            <div class="container">
                <div class="getin-touch-wrap overlap-99 brd-rd5 style2 thm-layer opc8 w-100 overflow-hidden position-relative">
                    <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($get_in_touch_data->get_in_touch_image) && !empty($get_in_touch_data->get_in_touch_image) && file_exists($get_in_touch_data->get_in_touch_image) ? $get_in_touch_data->get_in_touch_image : '') ?>);"></div>
                    <div class="row align-items-center justify-content-between">
                        <div class="col-md-7 col-sm-12 col-lg-5">
                            <div class="getin-touch-title w-100">
                                <span class="d-block"><?= isset($get_in_touch_data->get_in_touch_title) && !empty($get_in_touch_data->get_in_touch_title) ? $get_in_touch_data->get_in_touch_title : '' ?></span>
                                <h2 class="mb-0"><?= isset($get_in_touch_data->get_in_touch_desc) && !empty($get_in_touch_data->get_in_touch_desc) ? $get_in_touch_data->get_in_touch_desc : '' ?></h2>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-lg-4">
                            <div class="getin-touch-btn text-right">
                                <a class="thm-btn bg-color1" href="<?= base_url() ?>Contact" title="">Book An Appointment<i class="flaticon-arrow-pointing-to-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>