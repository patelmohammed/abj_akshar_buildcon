<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="icon" href="<?= base_url() ?>assets/images/favicon.png" sizes="35x35" type="image/png">
        <title>ABJ - AKSHAR BUILDCON</title>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/all.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/flaticon.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/slick.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/color.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/gallery/jquery.fancybox.min">

        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript">var base = "<?= base_url() ?>";</script>
    </head>
    <body>
        <div id="preloader">
            <div id="preloader-status"></div>
        </div>
        <main>
            <header class="stick style1 w-100">
                <div class="topbar bg-color1 w-100">
                    <div class="container">
                        <div class="topbar-inner d-flex flex-wrap justify-content-between align-items-center w-100">
                            <ul class="topbar-info-list mb-0 list-unstyled d-inline-flex">
                                <li><i class="thm-clr fas fa-map-marker-alt"></i><?= isset($footer_data->address) && !empty($footer_data->address) ? $footer_data->address : '' ?></li>
                                <li><i class="thm-clr far fa-envelope-open"></i>Email: <a href="mailto:<?= isset($footer_data->email) && !empty($footer_data->email) ? $footer_data->email : '' ?>" title=""><?= isset($footer_data->email) && !empty($footer_data->email) ? $footer_data->email : '' ?></a></li>
                            </ul>
                            <ul class="topbar-links mb-0 list-unstyled d-inline-flex">
                                <!--                                <li><a href="javascript:void(0);" title="">Careers</a></li>
                                                                <li><a href="javascript:void(0);" title="">Help Desk</a></li>-->
                                <li><a href="<?=base_url();?>/admin" title="">Login</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="logo-info-bar-wrap w-100">
                    <div class="container">
                        <div class="logo-info-bar-inner w-100 d-flex flex-wrap justify-content-between align-items-center">
                            <div class="logo-social d-inline-flex flex-wrap justify-content-between align-items-center">
                                <div class="logo"><h1 class="mb-0"><a href="<?= base_url() ?>" title="Home"><img class="img-fluid" src="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) && file_exists($footer_data->logo_image) ? $footer_data->logo_image : '') ?>" alt="Logo" srcset="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) && file_exists($footer_data->logo_image) ? $footer_data->logo_image : '') ?>"></a></h1></div>
                            </div>
                            <div class="top-info-wrap d-inline-flex flex-wrap justify-content-between align-items-center">
                                <div class="call-us">
                                    <i class="thm-clr flaticon-phone-call"></i>
                                    <span><?= isset($footer_data->contact_title) && !empty($footer_data->contact_title) ? $footer_data->contact_title : '' ?></span>
                                    <strong><?= isset($footer_data->contact_number) && !empty($footer_data->contact_number) ? $footer_data->contact_number : '' ?></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menu-wrap">
                    <div class="container">
                        <nav class="d-inline-flex justify-content-between align-items-center w-100 bg-color1">
                            <div class="header-left">
                                <ul class="mb-0 list-unstyled d-inline-flex">
                                    <li class="<?= $this->page_id == 'HOME' ? 'active' : '' ?>"><a href="<?= base_url() ?>" title="" >Home</a></li>
                                    <li class="<?= $this->page_id == 'ABOUT' ? 'active' : '' ?>"><a href="<?= base_url() ?>About" title="">About Us</a></li>
                                    <li class="<?= $this->page_id == 'PROJECTS' ? 'active' : '' ?>"><a href="<?= base_url() ?>Projects" title="">Projects</a></li>
                                    <li class="<?= $this->page_id == 'TEAM' ? 'active' : '' ?>"><a href="<?= base_url() ?>Team" title="">Team</a></li>
                                    <li class="<?= $this->page_id == 'SERVICES' ? 'active' : '' ?>"><a href="<?= base_url() ?>Services" title="">Services</a></li>
                                    <li class="<?= $this->page_id == 'CONTACT' ? 'active' : '' ?>"><a href="<?= base_url() ?>Contact" title="">Contact Us</a></li>
                                </ul>
                            </div>
                            <div class="header-right-btns">
                                <a class="get-quote" href="javascript:void(0);" title="" data-toggle="modal" data-target="#exampleModal"><i class="far fa-comments"></i>Get A Quote<i class="flaticon-arrow-pointing-to-right"></i></a>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>
            <div class="sticky-menu">
                <div class="container">
                    <div class="sticky-menu-inner d-flex flex-wrap align-items-center justify-content-between w-100">
                        <div class="logo"><h1 class="mb-0"><a href="<?= base_url() ?>" title="Home"><img class="img-fluid" src="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) && file_exists($footer_data->logo_image) ? $footer_data->logo_image : '') ?>" alt="Logo" srcset="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) && file_exists($footer_data->logo_image) ? $footer_data->logo_image : '') ?>"></a></h1></div>
                        <nav class="d-inline-flex justify-content-between align-items-center">
                            <div class="header-left">
                                <ul class="mb-0 list-unstyled d-inline-flex">
                                    <li class="<?= $this->page_id == 'HOME' ? 'active' : '' ?>"><a href="<?= base_url() ?>" title="" >Home</a></li>
                                    <li class="<?= $this->page_id == 'ABOUT' ? 'active' : '' ?>"><a href="<?= base_url() ?>About" title="">About Us</a></li>
                                    <li class="<?= $this->page_id == 'PROJECTS' ? 'active' : '' ?>"><a href="<?= base_url() ?>Projects" title="">Projects</a></li>
                                    <li class="<?= $this->page_id == 'TEAM' ? 'active' : '' ?>"><a href="<?= base_url() ?>Team" title="">Team</a></li>
                                    <li class="<?= $this->page_id == 'SERVICES' ? 'active' : '' ?>"><a href="<?= base_url() ?>Services" title="">Services</a></li>
                                    <li class="<?= $this->page_id == 'CONTACT' ? 'active' : '' ?>"><a href="<?= base_url() ?>Contact" title="">Contact Us</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="rspn-hdr">
                <div class="lg-mn">
                    <div class="logo"><a href="<?= base_url() ?>" title="Home"><img src="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) && file_exists($footer_data->logo_image) ? $footer_data->logo_image : '') ?>" alt="Logo"></a></div>
                    <div class="rspn-cnt">
                        <span><i class="thm-clr far fa-envelope-open"></i><a href="mailto:<?= isset($footer_data->email) && !empty($footer_data->email) ? $footer_data->email : '' ?>" title=""><?= isset($footer_data->email) && !empty($footer_data->email) ? $footer_data->email : '' ?></a></span>
                        <span><i class="thm-clr fas fa-map-marker-alt"></i><?= isset($footer_data->address) && !empty($footer_data->address) ? $footer_data->address : '' ?></span>
                    </div>
                    <span class="rspn-mnu-btn"><i class="fa fa-list-ul"></i></span>
                </div>
                <div class="rsnp-mnu">
                    <span class="rspn-mnu-cls"><i class="fa fa-times"></i></span>
                    <ul class="mb-0 list-unstyled w-100">
                        <li class="<?= $this->page_id == 'HOME' ? 'active' : '' ?>"><a href="<?= base_url() ?>" title="" >Home</a></li>
                        <li class="<?= $this->page_id == 'ABOUT' ? 'active' : '' ?>"><a href="<?= base_url() ?>about" title="">About Us</a></li>
                        <li class="<?= $this->page_id == 'PROJECTS' ? 'active' : '' ?>"><a href="<?= base_url() ?>Projects" title="">Projects</a></li>
                        <li class="<?= $this->page_id == 'TEAM' ? 'active' : '' ?>"><a href="<?= base_url() ?>Team" title="">Team</a></li>
                        <li class="<?= $this->page_id == 'SERVICES' ? 'active' : '' ?>"><a href="<?= base_url() ?>Services" title="">Services</a></li>
                        <li class="<?= $this->page_id == 'CONTACT' ? 'active' : '' ?>"><a href="<?= base_url() ?>Contact" title="">Contact Us</a></li>
                    </ul>
                </div>
            </div>