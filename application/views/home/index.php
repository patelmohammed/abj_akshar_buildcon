<?php if (isset($slider_data) && !empty($slider_data)) { ?>
    <section>
        <div class="w-100 position-relative">
            <div class="feat-wrap position-relative w-100">
                <div class="feat-caro">
                    <?php foreach ($slider_data as $k1 => $v1) {
                        ?>
                        <div class="feat-item">
                            <div class="feat-img position-absolute" style="background-image: url(<?= base_url() . (isset($v1->slider_image) && !empty($v1->slider_image) && file_exists($v1->slider_image) ? $v1->slider_image : '') ?>);"></div>
                            <div class="container">
                                <div class="feat-cap">
                                    <h2 class="mb-0"><?= isset($v1->slider_title) && !empty($v1->slider_title) ? $v1->slider_title : '' ?></h2>
                                    <p class="mb-0"><?= isset($v1->slider_description) && !empty($v1->slider_description) ? $v1->slider_description : '' ?></p>
                                    <div class="feat-cap-innr">
                                        <a class="thm-btn thm-bg" href="<?= base_url() ?>About" title="">Learn More<i class="flaticon-arrow-pointing-to-right"></i></a>
                                        <!--<a class="video-btn" href="https://www.youtube.com/embed/6gUOzbhtVd4" data-fancybox title=""><span class="spinner"><i class="flaticon-play"></i></span>Intro Video <br> Watch</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($about_us_data) && !empty($about_us_data)) {
    ?>
    <section>
        <div class="w-100 pt-100 pb-100 position-relative">
            <div class="container">
                <div class="about-wrap w-100">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-sm-12 col-lg-10">
                            <div class="about-content-wrap w-100">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-lg-4">
                                        <div class="sec-title title-with-shape w-100">
                                            <div class="sec-title-inner d-inline-block">
                                                <span class="thm-clr d-block"><?= isset($about_us_data->about_us_title_1) && !empty($about_us_data->about_us_title_1) ? $about_us_data->about_us_title_1 : '' ?></span>
                                                <h3 class="mb-0"><?= isset($about_us_data->about_us_title_2) && !empty($about_us_data->about_us_title_2) ? $about_us_data->about_us_title_2 : '' ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-lg-8">
                                        <div class="about-desc w-100">
                                            <h2 class="mb-0"><?= isset($about_us_data->about_us_desc_1) && !empty($about_us_data->about_us_desc_1) ? $about_us_data->about_us_desc_1 : '' ?></strong></h2>
                                            <p class="mb-0"><?= isset($about_us_data->about_us_desc_2) && !empty($about_us_data->about_us_desc_2) ? $about_us_data->about_us_desc_2 : '' ?></p>
                                        </div>
                                        <div class="serv-wrap w-100">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-lg-6">
                                                    <div class="serv-box w-100">
                                                        <i class="thm-clr <?= isset($about_us_data->about_us_alt_sub_icon_1) && !empty($about_us_data->about_us_alt_sub_icon_1) ? $about_us_data->about_us_alt_sub_icon_1 : '' ?>"></i>
                                                        <div class="serv-box-inner">
                                                            <h3 class="mb-0"><a href="<?= base_url('Services') ?>" title=""><?= isset($about_us_data->about_us_alt_sub_title_1) && !empty($about_us_data->about_us_alt_sub_title_1) ? $about_us_data->about_us_alt_sub_title_1 : '' ?></a></h3>
                                                            <p class="mb-0"><?= isset($about_us_data->about_us_alt_sub_desc_1) && !empty($about_us_data->about_us_alt_sub_desc_1) ? $about_us_data->about_us_alt_sub_desc_1 : '' ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-lg-6">
                                                    <div class="serv-box w-100">
                                                        <i class="thm-clr <?= isset($about_us_data->about_us_alt_sub_icon_2) && !empty($about_us_data->about_us_alt_sub_icon_2) ? $about_us_data->about_us_alt_sub_icon_2 : '' ?>"></i>
                                                        <div class="serv-box-inner">
                                                            <h3 class="mb-0"><a href="<?= base_url('Services') ?>" title=""><?= isset($about_us_data->about_us_alt_sub_title_2) && !empty($about_us_data->about_us_alt_sub_title_2) ? $about_us_data->about_us_alt_sub_title_2 : '' ?></a></h3>
                                                            <p class="mb-0"><?= isset($about_us_data->about_us_alt_sub_desc_2) && !empty($about_us_data->about_us_alt_sub_desc_2) ? $about_us_data->about_us_alt_sub_desc_2 : '' ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="about-image position-relative w-100">
                                <span><?= isset($about_us_data->image_title) && !empty($about_us_data->image_title) ? $about_us_data->image_title : '' ?></span>
                                <img class="img-fluid w-100" src="<?= base_url() . (isset($about_us_data->about_us_image_1) && !empty($about_us_data->about_us_image_1) && file_exists($about_us_data->about_us_image_1) ? $about_us_data->about_us_image_1 : '') ?>" alt="About Image 1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if ((isset($service_setting_data) && !empty($service_setting_data)) || (isset($statistic_data) && !empty($statistic_data))) {
    ?>
    <section>
        <div class="w-100 pt-100 blue-layer opc7 position-relative">
            <div class="sec-title v2 text-center w-100">
                <div class="sec-title-inner d-inline-block">
                    <span class="thm-clr d-block"><?= isset($service_setting_data->service_title_1) && !empty($service_setting_data->service_title_1) ? $service_setting_data->service_title_1 : '' ?></span>
                    <h2 class="mb-0"><?= isset($service_setting_data->service_title_2) && !empty($service_setting_data->service_title_2) ? $service_setting_data->service_title_2 : '' ?></h2>
                    <p class="mb-0" style="color: #FFFFFF;"><?php // echo isset($service_setting_data->service_desc) && !empty($service_setting_data->service_desc) ? $service_setting_data->service_desc : ''               ?></p>
                </div>
            </div>
            <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($service_setting_data->background_image) && !empty($service_setting_data->background_image) && file_exists($service_setting_data->background_image) ? $service_setting_data->background_image : '') ?>);">

            </div>
            <div class="particles-js" id="prtcl"></div>
            <div class="container">
                <div class="serv-wrap text-center w-100">                    
                    <div class="serv-caro">
                            <?php
                            if (isset($service_setting_data->service_item) && !empty($service_setting_data->service_item)) {
                                foreach ($service_setting_data->service_item as $k8 => $v8) {
                                    ?>
                                    <div class="col-md-4 col-sm-6 col-lg-3">
                                        <div class="serv-box2 position-relative overflow-hidden w-100" style="background-image: url(<?= base_url() ?>assets/images/resources/serv-bg1.jpg);">
                                            <i class="thm-clr <?= isset($v8->service_icon) && !empty($v8->service_icon) ? $v8->service_icon : '' ?>"></i>
                                            <div class="serv-box-inner">
                                                <h3 class="mb-0"><a href="<?= base_url('Services') ?>" title=""><?= isset($v8->service_title) && !empty($v8->service_title) ? $v8->service_title : '' ?></a></h3>
                                                <p class="mb-0"><?= isset($v8->service_desc) && !empty($v8->service_desc) ? $v8->service_desc : '' ?></p>
                                                <!--<span class="d-block"><?php // echo isset($v8->service_sub_desc) && !empty($v8->service_sub_desc) ? $v8->service_sub_desc : ''                                ?></span>-->
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                    </div>
                </div>
                <?php
                if (isset($statistic_data) && !empty($statistic_data)) {
                    ?>
                    <div class="facts-wrap thm-layer opc1 text-center position-relative overflow-hidden brd-rd5 w-100">
                        <div class="parallax-bg zoom-anim back-blend-multiply patern-bg thm-bg" style="background-image: url(<?= base_url() ?>assets/images/pattern-bg1.png);"></div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="fact-box w-100">
                                    <h2 class="mb-0"><span class="counter"><?= isset($statistic_data->title_1) && !empty($statistic_data->title_1) ? $statistic_data->title_1 : '' ?></span><sup>+</sup></h2>
                                    <h4 class="mb-0"><?= isset($statistic_data->desc_1) && !empty($statistic_data->desc_1) ? $statistic_data->desc_1 : '' ?></h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="fact-box w-100">
                                    <h2 class="mb-0"><span class="counter"><?= isset($statistic_data->title_2) && !empty($statistic_data->title_2) ? $statistic_data->title_2 : '' ?></span><sup>+</sup></h2>
                                    <h4 class="mb-0"><?= isset($statistic_data->desc_2) && !empty($statistic_data->desc_2) ? $statistic_data->desc_2 : '' ?></h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="fact-box w-100">
                                    <h2 class="mb-0"><span class="counter"><?= isset($statistic_data->title_3) && !empty($statistic_data->title_3) ? $statistic_data->title_3 : '' ?></span><sup>+</sup></h2>
                                    <h4 class="mb-0"><?= isset($statistic_data->desc_3) && !empty($statistic_data->desc_3) ? $statistic_data->desc_3 : '' ?></h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="fact-box w-100">
                                    <h2 class="mb-0"><span class="counter"><?= isset($statistic_data->title_4) && !empty($statistic_data->title_4) ? $statistic_data->title_4 : '' ?></span><sup>+</sup></h2>
                                    <h4 class="mb-0"><?= isset($statistic_data->desc_4) && !empty($statistic_data->desc_4) ? $statistic_data->desc_4 : '' ?></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <?php
}
if (isset($project_data) && !empty($project_data)) {
    ?>
    <section>
        <div class="w-100 pt-210 pb-100 position-relative">
            <div class="container">
                <div class="sec-title w-100">
                    <div class="sec-title-inner d-inline-block">
                        <span class="d-block thm-clr">OUR WORKS</span>
                        <h3 class="mb-0">Our Latest Projects</h3>
                    </div>
                </div>
                <div class="proj-wrap w-100">
                    <div class="row justify-content-center">
                        <?php
                        foreach ($project_data as $k5 => $v5) {
                            $k5++;
                            if ($k5 < 9) {
                                ?>
                                <div class="col-md-4 col-sm-6 col-lg-3">
                                    <div class="proj-box position-relative w-100">
                                        <div class="proj-thumb overflow-hidden w-100"><a href="<?= isset($v5->project_id) && !empty($v5->project_id) ? base_url('Projects/projectDetail/') . $v5->project_id : '' ?>" title=""><img class="img-fluid w-100" src="<?= isset($v5->project_poster_image) && !empty($v5->project_poster_image) && file_exists($v5->project_poster_image) ? $v5->project_poster_image : '' ?>" alt="Project Image 1"></a></div>
                                        <div class="proj-info position-absolute">
                                            <i class="<?= isset($v5->project_icon) && !empty($v5->project_icon) ? $v5->project_icon : 'flaticon-builder' ?>"></i>
                                            <h3 class="mb-0"><a href="<?= isset($v5->project_id) && !empty($v5->project_id) ? base_url('Projects/projectDetail/') . $v5->project_id : '' ?>" title=""><?= isset($v5->project_name) && !empty($v5->project_name) ? $v5->project_name : '' ?></a></h3>
                                        </div>
                                    </div>
                                </div>   
                                <?php
                            }
                        }
                        ?>                    
                    </div>
                </div>
                <?php if (count($project_data) > 8) { ?>
                    <div class="view-all mt-20 w-100 text-center">
                        <a class="thm-btn thm-bg" href="<?= base_url() ?>Projects" title="">View All Project<i class="flaticon-arrow-pointing-to-right"></i></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <?php
}
if (isset($get_in_touch_data) && !empty($get_in_touch_data)) {
    ?>
    <section>
        <div class="w-100 pt-100 pb-100 blue-layer opc8 position-relative">
            <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($get_in_touch_data->get_in_touch_image) && !empty($get_in_touch_data->get_in_touch_image) && file_exists($get_in_touch_data->get_in_touch_image) ? $get_in_touch_data->get_in_touch_image : '') ?>);"></div>
            <div class="particles-js" id="prtcl2"></div>
            <div class="container">
                <div class="getin-touch-wrap w-100">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-sm-12 col-lg-7">
                            <div class="getin-touch-title with-shap w-100">
                                <div class="getin-touch-blob thm-bg overflow-hidden position-absolute"></div>
                                <span class="d-block"><?= isset($get_in_touch_data->get_in_touch_title) && !empty($get_in_touch_data->get_in_touch_title) ? $get_in_touch_data->get_in_touch_title : '' ?></span>
                                <h2 class="mb-0"><?= isset($get_in_touch_data->get_in_touch_desc) && !empty($get_in_touch_data->get_in_touch_desc) ? $get_in_touch_data->get_in_touch_desc : '' ?></h2>
                                <p class="mb-0"><?= isset($get_in_touch_data->get_in_touch_sub_desc) && !empty($get_in_touch_data->get_in_touch_sub_desc) ? $get_in_touch_data->get_in_touch_sub_desc : '' ?></p>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-lg-5">
                            <div class="getin-touch-btn position-relative text-right">
                                <a class="thm-btn thm-bg" href="<?= base_url() ?>Contact" title="">Book An Appointment<i class="flaticon-arrow-pointing-to-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($hot_deal_data) && !empty($hot_deal_data)) {
    ?>
    <section>
        <div class="w-100 pt-130 pb-100 position-relative">
            <div class="container">
                <div class="sec-title2 w-100">
                    <div class="sec-title-inner2 d-inline-block">
                        <h2 class="mb-0"><?= isset($hot_deal_data->hot_deal_title) && !empty($hot_deal_data->hot_deal_title) ? $hot_deal_data->hot_deal_title : '' ?></h2>
                        <p class="mb-0"><?php echo isset($hot_deal_data->hot_deal_desc) && !empty($hot_deal_data->hot_deal_desc) ? $hot_deal_data->hot_deal_desc : '' ?></p>
                    </div>
                </div>
                <div class="solutions-wrap w-100">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-sm-12 col-lg-6">
                            <img class="img-fluid w-100" src="<?= base_url() . ( isset($hot_deal_data->hot_deal_image) && !empty($hot_deal_data->hot_deal_image) && file_exists($hot_deal_data->hot_deal_image) ? $hot_deal_data->hot_deal_image : '' ) ?>" alt="Solutions Image">
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-6">
                            <div class="toggle w-100" id="toggle">
                                <h2 class="mb-0"><?= isset($hot_deal_data->hot_deal_main_title) && !empty($hot_deal_data->hot_deal_main_title) ? $hot_deal_data->hot_deal_main_title : '' ?></h2>
                                <ul class="solutions-list mb-0 list-unstyled w-100">
                                    <li>
                                        <h4 class="mb-0 toggle-item"><i>+</i> <?= isset($hot_deal_data->hot_deal_sub_title_1) && !empty($hot_deal_data->hot_deal_sub_title_1) ? $hot_deal_data->hot_deal_sub_title_1 : '' ?></h4>
                                        <p class="mb-0 toggle-content"><?= isset($hot_deal_data->hot_deal_sub_desc_1) && !empty($hot_deal_data->hot_deal_sub_desc_1) ? $hot_deal_data->hot_deal_sub_desc_1 : '' ?></p>
                                    </li>
                                    <li>
                                        <h4 class="mb-0 toggle-item"><i>+</i> <?= isset($hot_deal_data->hot_deal_sub_title_2) && !empty($hot_deal_data->hot_deal_sub_title_2) ? $hot_deal_data->hot_deal_sub_title_2 : '' ?></h4>
                                        <p class="mb-0 toggle-content"><?= isset($hot_deal_data->hot_deal_sub_desc_2) && !empty($hot_deal_data->hot_deal_sub_desc_2) ? $hot_deal_data->hot_deal_sub_desc_2 : '' ?></p>
                                    </li>
                                    <li>
                                        <h4 class="mb-0 toggle-item"><i>+</i> <?= isset($hot_deal_data->hot_deal_sub_title_3) && !empty($hot_deal_data->hot_deal_sub_title_3) ? $hot_deal_data->hot_deal_sub_title_3 : '' ?></h4>
                                        <p class="mb-0 toggle-content"><?= isset($hot_deal_data->hot_deal_sub_desc_3) && !empty($hot_deal_data->hot_deal_sub_desc_3) ? $hot_deal_data->hot_deal_sub_desc_3 : '' ?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($testimonial_data) && !empty($testimonial_data) && isset($testimonial_data->testimonial_item_data) && !empty($testimonial_data->testimonial_item_data)) {
    ?>
    <section>
        <div class="w-100 dark-layer2 pt-100 pb-100 opc1 overflow-hidden position-relative">
            <div class="fixed-bg zoom-anim back-blend-screen h-tst patern-bg dark-bg" style="background-image: url(<?= base_url() ?>assets/images/pattern-bg2.jpg);"></div>
            <div class="particles-js" id="prtcl3"></div>
            <div class="container">
                <div class="testi-wrap position-relative w-100">
                    <h2 class="mb-0">WHAT CLIENTS SAYS?</h2>
                    <div class="testi-caro">
                        <?php foreach ($testimonial_data->testimonial_item_data as $k2 => $v2) { ?>
                            <div class="testi-box-wrap">
                                <div class="testi-box">
                                    <div class="testi-img">
                                        <img class="img-fluid" src="<?= base_url() . (isset($v2->testimonial_image) && !empty($v2->testimonial_image) && file_exists($v2->testimonial_image) ? $v2->testimonial_image : '') ?>" alt="Testimonial Image 1">
                                    </div>
                                    <div class="testi-info">
                                        <h3 class="mb-0"><?= isset($v2->testimonial_name) && !empty($v2->testimonial_name) ? $v2->testimonial_name : '' ?></h3>
                                        <p class="mb-0"><?= isset($v2->testimonial_description) && !empty($v2->testimonial_description) ? $v2->testimonial_description : '' ?></p>
                                        <span class="d-inline-block text-color3">
                                            <?php
                                            if (isset($v2->testimonial_rating) && !empty($v2->testimonial_rating)) {
                                                for ($i = 1; $i <= $v2->testimonial_rating; $i++) {
                                                    ?>
                                                    <i class="fas fa-star"></i>
                                                    <?php
                                                }
                                                for ($j = 1; $j <= (5 - $v2->testimonial_rating); $j++) {
                                                    ?>
                                                    <i class="far fa-star"></i>
                                                    <?php
                                                }
                                            }
                                            ?>
        <!--<span class="thm-clr">(07 Review)</span>-->
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($faq_data) && !empty($faq_data)) {
    ?>
    <section>
        <div class="w-100 pt-100 pb-100 position-relative">
            <div class="container">
                <div class="sec-title w-100">
                    <div class="sec-title-inner d-inline-block">
                        <span class="d-block thm-clr"><?= isset($faq_data->faq_title) && !empty($faq_data->faq_title) ? $faq_data->faq_title : '' ?></span>
                        <h3 class="mb-0"><?= isset($faq_data->faq_desc) && !empty($faq_data->faq_desc) ? $faq_data->faq_desc : '' ?></h3>
                    </div>
                </div>
                <div class="faq-wrap w-100">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-6 order-md-1">
                            <img class="img-fluid" src="<?= base_url() . (isset($faq_data->faq_image) && !empty($faq_data->faq_image) && file_exists($faq_data->faq_image) ? $faq_data->faq_image : '') ?>" alt="Faq Mockup">
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-6">
                            <div class="toggle w-100" id="toggle">
                                <?php
                                if (isset($faq_data->faq_item_data) && !empty($faq_data->faq_item_data)) {
                                    foreach ($faq_data->faq_item_data as $k3 => $v3) {
                                        $k3++;
                                        ?>
                                        <div class="toggle-item w-100">
                                            <h4 class="mb-0"><span><?= str_pad($k3, 2, "0", STR_PAD_LEFT) ?>.</span><?= isset($v3->faq_item_title) && !empty($v3->faq_item_title) ? $v3->faq_item_title : '' ?></h4>
                                            <div class="toggle-content w-100"><p class="mb-0"><?= isset($v3->faq_item_desc) && !empty($v3->faq_item_desc) ? $v3->faq_item_desc : '' ?></p></div>
                                        </div>                         
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($team_data) && !empty($team_data)) {
    ?>
    <section>
        <div class="w-100 pb-100 position-relative">
            <div class="container">
                <div class="sec-title w-100">
                    <div class="sec-title-inner d-inline-block">
                        <span class="d-block thm-clr">OUR TEAM</span>
                        <h3 class="mb-0">Our Team Member</h3>
                    </div>
                </div>
                <div class="team-wrap w-100">
                    <div class="team-inner team-caro2">
                        <?php
                        foreach ($team_data as $k7 => $v7) {
                            if (isset($v7->team_image) && !empty($v7->team_image) && file_exists($v7->team_image)) {
                                ?>
                                <div class="team-box text-center w-100">
                                    <div class="team-img rounded-circle position-relative overflow-hidden">
                                        <img class="rounded-circle img-fluid" src="<?= base_url() . (isset($v7->team_image) && !empty($v7->team_image) && file_exists($v7->team_image) ? $v7->team_image : '') ?>" alt="Team Image 1">
                                    </div>
                                    <div class="team-info w-100">
                                        <h3 class="mb-0"><?= isset($v7->team_name) && !empty($v7->team_name) ? $v7->team_name : '' ?></h3>
                                        <span class="d-block"><?= isset($v7->tema_designation) && !empty($v7->tema_designation) ? $v7->tema_designation : '' ?></span>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($other_service_data) && !empty($other_service_data)) {
    ?>
    <section>
        <div class="w-100 pt-155 pb-155 blue-layer opc85 position-relative">
            <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($other_service_data->other_sevice_image) && !empty($other_service_data->other_sevice_image) && file_exists($other_service_data->other_sevice_image) ? $other_service_data->other_sevice_image : '' ) ?>);"></div>
            <div class="particles-js" id="prtcl4"></div>
            <div class="container">
                <div class="banner-wrap position-relative text-center w-100">
                    <div class="banner-inner d-inline-block">
                        <h2 class="mb-0"><?= isset($other_service_data->other_sevice_title) && !empty($other_service_data->other_sevice_title) ? $other_service_data->other_sevice_title : '' ?></h2>
                        <p class="mb-0"><?= isset($other_service_data->other_sevice_desc) && !empty($other_service_data->other_sevice_desc) ? $other_service_data->other_sevice_desc : '' ?></p>
                        <a class="thm-btn thm-bg" href="<?= base_url() ?>About" title="">Learn More<i class="flaticon-arrow-pointing-to-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
if (isset($client_data) && !empty($client_data)) {
    ?>
    <section>
        <div class="w-100 pb-50 pt-140 position-relative">
            <div class="container">
                <div class="clients-wrap w-100">
                    <div class="team-inner team-caro2">
                        <?php
                        if (isset($client_data) && !empty($client_data)) {
                            foreach ($client_data as $k4 => $v4) {
                                if (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo)) {
                                    ?>
                                    <div class="col-md-3 col-sm-4 col-lg-2">
                                        <div class="client-box w-100">
                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?= base_url() . (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo) ? $v4->client_logo : '') ?>" alt="Client Image <?= $k4 ?>"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>