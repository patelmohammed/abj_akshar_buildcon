<footer>
    <div class="w-100 bg-color5 <?= $this->page_id == 'HOME' ? 'pt-100' : 'pt-195' ?> pb-10 position-relative">
        <div class="particles-js" id="prtcl5"></div>
        <div class="container">
            <div class="footer-data w-100">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-lg-3">
                        <div class="widget w-100">
                            <div class="logo w-100">
                                <h1 class="mb-0"><a href="<?= base_url() ?>" title="Home"><img class="img-fluid" src="<?= base_url() . (isset($footer_data->footer_logo_image) && !empty($footer_data->footer_logo_image) ? $footer_data->footer_logo_image : '') ?>" alt="Logo" srcset="<?= base_url() . (isset($footer_data->footer_logo_image) && !empty($footer_data->footer_logo_image) ? $footer_data->footer_logo_image : '') ?>"></a></h1>
                            </div>
                            <p class="mb-0"><?= isset($footer_data->footer_desc) && !empty($footer_data->footer_desc) ? $footer_data->footer_desc : '' ?></p>
                            <div class="social-links2 d-inline-block">
                                <a href="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? $footer_data->footer_facebook : 'javascript:void(0);' ?>" title="Facebook" target="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? '_blank' : '' ?>"><i class="flaticon-facebook"></i></a>
                                <a href="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? $footer_data->footer_twitter : 'javascript:void(0);' ?>" title="Twitter" target="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? '_blank' : '' ?>"><i class="flaticon-twitter"></i></a>
                                <a href="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? $footer_data->footer_insta : 'javascript:void(0);' ?>" title="Instagram" target="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? '_blank' : '' ?>"><i class="fab fa-instagram"></i></a>
                                <a href="<?= isset($footer_data->footer_google) && !empty($footer_data->footer_google) && $footer_data->footer_google != 'javascript:void(0);' ? $footer_data->footer_google : 'javascript:void(0);' ?>" title="Google Plus" target="<?= isset($footer_data->footer_google) && !empty($footer_data->footer_google) && $footer_data->footer_google != 'javascript:void(0);' ? '_blank' : '' ?>"><i class="flaticon-google-plus-logo"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-3 order-lg-1">
                        <div class="widget w-100">
                            <div class="visitor-stats w-100">
                                <div class="visitor-stat-box w-100">
                                    <h4 class="mb-0 thm-clr counter"><?= isset($footer_data->total_visitor) && !empty($footer_data->total_visitor) ? number_format($footer_data->total_visitor) : '' ?></h4>
                                    <h5 class="mb-0">Our Total visitor</h5>
                                </div>
                                <div class="visitor-stat-box w-100">
                                    <h4 class="mb-0 text-color4 counter"><?= isset($footer_data->unique_visitor) && !empty($footer_data->unique_visitor) ? number_format($footer_data->unique_visitor) : '' ?></h4>
                                    <h5 class="mb-0">Our Unique visitor</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-lg-4">
                                <div class="widget w-100">
                                    <h3>About Us</h3>
                                    <ul class="mb-0 list-unstyled w-100">
                                        <li><a href="<?= base_url() ?>Projects" title="">Our Project</a></li>
                                        <li><a href="<?= base_url() ?>About" title="">About Us</a></li>
                                        <li><a href="<?= base_url() ?>Services" title="">Our Services</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-lg-4">
                                <div class="widget w-100">
                                    <h3>Support</h3>
                                    <ul class="mb-0 list-unstyled w-100">
                                        <li><a href="<?= base_url() ?>About" title="">Get Started Us</a></li>
                                        <li><a href="<?= base_url() ?>Contact" title="">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-lg-4">
                                <div class="widget w-100">
                                    <h3>Quick Links</h3>
                                    <ul class="mb-0 list-unstyled w-100">
                                        <li><a href="<?= base_url() ?>Projects" title="">Our Work</a></li>
                                        <li><a href="<?= base_url() ?>Services" title="">Services We Offers</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade" style="z-index: 9999;" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header thm-btn thm-bg" style="height: 60px; border-radius: 0;">
                <h5 class="modal-title" style="color: #fff; font-weight: 400;" id="exampleModalLabel">Book a Consultation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 0;margin-top: -25px;">
                    <span style="color: #fff; font-weight: 400;" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="text-center w-100" action="#" method="post" id="quote-form">

                    <div class="form-group">
                        <label for="response" class="col-form-label response"></label>
                    </div>
                    <div class="form-group">
                        <label for="recipient_name" class="col-form-label">Name:</label>
                        <input type="text" class="form-control" id="recipient_name" name="recipient_name">
                    </div>
                    <div class="form-group">
                        <label for="recipient_email" class="col-form-label">Email:</label>
                        <input type="email" class="form-control" id="recipient_email" name="recipient_email">
                    </div>
                    <div class="form-group">
                        <label for="recipient_mobile" class="col-form-label">Mobile:</label>
                        <input type="text" class="form-control" id="recipient_mobile" name="recipient_mobile">
                    </div>
                    <div class="form-group">
                        <label for="message_text" class="col-form-label">Message:</label>
                        <textarea class="form-control" id="message_text" name="message_text"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="thm-btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="thm-btn bg-color1" id="quote_form_submit">Get Quote</button>
            </div>
        </div>
    </div>
</div>

<div class="copyright w-100 text-center bg-color6 position-relative">
    <div class="container">
        <p class="mb-0">Copyright by <a href="https://www.weborative.com/" title="">weborative.com</a>. All Rights Reserved</p>
    </div>
</div>
</main>
<script src="<?= base_url() ?>assets/js/popper.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/wow.min.js"></script>
<script src="<?= base_url() ?>assets/js/counterup.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.fancybox.min.js"></script>
<script src="<?= base_url() ?>assets/js/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>assets/js/slick.min.js"></script>
<script src="<?= base_url() ?>assets/js/ResizeSensor.min.js"></script>
<script src="<?= base_url() ?>assets/js/theia-sticky-sidebar.min.js"></script>
<script src="<?= base_url() ?>assets/js/particles.min.js"></script>
<script src="<?= base_url() ?>assets/js/particle-int.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?= base_url() ?>assets/js/custom-scripts.js"></script>
<script src="<?= base_url() ?>assets/plugins/gallery/masonry.pkgd.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/gallery/imagesloaded.pkgd.min.js"></script>

<script>
    $(document).ready(function () {
        var $grid = $('.grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true,
            columnWidth: '.grid-sizer'
        });

        $grid.imagesLoaded().progress(function () {
            $grid.masonry();
        });
    });
</script>

</body>
</html>