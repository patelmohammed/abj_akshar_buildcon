<section>
    <div class="w-100 pt-170 pb-150 dark-layer3 opc7 position-relative">
        <div class="fixed-bg" style="background-image: url(assets/images/pagetop-bg.jpg);"></div>
        <div class="container">
            <div class="page-top-wrap w-100">
                <h1 class="mb-0">Latest Project</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>" title="">Home</a></li>
                    <li class="breadcrumb-item active">Project</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<?php if (isset($project_data) && !empty($project_data)) { ?>
    <section>
        <div class="w-100 pt-100 pb-100 position-relative">
            <div class="container">
                <div class="proj-wrap w-100">
                    <div class="row">
                        <?php foreach ($project_data as $k1 => $v1) { ?>
                            <div class="col-md-4 col-sm-6 col-lg-3">
                                <div class="proj-box position-relative w-100">
                                    <div class="proj-thumb overflow-hidden w-100"><a href="<?= isset($v1->project_id) && !empty($v1->project_id) ? base_url('Projects/projectDetail/') . $v1->project_id : '' ?>" title=""><img class="img-fluid w-100" src="<?= isset($v1->project_poster_image) && !empty($v1->project_poster_image) && file_exists($v1->project_poster_image) ? $v1->project_poster_image : '' ?>" alt="Project Image 1"></a></div>
                                    <div class="proj-info position-absolute">
                                        <i class="<?= isset($v1->project_icon) && !empty($v1->project_icon) ? $v1->project_icon : 'flaticon-builder' ?>"></i>
                                        <h3 class="mb-0"><a href="<?= isset($v1->project_id) && !empty($v1->project_id) ? base_url('Projects/projectDetail/') . $v1->project_id : '' ?>" title=""><?= isset($v1->project_name) && !empty($v1->project_name) ? $v1->project_name : '' ?></a></h3>
                                    </div>
                                </div>
                            </div>   
                        <?php } ?>
                    </div>
                </div>
                <div class="pagination-wrap mt-10">
                    <ul class="pagination justify-content-center">
                        <li class="page-item"><a class="page-link" href="javascript:void(0);" title=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">1</a></li>
                        <li class="page-item active"><a class="page-link" href="javascript:void(0);" title="">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);" title="">3</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0);" title=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <!--            <div class="view-all mt-20 w-100 text-center">
                                <a class="thm-btn thm-bg" href="projects.php" title="">View All Portfolio<i class="flaticon-arrow-pointing-to-right"></i></a>
                            </div>-->
            </div>
        </div>
    </section>
<?php } ?>
<?php if (isset($client_data) && !empty($client_data)) {
    ?>
    <section>
        <div class="w-100 pt-50 pb-50 position-relative">
            <div class="container">
                <div class="clients-wrap w-100">
                    <div class="team-inner team-caro2">
                        <?php
                        if (isset($client_data) && !empty($client_data)) {
                            foreach ($client_data as $k4 => $v4) {
                                if (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo)) {
                                    ?>
                                    <div class="col-md-3 col-sm-4 col-lg-2">
                                        <div class="client-box w-100">
                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?= base_url() . (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo) ? $v4->client_logo : '') ?>" alt="Client Image <?= $k4 ?>"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
}
if (isset($get_in_touch_data) && !empty($get_in_touch_data)) {
    ?>
    <section>
        <div class="w-100 position-relative">
            <div class="container">
                <div class="getin-touch-wrap overlap-99 brd-rd5 style2 thm-layer opc8 w-100 overflow-hidden position-relative">
                    <div class="fixed-bg" style="background-image: url(<?= base_url() . (isset($get_in_touch_data->get_in_touch_image) && !empty($get_in_touch_data->get_in_touch_image) && file_exists($get_in_touch_data->get_in_touch_image) ? $get_in_touch_data->get_in_touch_image : '') ?>);"></div>
                    <div class="row align-items-center justify-content-between">
                        <div class="col-md-7 col-sm-12 col-lg-5">
                            <div class="getin-touch-title w-100">
                                <span class="d-block"><?= isset($get_in_touch_data->get_in_touch_title) && !empty($get_in_touch_data->get_in_touch_title) ? $get_in_touch_data->get_in_touch_title : '' ?></span>
                                <h2 class="mb-0"><?= isset($get_in_touch_data->get_in_touch_desc) && !empty($get_in_touch_data->get_in_touch_desc) ? $get_in_touch_data->get_in_touch_desc : '' ?></h2>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-lg-4">
                            <div class="getin-touch-btn text-right">
                                <a class="thm-btn bg-color1" href="<?= base_url() ?>Contact" title="">Book An Appointment<i class="flaticon-arrow-pointing-to-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>