<section>
    <div class="w-100 pt-170 pb-150 dark-layer3 opc7 position-relative">
        <div class="fixed-bg" style="background-image: url(<?= base_url() ?>assets/images/pagetop-bg.jpg);"></div>
        <div class="container">
            <div class="page-top-wrap w-100">
                <h1 class="mb-0"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>" title="">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('Projects') ?>" title="">Projects</a></li>
                    <li class="breadcrumb-item active"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="w-100 pt-100 pb-100 position-relative">
        <div class="container">
            <div class="post-detail-wrap w-100">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-8">
                        <div class="post-detail w-100">
                            <h2 class="mb-20 text-color1"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></h2>
                            <?php
                            echo (isset($project_data->main_desc) && !empty($project_data->main_desc) ? $project_data->main_desc : '');
                            if (isset($project_data->project_image) && !empty($project_data->project_image)) {
                                ?>
                                <div class="post-detail-gallery-video-box position-relative w-100">
                                    <?php if (count($project_data->project_image) > 0) { ?>
                                        <h2 class="mt-10 mb-50 text-color1" style="font-size: 1.5rem !important;">Project Gallery</h2>
                                    <?php } ?>
                                    <div class="grid">
                                        <div class="grid-sizer"></div>
                                        <?php
                                        foreach ($project_data->project_image as $k2 => $v2) {
                                            if (isset($v2->project_image) && !empty($v2->project_image) && file_exists($v2->project_image)) {
                                                ?>
                                                <div class="grid-item">
                                                    <a href="<?= base_url() . $v2->project_image ?>" data-fancybox="gallery">
                                                        <img alt=project src="<?= base_url() . $v2->project_image ?>">
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-lg-4">
                        <aside class="sidebar-wrap w-100">
                            <div class="widget2 brd-rd5 w-100">
                                <div class="about-widget text-center w-100">
                                    <div class="about-widget-img d-inline-block">
                                        <?php if (isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);') { ?>
                                            <a class="thm-bg rounded-circle" href="<?= $footer_data->footer_facebook ?>" target="_blank" title=""><i class="fab fa-facebook-f"></i></a>
                                        <?php } ?>
                                        <img class="img-fluid rounded-circle" src="<?= base_url() . (isset($project_data->project_widget) && !empty($project_data->project_widget) && file_exists($project_data->project_widget) ? $project_data->project_widget : '') ?>" alt="Project Widget">
                                    </div>
                                    <h4 class="mb-0"><?= isset($project_data->project_name) && !empty($project_data->project_name) ? $project_data->project_name : '' ?></h4>
                                    <p class="mb-0"><?= isset($project_data->project_desc) && !empty($project_data->project_desc) ? $project_data->project_desc : '' ?></p>
                                    <span class="d-block"><img class="img-fluid" src="<?= base_url() ?>assets/images/resources/about-widget-icon1.png" alt="About Widget Icon 1"><?= isset($project_data->project_location) && !empty($project_data->project_location) ? $project_data->project_location : '' ?></span>
                                </div>
                            </div>
                            <?php if (isset($all_project_data) && !empty($all_project_data) && count($all_project_data) > 1) { ?>
                                <div class="widget2 category_widget brd-rd5 w-100">
                                    <h3>Projects</h3>
                                    <ul class="mb-0 w-100" style="">
                                        <?php
                                        foreach ($all_project_data as $k3 => $v3) {
                                            $k3++;
                                            if ($v3->project_id != (isset($project_data->project_id) && !empty($project_data->project_id) ? $project_data->project_id : '') && $k3 < 8) {
                                                ?>
                                                <li><a href="<?= base_url('Projects/projectDetail/') . $v3->project_id ?>" title=""><?= isset($v3->project_name) && !empty($v3->project_name) ? $v3->project_name : '' ?></a></li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                            }
                            if (isset($project_data->project_video) && !empty($project_data->project_video)) {
                                ?>
                                <div class="widget2 p-0 video-widget brd-rd5 w-100" >
                                    <div class="widget-video-box w-100 position-relative">
                                        <img class="img-fluid w-100" src="<?= base_url() ?>assets/images/resources/widget-video-img.jpg" alt="Widget Video Image" style="height: 300px;">
                                        <a class="spinner" href="<?= isset($project_data->project_video) && !empty($project_data->project_video) ? $project_data->project_video : '' ?>" data-fancybox title=""><i class="fas fa-plus"></i></a>
                                    </div>
                                </div>
                            <?php } ?>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (isset($client_data) && !empty($client_data)) {
    ?>
    <section>
        <div class="w-100 pb-50 pt-100 position-relative">
            <div class="container">
                <div class="clients-wrap w-100">
                    <div class="team-inner team-caro2">
                        <?php
                        if (isset($client_data) && !empty($client_data)) {
                            foreach ($client_data as $k4 => $v4) {
                                if (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo)) {
                                    ?>
                                    <div class="col-md-3 col-sm-4 col-lg-2">
                                        <div class="client-box w-100">
                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="<?= base_url() . (isset($v4->client_logo) && !empty($v4->client_logo) && file_exists($v4->client_logo) ? $v4->client_logo : '') ?>" alt="Client Image <?= $k4 ?>"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<section>
    <div class="w-100 position-relative">
        <div class="container">
            <div class="getin-touch-wrap overlap-99 brd-rd5 style2 thm-layer opc8 w-100 overflow-hidden position-relative">
                <div class="fixed-bg" style="background-image: url(<?= base_url() ?>assets/images/parallax2.jpg);"></div>
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-7 col-sm-12 col-lg-5">
                        <div class="getin-touch-title w-100">
                            <span class="d-block"><?= isset($get_in_touch_data->get_in_touch_title) && !empty($get_in_touch_data->get_in_touch_title) ? $get_in_touch_data->get_in_touch_title : '' ?></span>
                            <h2 class="mb-0"><?= isset($get_in_touch_data->get_in_touch_desc) && !empty($get_in_touch_data->get_in_touch_desc) ? $get_in_touch_data->get_in_touch_desc : '' ?></h2>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-lg-4">
                        <div class="getin-touch-btn text-right">
                            <a class="thm-btn bg-color1" href="<?= base_url() ?>Contact" title="">Book Visit Now<i class="flaticon-arrow-pointing-to-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>