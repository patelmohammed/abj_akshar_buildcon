<?php

class Mail extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'MAIL';
        $this->load->model('Mail_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'SMTP_SETTING';
        $data['smtp_data'] = $this->Common_model->getDataById2('smtp_setting', 'del_status', 'Live', 'Live');
        $id = $data['smtp_data']->smtp_id;
        if ($this->input->post()) {
            $insert_data['smtp_host'] = $this->input->post('smtp_host');
            $insert_data['smtp_port'] = $this->input->post('smtp_port');
            $insert_data['smtp_user'] = $this->input->post('smtp_user');
            $insert_data['smtp_pass'] = $this->input->post('smtp_pass');
            $insert_data['smtp_crypto'] = $this->input->post('smtp_crypto');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'smtp_id', $id, 'smtp_setting');
            }
            redirect('admin/Mail');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SMTP_SETTING', 'EDIT');
                $view = 'admin/mail/smtp_setting/editSmtpSetting';
                $this->page_title = 'SMTP SETTING';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SMTP_SETTING', 'ADD');
                $this->_show_message("You cant insert new smtp setting", "error");
                redirect('admin/Mail');
            }
        }
    }

    public function newsletter() {
        $data = [];
        $this->menu_id = 'NEWSLETTER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('NEWSLETTER');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('NEWSLETTER', 'VIEW');
        $data['newsletter_data'] = $this->Common_model->geAlldata('newsletter');
        $data['newsletter'] = $this->Common_model->geAlldata('newsletter_section');
        $view = 'admin/mail/newsletter';
        $this->page_title = 'NEWSLETTER';
        $this->load_admin_view($view, $data);
    }

    public function addEditNewsletter($encrypted_id = "") {
        $this->menu_id = 'NEWSLETTER';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['newsletter_title'] = $this->input->post('newsletter_title');
            $insert_data['newsletter_desc'] = $this->input->post('newsletter_desc');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'id', $id, 'newsletter_section');
            }
            redirect('admin/Mail/newsletter');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('NEWSLETTER', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['newsletter_data'] = $this->Common_model->getDataById2('newsletter_section', 'id', $id, 'Live');
                $view = 'admin/mail/editNewsletter';
                $this->page_title = 'NEWSLETTER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('NEWSLETTER', 'ADD');
                $this->_show_message("You cant insert new newsletter", "error");
                redirect('admin/Mail');
            }
        }
    }

    public function contact_us() {
        $data = [];
        $this->menu_id = 'CONTACT_US';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CONTACT_US');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CONTACT_US', 'VIEW');
        $data['contact_us_data'] = $this->Common_model->geAlldata('contact_us');
        $view = 'admin/mail/contact_us';
        $this->page_title = 'CONTACT US';
        $this->load_admin_view($view, $data);
    }

    public function get_quote() {
        $data = [];
        $this->menu_id = 'GET_QUOTE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('GET_QUOTE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('GET_QUOTE', 'VIEW');
        $data['get_quote_data'] = $this->Common_model->geAlldata('tbl_get_quote');
        $view = 'admin/mail/get_quote';
        $this->page_title = 'GET QUOTE';
        $this->load_admin_view($view, $data);
    }

    public function deleteNewsletter() {
        $this->Common_model->check_menu_access('NEWSLETTER', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('newsletter_id', $id);
            $this->db->update('newsletter');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deleteContactUs() {
        $this->Common_model->check_menu_access('CONTACT_US', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('contact_us_id', $id);
            $this->db->update('contact_us');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deleteGetQuote() {
        $this->Common_model->check_menu_access('GET_QUOTE', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('get_quote_id', $id);
            $this->db->update('tbl_get_quote');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function mailTemplate() {
        $data = [];
        $this->menu_id = 'MAIL_TEMPLATE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MAIL_TEMPLATE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MAIL_TEMPLATE', 'VIEW');
        $data['template_data'] = $this->Common_model->geAlldata('tbl_mail_template');
        $view = 'admin/mail/mail_template/mail_template';
        $this->page_title = 'MAIL TEMPLATE';
        $this->load_admin_view($view, $data);
    }

    public function addEditMailTemplate($encrypted_id = "") {
        $this->menu_id = 'MAIL_TEMPLATE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['subject'] = $this->input->post('subject');
            $insert_data['message'] = $this->input->post('message');
            $insert_data['fromname'] = $this->input->post('fromname');
            $insert_data['fromemail'] = $this->input->post('fromemail');

            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'mail_template_id', $id, 'tbl_mail_template');
            }
            redirect('admin/Mail/mailTemplate');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('MAIL_TEMPLATE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['template_data'] = $this->Common_model->getDataById2('tbl_mail_template', 'mail_template_id', $id, 'Live');
                $view = 'admin/mail/mail_template/editMailTemplate';
                $this->page_title = 'MAIL TEMPLATE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function mailTemplateById() {
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $data['template'] = $this->Common_model->getDataById2('tbl_mail_template', 'mail_template_id', $id, 'Live');
            if (isset($data['template']) && !empty($data['template'])) {
                $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
                $data['template']->message = $this->load->view('email_template', $data, true);
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function smsTemplate() {
        $data = [];
        $this->menu_id = 'SMS_TEMPLATE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SMS_TEMPLATE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SMS_TEMPLATE', 'VIEW');
        $data['template_data'] = $this->Common_model->geAlldata('tbl_sms_template');
        $view = 'admin/mail/sms_template/sms_template';
        $this->page_title = 'MAIL TEMPLATE';
        $this->load_admin_view($view, $data);
    }

    public function addEditSmsTemplate($encrypted_id = "") {
        $this->menu_id = 'SMS_TEMPLATE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['message'] = $this->input->post('message');

            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'sms_template_id ', $id, 'tbl_sms_template');
            }
            redirect('admin/Mail/smsTemplate');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SMS_TEMPLATE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['template_data'] = $this->Common_model->getDataById2('tbl_sms_template', 'sms_template_id', $id, 'Live');
                $view = 'admin/mail/sms_template/editSmsTemplate';
                $this->page_title = 'SMS TEMPLATE';
                $this->load_admin_view($view, $data);
            }
        }
    }

}
