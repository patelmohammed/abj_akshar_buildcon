<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->data['page_id'] = '';
    }

    public function index() {
        if ($this->is_login()) {
            redirect(base_url('admin/dashboard'));
        }
        $data = [];
        $data['show_error'] = '';
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $usr_data = $this->Auth_model->getUserByEmail($username);
            if (!empty($usr_data)) {
                if ($this->check_pwd($password, $usr_data->password)) {
                    $user_data = array();
                    $user_data['user_id'] = $usr_data->id;
                    $user_data['user_name'] = $usr_data->user_name;
                    $user_data['user_email'] = $usr_data->user_email;
                    $user_data['role'] = $usr_data->role;

                    $menu_data = $this->Auth_model->get_assigned_menu($usr_data->id, $usr_data->role);
                    $user_data['side_menu'] = $menu_data;

                    $this->session->set_userdata($user_data);
                    redirect(base_url('admin/Dashboard'));
                } else {
                    $this->session->sess_destroy();
                    $this->_show_message("wrong password", "error");
                }
            } else {
                $this->session->sess_destroy();
                $this->_show_message("wrong username", "error");
            }
        }
        $this->page_id = 'login_pg';
        $this->page_title = 'Login';
        $view = 'admin/auth/login';
        $this->load_admin_view($view, $data, false);
    }

    function check_pwd($password, $db) {
        return md5($password) == $db;
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('admin/Auth');
    }

    public function forgotPassword() {
        if ($this->is_login()) {
            redirect(base_url('admin/dashboard'));
        }
        $data = [];
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $usr_data = $this->Auth_model->getUserByEmail($username);
            if (!empty($usr_data)) {
                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);

                $this->email->from($from_email, $from_name);
                $this->email->subject('Forgot Password' . ADDITIONAL_EMAIL_SUBJECT);
                $this->email->message('Please use the following link to <a href="' . base_url() . 'admin/Auth/resetPassword/' . $usr_data->id . '">Reset your password.</a>');
                $this->email->to($username);
                if ($this->email->send()) {
                    $this->_show_message("Forgot password link sent to your mail.", "success");
                    redirect(base_url('admin/Auth'));
                }
            } else {
                $this->session->sess_destroy();
                $this->_show_message("wrong username", "error");
            }
        }
        $this->page_id = 'forgot_pass';
        $this->page_title = 'Forgot Password';
        $view = 'admin/auth/forgot_password';
        $this->load_admin_view($view, $data, false);
    }

    public function resetPassword($id) {
        if ($this->is_login()) {
            redirect(base_url('admin/dashboard'));
        }
        $data = [];
        if ($this->input->post()) {
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['password'] = md5($password);
                    $this->Common_model->updateInformation2($insert_data, 'id', $id, 'user_information');
                    $this->_show_message("Password reset successfully.", "success");
                    redirect(base_url('admin/Auth'));
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    redirect(base_url('admin/Auth/resetPassword/') . $id);
                }
            }
        }
        $data['id'] = $id;
        $this->page_id = 'reset_password';
        $this->page_title = 'Reset Password';
        $view = 'admin/auth/reset_password';
        $this->load_admin_view($view, $data, false);
    }

    public function Unauthorized() {
        $data['title'] = 'Access Denied';
        $this->load_admin_view("admin/home/access_denied", $data);
    }

}
