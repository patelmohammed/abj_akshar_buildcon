<?php

class Team extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'TEAM';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->page_id = 'TEAM_CATEGORY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('TEAM_CATEGORY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('TEAM_CATEGORY', 'VIEW');
        $data['team_categrooy_data'] = $this->Common_model->geAlldata('tbl_team_category');
        $view = 'admin/team/team_category/team_category';
        $this->page_title = 'TEAM CATEGORY';
        $this->load_admin_view($view, $data);
    }

    public function addEditTeamCategory($encrypted_id = "") {
        $this->page_id = 'TEAM_CATEGORY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['team_category_name'] = $this->input->post('team_category_name');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_team_category');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'team_category_id ', $id, 'tbl_team_category');
            }
            redirect('admin/Team');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('TEAM_CATEGORY', 'ADD');
                $data = [];
                $view = 'admin/team/team_category/addTeamCategory';
                $this->page_title = 'TEAM CATEGORY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('TEAM_CATEGORY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['team_category_data'] = $this->Common_model->getDataById2('tbl_team_category', 'team_category_id  ', $id, 'Live');
                $view = 'admin/team/team_category/editTeamCategory';
                $this->page_title = 'TEAM CATEGORY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteTeamCategory() {
        $this->Common_model->check_menu_access('TEAM_CATEGORY', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('team_category_id  ', $id);
            $this->db->update('tbl_team_category');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deactiveTeamCategory() {
        $update_data = $data = array();
        $client_id = $this->input->post('team_category_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'team_category_id', $client_id, 'tbl_team_category', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Something went wrong.';
        }
        echo json_encode($data);
        die;
    }

    public function activeTeamCategory() {
        $update_data = $data = array();
        $client_id = $this->input->post('team_category_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'team_category_id', $client_id, 'tbl_team_category', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function teamMember() {
        $data = [];
        $this->page_id = 'TEAM_MEMBER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('TEAM_MEMBER');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('TEAM_MEMBER', 'VIEW');
        $data['team_data'] = $this->Common_model->getTeamMemberData();
        $view = 'admin/team/team_member/team_member';
        $this->page_title = 'TEAM MEMBER';
        $this->load_admin_view($view, $data);
    }

    public function addEditTeamMemeber($encrypted_id = "") {
        $this->page_id = 'TEAM_MEMBER';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['ref_team_category_id'] = $this->input->post('ref_team_category_id');
            $insert_data['team_name'] = $this->input->post('team_name');
            $insert_data['tema_designation'] = $this->input->post('tema_designation');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $new_path = 'assets/images/resources/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['team_image']['name']) && isset($_FILES['team_image']['name'])) {
                if ($_FILES['team_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('team_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Team/teamMember');
                    } else {
                        $old_image = $this->input->post('hidden_team_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $team_image = $new_path . $image['file_name'];
                    }
                } else {
                    $team_image = $this->input->post('hidden_team_image');
                }
            } else {
                $team_image = $this->input->post('hidden_team_image');
            }
            $insert_data['team_image'] = $team_image;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_team');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'team_id', $id, 'tbl_team');
            }
            redirect('admin/Team/teamMember');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('TEAM_MEMBER', 'ADD');
                $data = [];
                $data['team_category_data'] = $this->Common_model->geAlldataById('tbl_team_category', 'is_active', 1, 'del_status = "Live"');
                $view = 'admin/team/team_member/addTeamMember';
                $this->page_title = 'TEAM MEMBER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('TEAM_MEMBER', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['team_category_data'] = $this->Common_model->geAlldataById('tbl_team_category', 'is_active', 1, 'del_status = "Live"');
                $data['team_member_data'] = $this->Common_model->getDataById2('tbl_team', 'team_id', $id, 'Live');
                $view = 'admin/team/team_member/editTeamMember';
                $this->page_title = 'TEAM MEMBER';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteTeamMember() {
        $this->Common_model->check_menu_access('TEAM_MEMBER', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('team_id', $id);
            $this->db->update('tbl_team');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deactiveTeamMember() {
        $update_data = $data = array();
        $client_id = $this->input->post('team_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'team_id', $client_id, 'tbl_team', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Something went wrong.';
        }
        echo json_encode($data);
        die;
    }

    public function activeTeamMember() {
        $update_data = $data = array();
        $client_id = $this->input->post('team_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'team_id', $client_id, 'tbl_team', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
