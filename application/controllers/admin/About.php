<?php

class About extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'ABOUT';
        $this->load->model('About_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'ABOUT';
        $data['about_us_data'] = $this->Common_model->getDataByIdStatus('about_us', 'del_status', 'Live');
        $id = $data['about_us_data']->about_us_id;
        if ($this->input->post()) {
            $insert_data['about_us_title_1'] = $this->input->post('about_us_title_1');
            $insert_data['about_us_title_2'] = $this->input->post('about_us_title_2');
            
            $insert_data['about_us_desc_1'] = $this->input->post('about_us_desc_1');
            $insert_data['about_us_desc_2'] = $this->input->post('about_us_desc_2');

            $insert_data['about_us_alt_sub_icon_1'] = $this->input->post('about_us_alt_sub_icon_1');
            $insert_data['about_us_alt_sub_title_1'] = $this->input->post('about_us_alt_sub_title_1');
            $insert_data['about_us_alt_sub_desc_1'] = $this->input->post('about_us_alt_sub_desc_1');

            $insert_data['about_us_alt_sub_icon_2'] = $this->input->post('about_us_alt_sub_icon_2');
            $insert_data['about_us_alt_sub_title_2'] = $this->input->post('about_us_alt_sub_title_2');
            $insert_data['about_us_alt_sub_desc_2'] = $this->input->post('about_us_alt_sub_desc_2');
            
            $insert_data['about_us_alt_sub_icon_3'] = $this->input->post('about_us_alt_sub_icon_3');
            $insert_data['about_us_alt_sub_title_3'] = $this->input->post('about_us_alt_sub_title_3');
            $insert_data['about_us_alt_sub_desc_3'] = $this->input->post('about_us_alt_sub_desc_3');
            
            $insert_data['about_us_alt_sub_icon_4'] = $this->input->post('about_us_alt_sub_icon_4');
            $insert_data['about_us_alt_sub_title_4'] = $this->input->post('about_us_alt_sub_title_4');
            $insert_data['about_us_alt_sub_desc_4'] = $this->input->post('about_us_alt_sub_desc_4');
            
            $insert_data['image_title'] = $this->input->post('image_title');

            $new_path = 'assets/images/resources/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['about_us_image_1']['name']) && isset($_FILES['about_us_image_1']['name'])) {
                if ($_FILES['about_us_image_1']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('about_us_image_1')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About');
                    } else {
                        $old_image = $this->input->post('hidden_about_us_image_1');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_about_us_image_1');
                }
            } else {
                $image_url = $this->input->post('hidden_about_us_image_1');
            }
            $insert_data['about_us_image_1'] = $image_url;
            
            if (!empty($_FILES['about_us_image_2']['name']) && isset($_FILES['about_us_image_2']['name'])) {
                if ($_FILES['about_us_image_2']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('about_us_image_2')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About');
                    } else {
                        $old_image = $this->input->post('hidden_about_us_image_2');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_about_us_image_2');
                }
            } else {
                $image_url = $this->input->post('hidden_about_us_image_2');
            }
            $insert_data['about_us_image_2'] = $image_url;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'about_us_id', $id, 'about_us');
            }
            redirect('admin/About');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('ABOUT', 'EDIT');
                $view = 'admin/about/editAboutUs';
                $this->page_title = 'ABOUT US';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('ABOUT', 'ADD');
                $this->_show_message("You cant insert new about us detail", "error");
                redirect('admin/About');
            }
        }
    }

}
