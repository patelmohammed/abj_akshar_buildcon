<?php

class Service extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'SERVICE';
        $this->load->model('Common_model');
        $this->load->model('Service_moodel');
    }

    public function index() {
        $this->menu_id = 'SERVICE_SETTING';
        $data['service_setting_data'] = $this->Common_model->getDataByIdStatus('tbl_service_setting', 'del_status', 'Live');
        $id = $data['service_setting_data']->service_setting_id;
        if ($this->input->post()) {
            $insert_data['service_title_1'] = $this->input->post('service_title_1');
            $insert_data['service_title_2'] = $this->input->post('service_title_2');

            $insert_data['service_desc'] = $this->input->post('service_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'service_setting_id', $id, 'tbl_service_setting');
            }
            redirect('admin/Service');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SERVICE_SETTING', 'EDIT');
                $view = 'admin/service/editServiceSetting';
                $this->page_title = 'SERVICE_SETTING';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SERVICE_SETTING', 'ADD');
                $this->_show_message("You cant insert new about us detail", "error");
                redirect('admin/Service');
            }
        }
    }

    public function serviceItem() {
        $data = [];
        $this->page_id = 'SERVICE_ITEM';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SERVICE_ITEM');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SERVICE_ITEM', 'VIEW');
        $data['service_item_data'] = $this->Common_model->geAlldata('tbl_service_item');
        $view = 'admin/service/service_item/service_item';
        $this->page_title = 'SERVICE ITEM';
        $this->load_admin_view($view, $data);
    }

    public function addEditServiceItem($encrypted_id = "") {
        $this->page_id = 'SERVICE_ITEM';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['service_title'] = $this->input->post('service_title');
            $insert_data['service_icon'] = $this->input->post('service_icon');
            $insert_data['service_desc'] = $this->input->post('service_desc');
            $insert_data['service_sub_desc'] = $this->input->post('service_sub_desc');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $id = $this->Common_model->insertInformation($insert_data, 'tbl_service_item');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'service_id ', $id, 'tbl_service_item');
            }
            redirect('admin/Service/serviceItem');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('SERVICE_ITEM', 'ADD');
                $data = [];
                $view = 'admin/service/service_item/addServiceItem';
                $this->page_title = 'SERVICE ITEM';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SERVICE_ITEM', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['service_data'] = $this->Common_model->getDataById2('tbl_service_item', 'service_id', $id, 'Live');
                $view = 'admin/service/service_item/editServiceItem';
                $this->page_title = 'PROJECT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteService() {
        $this->Common_model->check_menu_access('SERVICE_ITEM', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('service_id  ', $id);
            $this->db->update('tbl_service_item');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deactiveService() {
        $update_data = $data = array();
        $service_id = $this->input->post('service_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'service_id', $service_id, 'tbl_service_item', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Something went wrong.';
        }
        echo json_encode($data);
        die;
    }

    public function activeService() {
        $update_data = $data = array();
        $service_id = $this->input->post('service_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'service_id', $service_id, 'tbl_service_item', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
