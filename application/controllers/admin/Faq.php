<?php

class Faq extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'FAQ';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'FAQ';
        $data['faq_data'] = $this->Common_model->getDataByIdStatus('tbl_faq', 'del_status', 'Live');
        if (isset($data['faq_data']) && !empty($data['faq_data'])) {
            $data['faq_data']->faq_item_data = $this->Common_model->geAlldataById('tbl_faq_item', 'ref_faq_id', $data['faq_data']->faq_id);
        }
        $id = $data['faq_data']->faq_id;
        if ($this->input->post()) {
            $insert_data['faq_title'] = $this->input->post('faq_title');
            $insert_data['faq_desc'] = $this->input->post('faq_desc');

            $new_path = 'assets/images/resources/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['faq_image']['name']) && isset($_FILES['faq_image']['name'])) {
                if ($_FILES['faq_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('faq_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_faq_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $faq_image = $new_path . $image['file_name'];
                    }
                } else {
                    $faq_image = $this->input->post('hidden_faq_image');
                }
            } else {
                $faq_image = $this->input->post('hidden_faq_image');
            }
            $insert_data['faq_image'] = $faq_image;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->deleteRecord($id, 'tbl_faq_item', 'ref_faq_id');
                $this->Common_model->insertFaqItem($id);
                $this->Common_model->updateInformation2($insert_data, 'faq_id', $id, 'tbl_faq');
            }
            redirect('admin/Faq');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('FAQ', 'EDIT');
                $view = 'admin/footer/editFaq';
                $this->page_title = 'FAQ';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SERVICE_SETTING', 'ADD');
                $this->_show_message("You cant insert new detail", "error");
                redirect('admin');
            }
        }
    }

}
