<?php

class Slider extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'HOME';
        $this->load->model('Slider_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'SLIDER';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SLIDER');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SLIDER', 'VIEW');
        $view = 'admin/slider/slider';
        $data['slider_data'] = $this->Common_model->geAlldata('slider');
        $this->page_title = 'SLIDER';
        $this->load_admin_view($view, $data);
    }

    public function addEditSlider($encrypted_id = "") {
        $this->menu_id = 'SLIDER';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $new_path = 'assets/images/resources/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['slider_image']['name']) && isset($_FILES['slider_image']['name'])) {
                if ($_FILES['slider_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('slider_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Slider');
                    } else {
                        $old_image = $this->input->post('hidden_slider_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_slider_image');
                }
            } else {
                $image_url = $this->input->post('hidden_slider_image');
            }
            $insert_data['slider_image'] = $image_url;
            $insert_data['slider_order_no'] = $this->input->post('order_number');
            $insert_data['slider_title'] = $this->input->post('slider_title');
            $insert_data['slider_description'] = $this->input->post('slider_description');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'slider_id', $id, 'slider');
            }
            redirect('admin/Slider');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SLIDER', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['slider_data'] = $this->Common_model->getDataById2('slider', 'slider_id', $id, 'Live');
                $view = 'admin/slider/editSlider';
                $this->page_title = 'SLIDER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SLIDER', 'ADD');
                $this->_show_message("You cant insert new slider", "error");
                redirect('admin/Slider');
            }
        }
    }

    public function deactiveSlider() {
        $update_data = $data = array();
        $slider_id = $this->input->post('slider_id');
        $slider_data = $this->Common_model->geAlldataById('slider', 'is_active', 1, '');
        if (isset($slider_data) && !empty($slider_data) && count($slider_data) > 1) {
            $update_data['is_active'] = 2;
            $response = $this->Common_model->updateInformation2($update_data, 'slider_id', $slider_id, 'slider', 'Live');
            if ($response == TRUE) {
                $data['result'] = TRUE;
            } else {
                $data['result'] = FALSE;
                $data['message'] = 'Something went wrong.';
            }
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Minimum 1 slider required.';
        }
        echo json_encode($data);
        die;
    }

    public function activeSlider() {
        $update_data = $data = array();
        $slider_id = $this->input->post('slider_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'slider_id', $slider_id, 'slider', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
