<?php

class Contact extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'CONTACT';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'CONTACT';
        $data['contact_data'] = $this->Common_model->getDataById2('contact', 'del_status', 'Live', 'Live');
        $id = $data['contact_data']->contact_id;
        if ($this->input->post()) {
                $insert_data['contact_number_1'] = $this->input->post('contact_number_1');
            $insert_data['contact_number_2'] = $this->input->post('contact_number_2');
            $insert_data['email'] = $this->input->post('email');
            $insert_data['email_alt'] = $this->input->post('email_alt');
            $insert_data['address'] = $this->input->post('address');
            $insert_data['map'] = $this->input->post('map');
            $insert_data['contact_us_title'] = $this->input->post('contact_us_title');
            $insert_data['contact_us_desc'] = $this->input->post('contact_us_desc');
            $insert_data['contact_us_sub_desc'] = $this->input->post('contact_us_sub_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Contact');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'contact_id', $id, 'contact');
            }
            redirect('admin/Contact');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('CONTACT', 'EDIT');
                $view = 'admin/contact/editContact';
                $this->page_title = 'CONTACT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CONTACT', 'ADD');
                $this->_show_message("You cant insert new contact detail", "error");
                redirect('admin/Contact');
            }
        }
    }

}
