<?php

class Footer extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'FOOTER';
        $this->load->model('Footer_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'FOOTER_SETTING';
        $data['footer_data'] = $this->Common_model->getDataByIdStatus('footer_setting', 'del_status', 'Live');
        $id = $data['footer_data']->footer_id;
        if ($this->input->post()) {

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['logo']['name']) && isset($_FILES['logo']['name'])) {
                if ($_FILES['logo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('logo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Footer');
                    } else {
                        $old_image = $this->input->post('hidden_logo');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_logo');
                }
            } else {
                $image_url = $this->input->post('hidden_logo');
            }
            $insert_data['logo_image'] = $image_url;
            
            if (!empty($_FILES['footer_logo']['name']) && isset($_FILES['footer_logo']['name'])) {
                if ($_FILES['footer_logo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('footer_logo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Footer');
                    } else {
                        $old_image = $this->input->post('hidden_footer_logo');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_footer_logo');
                }
            } else {
                $image_url = $this->input->post('hidden_footer_logo');
            }
            $insert_data['footer_logo_image'] = $image_url;

            $insert_data['footer_desc'] = $this->input->post('footer_desc');
            $insert_data['address'] = $this->input->post('address');
            $insert_data['email'] = $this->input->post('email');
            $insert_data['total_visitor'] = $this->input->post('total_visitor');
            $insert_data['unique_visitor'] = $this->input->post('unique_visitor');
            $insert_data['contact_number'] = $this->input->post('contact_number');
            $insert_data['contact_title'] = $this->input->post('contact_title');

            $footer_facebook = $this->input->post('footer_facebook');
            $insert_data['footer_facebook'] = (isset($footer_facebook) && !empty($footer_facebook) ? $footer_facebook : 'javascript:void(0);');

            $footer_twitter = $this->input->post('footer_twitter');
            $insert_data['footer_twitter'] = (isset($footer_twitter) && !empty($footer_twitter) ? $footer_twitter : 'javascript:void(0);');

            $footer_insta = $this->input->post('footer_insta');
            $insert_data['footer_insta'] = (isset($footer_insta) && !empty($footer_insta) ? $footer_insta : 'javascript:void(0);');

            $footer_google = $this->input->post('footer_google');
            $insert_data['footer_google'] = (isset($footer_google) && !empty($footer_google) ? $footer_google : 'javascript:void(0);');


            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'footer_id', $id, 'footer_setting');
            }
            redirect('admin/Footer');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('FOOTER_SETTING', 'EDIT');
                $data = [];
                $data['footer_data'] = $this->Common_model->getDataById2('footer_setting', 'footer_id', $id, 'Live');
                $view = 'admin/footer/editFooterSetting';
                $this->page_title = 'FOOTER SETTING';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('FOOTER_SETTING', 'ADD');
                $this->_show_message("You cant insert new contact detail", "error");
                redirect('admin/Footer');
            }
        }
    }

    public function privacyPolicy() {
        $data = [];
        $this->menu_id = 'PRIVACY_POLICY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRIVACY_POLICY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRIVACY_POLICY', 'VIEW');
        $data['privacy_policy_data'] = $this->Common_model->geAlldata('privacy_policy');
        $view = 'admin/footer/privacy_policy';
        $this->page_title = 'PRIVACY POLICY';
        $this->load_admin_view($view, $data);
    }

    public function addEditprivacyPolicy($encrypted_id = "") {
        $this->menu_id = 'PRIVACY_POLICY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['page_title'] = $this->input->post('title');
            $insert_data['page_desc'] = $this->input->post('page_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'id', $id, 'privacy_policy');
            }
            redirect('admin/Footer/privacyPolicy');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('PRIVACY_POLICY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['privacy_policy_data'] = $this->Common_model->getDataById2('privacy_policy', 'id', $id, 'Live');
                $data['background_data'] = $this->Common_model->GetBackgroundImage($data['privacy_policy_data']->page_id);
                $view = 'admin/footer/editPrivacy';
                $this->page_title = 'PRIVACY POLICY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('PRIVACY_POLICY', 'ADD');
                $this->_show_message("You cant insert new detail", "error");
                redirect('admin/Footer/privacyPolicy');
            }
        }
    }

    public function statistic() {
        $this->menu_id = 'STATISTIC';
        $data['statistic_data'] = $this->Common_model->getDataByIdStatus('tbl_statistic', 'del_status', 'Live');
        $id = $data['statistic_data']->statistic_id;
        if ($this->input->post()) {
            $insert_data['title_1'] = $this->input->post('title_1');
            $insert_data['desc_1'] = $this->input->post('desc_1');

            $insert_data['title_2'] = $this->input->post('title_2');
            $insert_data['desc_2'] = $this->input->post('desc_2');

            $insert_data['title_3'] = $this->input->post('title_3');
            $insert_data['desc_3'] = $this->input->post('desc_3');

            $insert_data['title_4'] = $this->input->post('title_4');
            $insert_data['desc_4'] = $this->input->post('desc_4');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');

            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'statistic_id', $id, 'tbl_statistic');
            }
            redirect('admin/Footer/statistic');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('STATISTIC', 'EDIT');
                $view = 'admin/footer/editStatistic';
                $this->page_title = 'STATISTIC';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('STATISTIC', 'ADD');
                $this->_show_message("You cant insert new contact detail", "error");
                redirect('admin/Footer');
            }
        }
    }

    public function getInTouchSetting($encrypted_id = "") {
        $this->menu_id = 'GET_IN_TOUCH';
        $data = [];
        $data['setting_data'] = $this->Common_model->getDataById2('tbl_get_in_touch_setting', 'del_status', 'Live');
        $id = $data['setting_data']->get_in_touch_id;
        if ($this->input->post()) {
            $insert_data['get_in_touch_title'] = $this->input->post('get_in_touch_title');
            $insert_data['get_in_touch_desc'] = $this->input->post('get_in_touch_desc');
            $insert_data['get_in_touch_sub_desc'] = $this->input->post('get_in_touch_sub_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['get_in_touch_image']['name']) && isset($_FILES['get_in_touch_image']['name'])) {
                if ($_FILES['get_in_touch_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('get_in_touch_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_get_in_touch_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $get_in_touch_image = $new_path . $image['file_name'];
                    }
                } else {
                    $get_in_touch_image = $this->input->post('hidden_get_in_touch_image');
                }
            } else {
                $get_in_touch_image = $this->input->post('hidden_get_in_touch_image');
            }
            $insert_data['get_in_touch_image'] = $get_in_touch_image;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'get_in_touch_id', $id, 'tbl_get_in_touch_setting');
            }
            redirect('admin/Footer/getInTouchSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('GET_IN_TOUCH', 'EDIT');
                $view = 'admin/footer/editGetInTouch';
                $this->page_title = 'GET IN TOUCH SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function OtherServiceSetting($encrypted_id = "") {
        $this->menu_id = 'OTHER_SERVICE';
        $data = [];
        $data['setting_data'] = $this->Common_model->getDataById2('tbl_other_service', 'del_status', 'Live');
        $id = $data['setting_data']->other_sevice_id;
        if ($this->input->post()) {
            $insert_data['other_sevice_title'] = $this->input->post('other_sevice_title');
            $insert_data['other_sevice_desc'] = $this->input->post('other_sevice_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['other_sevice_image']['name']) && isset($_FILES['other_sevice_image']['name'])) {
                if ($_FILES['other_sevice_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('other_sevice_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_other_sevice_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $other_sevice_image = $new_path . $image['file_name'];
                    }
                } else {
                    $other_sevice_image = $this->input->post('hidden_other_sevice_image');
                }
            } else {
                $other_sevice_image = $this->input->post('hidden_other_sevice_image');
            }
            $insert_data['other_sevice_image'] = $other_sevice_image;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'other_sevice_id ', $id, 'tbl_other_service');
            }
            redirect('admin/Footer/OtherServiceSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('OTHER_SERVICE', 'EDIT');
                $view = 'admin/footer/editOtherService';
                $this->page_title = 'OTHER SERVICE SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }
    public function hotDealSetting($encrypted_id = "") {
        $this->menu_id = 'HOT_DEAL';
        $data = [];
        $data['setting_data'] = $this->Common_model->getDataById2('tbl_hot_deal', 'del_status', 'Live');
        $id = $data['setting_data']->hot_deal_id;
        if ($this->input->post()) {
            $insert_data['hot_deal_main_title'] = $this->input->post('hot_deal_main_title');
            
            $insert_data['hot_deal_title'] = $this->input->post('hot_deal_title');
            $insert_data['hot_deal_desc'] = $this->input->post('hot_deal_desc');
            
            $insert_data['hot_deal_sub_title_1'] = $this->input->post('hot_deal_sub_title_1');
            $insert_data['hot_deal_sub_desc_1'] = $this->input->post('hot_deal_sub_desc_1');
            
            $insert_data['hot_deal_sub_title_2'] = $this->input->post('hot_deal_sub_title_2');
            $insert_data['hot_deal_sub_desc_2'] = $this->input->post('hot_deal_sub_desc_2');
            
            $insert_data['hot_deal_sub_title_3'] = $this->input->post('hot_deal_sub_title_3');
            $insert_data['hot_deal_sub_desc_3'] = $this->input->post('hot_deal_sub_desc_3');

            $new_path = 'assets/images/resources/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['hot_deal_image']['name']) && isset($_FILES['hot_deal_image']['name'])) {
                if ($_FILES['hot_deal_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('hot_deal_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_hot_deal_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $hot_deal_image = $new_path . $image['file_name'];
                    }
                } else {
                    $hot_deal_image = $this->input->post('hidden_hot_deal_image');
                }
            } else {
                $hot_deal_image = $this->input->post('hidden_hot_deal_image');
            }
            $insert_data['hot_deal_image'] = $hot_deal_image;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'hot_deal_id', $id, 'tbl_hot_deal');
            }
            redirect('admin/Footer/hotDealSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('HOT_DEAL', 'EDIT');
                $view = 'admin/footer/editHotDeal';
                $this->page_title = 'HOT DEAL SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

}
