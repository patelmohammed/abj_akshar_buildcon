<?php

class Project extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'DASHBOARD';
        $this->load->model('Dashboard_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $this->page_id = 'PROJECT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PROJECT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PROJECT', 'VIEW');
        $data['project_data'] = $this->Common_model->geAlldata('tbl_project');
        $view = 'admin/project/project';
        $this->page_title = 'PROJECT';
        $this->load_admin_view($view, $data);
    }

    public function addEditProject($encrypted_id = "") {
        $this->page_id = 'PROJECT';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['project_name'] = $this->input->post('project_name');
            $insert_data['project_location'] = $this->input->post('project_location');
            $insert_data['project_desc'] = $this->input->post('project_desc');
            $insert_data['project_video'] = $this->input->post('project_video');
            $insert_data['project_icon'] = $this->input->post('project_icon');
            $insert_data['main_desc'] = $this->input->post('main_desc');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $project_image_path = 'assets/images/project/';
            if (!is_dir($project_image_path)) {
                if (!mkdir($project_image_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['project_poster_image']['name']) && isset($_FILES['project_poster_image']['name'])) {
                if ($_FILES['project_poster_image']['name']) {
                    $config['upload_path'] = $project_image_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('project_poster_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Slider');
                    } else {
                        $old_image = $this->input->post('hidden_project_poster_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $project_image_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_project_poster_image');
                }
            } else {
                $image_url = $this->input->post('hidden_project_poster_image');
            }
            $insert_data['project_poster_image'] = $image_url;

            if (!empty($_FILES['project_widget']['name']) && isset($_FILES['project_widget']['name'])) {
                if ($_FILES['project_widget']['name']) {
                    $config['upload_path'] = $project_image_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('project_widget')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Slider');
                    } else {
                        $old_image = $this->input->post('hidden_project_widget');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $project_image_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_project_widget');
                }
            } else {
                $image_url = $this->input->post('hidden_project_widget');
            }
            $insert_data['project_widget'] = $image_url;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $id = $this->Common_model->insertInformation($insert_data, 'tbl_project');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'project_id ', $id, 'tbl_project');
                $this->Common_model->deleteRecord($id, 'tbl_project_image', 'ref_project_id');
            }

            $project_image = $_FILES['project_image'];
            $is_active_project_image = $this->input->post('is_active_project_image');
            $hidden_project_image = $this->input->post('hidden_project_image');

            if (isset($project_image) && !empty($project_image)) {
                foreach ($project_image['name'] as $key => $value) {
                    if (isset($value) && !empty($value)) {
                        $_FILES['project_image']['name'] = $project_image['name'][$key];
                        $_FILES['project_image']['type'] = $project_image['type'][$key];
                        $_FILES['project_image']['tmp_name'] = $project_image['tmp_name'][$key];
                        $_FILES['project_image']['error'] = $project_image['error'][$key];
                        $_FILES['project_image']['size'] = $project_image['size'][$key];

                        $config['upload_path'] = $project_image_path;
                        $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                        $config['max_size'] = "*";
                        $config['max_width'] = "*";
                        $config['max_height'] = "*";
                        $config['encrypt_name'] = FALSE;

                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('project_image')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->_show_message((isset($error['error']) && !empty($error['error']) ? $error['error'] : 'Somethig wrong'), "error");
                            redirect('admin/Project');
                        } else {
                            $image = $this->upload->data();
                            $project_image_insert = array();
                            $project_image_insert['file_path'] = $project_image_path . $image['file_name'];
                            $project_image_insert['is_active'] = (isset($is_active_project_image[$key]) && !empty($is_active_project_image[$key]) ? ($is_active_project_image[$key] == 'on' ? 1 : 0) : 0);
                            $project_image_info[] = $project_image_insert;
                        }
                    } else {
                        $project_image_insert = array();
                        $project_image_insert['is_active'] = (isset($is_active_project_image[$key]) && !empty($is_active_project_image[$key]) ? ($is_active_project_image[$key] == 'on' ? 1 : 0) : 0);
                        $project_image_insert['file_path'] = $hidden_project_image[$key];
                        $project_image_info[] = $project_image_insert;
                    }
                }
            } else {
                if (isset($hidden_project_image) && !empty($hidden_project_image)) {
                    foreach ($hidden_download_document as $key2 => $value2) {
                        $project_image_insert = array();
                        $project_image_insert['is_active'] = (isset($is_active_project_image[$key2]) && !empty($is_active_project_image[$key2]) ? ($is_active_project_image[$key2] == 'on' ? 1 : 0) : 0);
                        $project_image_insert['file_path'] = $value2;
                        $project_image_info[] = $project_image_insert;
                    }
                }
            }

            if (isset($project_image_info) && !empty($project_image_info)) {
                $this->Common_model->insertProjectImage($id, $project_image_info);
            }
            redirect('admin/Project');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('PROJECT', 'ADD');
                $data = [];
                $view = 'admin/project/addProject';
                $this->page_title = 'PROJECT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('PROJECT', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['project_data'] = $this->Common_model->getDataById2('tbl_project', 'project_id ', $id, 'Live');
                $data['project_data']->project_image = $this->Common_model->geAlldataById('tbl_project_image', 'del_status', 'Live', 'del_status = "Live"', 'ref_project_id', $data['project_data']->project_id);
                $view = 'admin/project/editProject';
                $this->page_title = 'PROJECT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkUserName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Dashboard_model->getUserName($category_id);
            $respone = $this->Dashboard_model->checkUserName($this->input->get('user_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Dashboard_model->checkUserName($this->input->get('user_name'));
            echo $response;
            die;
        }
    }

    public function deleteProject() {
        $this->Common_model->check_menu_access('PROJECT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('project_id ', $id);
            $this->db->update('tbl_project');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }
    
     public function deactiveProject() {
        $update_data = $data = array();
        $project_id = $this->input->post('project_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'project_id', $project_id, 'tbl_project', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Something went wrong.';
        }
        echo json_encode($data);
        die;
    }

    public function activeProject() {
        $update_data = $data = array();
        $project_id = $this->input->post('project_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'project_id', $project_id, 'tbl_project', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
