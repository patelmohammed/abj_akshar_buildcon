<?php

class Client extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'CLIENT';
        $this->load->model('Dashboard_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $this->page_id = 'CLIENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CLIENT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CLIENT', 'VIEW');
        $data['client_data'] = $this->Common_model->geAlldata('tbl_client');
        $view = 'admin/client/client';
        $this->page_title = 'CLIENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditClient($encrypted_id = "") {
        $this->page_id = 'CLIENT';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $new_path = 'assets/images/resources/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['client_logo']['name']) && isset($_FILES['client_logo']['name'])) {
                if ($_FILES['client_logo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('client_logo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Client');
                    } else {
                        $old_image = $this->input->post('hidden_client_logo');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $client_logo = $new_path . $image['file_name'];
                    }
                } else {
                    $client_logo = $this->input->post('hidden_client_logo');
                }
            } else {
                $client_logo = $this->input->post('hidden_client_logo');
            }
            $insert_data['client_logo'] = $client_logo;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_client');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'client_id ', $id, 'tbl_client');
            }
            redirect('admin/Client');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('CLIENT', 'ADD');
                $data = [];
                $view = 'admin/client/addClient';
                $this->page_title = 'CLIENT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CLIENT', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['client_data'] = $this->Common_model->getDataById2('tbl_client', 'client_id  ', $id, 'Live');
                $view = 'admin/client/editClient';
                $this->page_title = 'CLIENT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteClient() {
        $this->Common_model->check_menu_access('CLIENT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('client_id  ', $id);
            $this->db->update('tbl_client');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deactiveClient() {
        $update_data = $data = array();
        $client_id = $this->input->post('client_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'client_id', $client_id, 'tbl_client', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Something went wrong.';
        }
        echo json_encode($data);
        die;
    }

    public function activeClient() {
        $update_data = $data = array();
        $client_id = $this->input->post('client_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'client_id', $client_id, 'tbl_client', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
