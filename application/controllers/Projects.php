<?php

class Projects extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'PROJECTS';
//        $this->load->model('Common_model');
//        $this->load->model('About_model');
    }

    public function index() {
        $data = [];
        $view = 'projects';
        $data['project_data'] = $this->Common_model->geAlldataById('tbl_project', 'is_active', 1, 'del_status = "Live"');
        $data['client_data'] = $this->Common_model->geAlldataById('tbl_client', 'is_active', 1, 'del_status = "Live"');
        $data['get_in_touch_data'] = $this->Common_model->getDataByIdStatus('tbl_get_in_touch_setting', 'del_status', 'Live');
        $this->page_title = 'PROJECTS';
        $this->load_view($view, $data);
    }

    public function projectDetail($project_id) {
        $data = [];
        $view = 'projects_detail';
        $data['client_data'] = $this->Common_model->geAlldataById('tbl_client', 'is_active', 1, 'del_status = "Live"');
        $data['all_project_data'] = $this->Common_model->geAlldataById('tbl_project', 'is_active', 1, 'del_status = "Live"');
        $data['get_in_touch_data'] = $this->Common_model->getDataByIdStatus('tbl_get_in_touch_setting', 'del_status', 'Live');
        $data['project_data'] = $this->Common_model->getDataByIdStatus('tbl_project', 'project_id ', $project_id);
        $data['project_data']->project_image = $this->Common_model->geAlldataById('tbl_project_image', 'is_active', 1, 'del_status = "Live"', 'ref_project_id', $data['project_data']->project_id);
        $this->page_title = 'PROJECTS DETAIL';
        $this->load_view($view, $data);
    }

}
