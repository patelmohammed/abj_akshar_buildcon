<?php

class Home extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'HOME';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $view = 'home/index';
        $this->page_title = 'HOME';
        $data['slider_data'] = $this->Home_model->geSliderdata();
        $data['about_us_data'] = $this->Common_model->getDataByIdStatus('about_us', 'del_status', 'Live');
        $data['service_setting_data'] = $this->Common_model->getDataByIdStatus('tbl_service_setting', 'del_status', 'Live');
        $data['service_setting_data']->service_item = $this->Common_model->geAlldataById('tbl_service_item', 'is_active', 1, '');
        $data['statistic_data'] = $this->Common_model->getDataByIdStatus('tbl_statistic', 'del_status', 'Live');
        $data['testimonial_data'] = $this->Common_model->getDataByIdStatus('tbl_statistic', 'del_status', 'Live');
        $data['testimonial_data']->testimonial_item_data = $this->Common_model->geAlldataById('tbl_testimonial', 'is_active', 1, 'del_status = "Live"');
        $data['get_in_touch_data'] = $this->Common_model->getDataByIdStatus('tbl_get_in_touch_setting', 'del_status', 'Live');
        $data['other_service_data'] = $this->Common_model->getDataByIdStatus('tbl_other_service', 'del_status', 'Live');
        $data['faq_data'] = $this->Common_model->getDataByIdStatus('tbl_faq', 'del_status', 'Live');
        if (isset($data['faq_data']) && !empty($data['faq_data'])) {
            $data['faq_data']->faq_item_data = $this->Common_model->geAlldataById('tbl_faq_item', 'ref_faq_id', $data['faq_data']->faq_id, '', 'is_active', 1);
        }
        $data['client_data'] = $this->Common_model->geAlldataById('tbl_client', 'is_active', 1, 'del_status = "Live"');
        $data['project_data'] = $this->Common_model->geAlldataById('tbl_project', 'is_active', 1, 'del_status = "Live"');
        $data['hot_deal_data'] = $this->Common_model->getDataByIdStatus('tbl_hot_deal', 'del_status', 'Live');
        $data['team_data'] = $this->Common_model->getTeamDataClient();

        $this->load_view($view, $data);
    }

    public function newsletter() {
        $template_data = $this->Common_model->getDataById2('mail_template', 'template_for', 'newsletter', 'Live');
        if ($this->input->post()) {
            $newsletter_email = $this->input->post('newsletter_email');
            $response = $this->Common_model->chkUniqueData('newsletter', 'newsletter_email', $newsletter_email, 'Live');
            if ($response == TRUE) {
                $from_email = $smtp_setting->smtp_user;
                $this->email->from($from_email, COMPANY_NAME);
                $this->email->subject('Newsletter');
                $this->email->message($template_data->template);
                $this->email->to($newsletter_email);

                $insert_data['newsletter_email'] = $newsletter_email;
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                if ($this->email->send()) {

                    $this->email->from($from_email, COMPANY_NAME);
                    $this->email->subject('Newsletter');
                    $this->email->message($newsletter_email . ' just subscribed Sweet n Rush');
                    $admin_email = isset($template_data->mail_to) && !empty($template_data->mail_to) ? $template_data->mail_to : COMPANY_EMAIL;
                    $this->email->to($admin_email);
                    $this->email->send();

                    $id = $this->Common_model->insertInformation($insert_data, 'newsletter');
                }
                if (isset($id) && !empty($id)) {
                    $data['result'] = TRUE;
                } else {
                    $data['result'] = FALSE;
                }
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function privacy() {
        $data = [];
        $this->page_id = 'privacy';
        $view = 'privacy';
        $data['privacy_policy_data'] = $this->Common_model->getDataById2('privacy_policy', 'page_id', $view, 'Live');
//        $data['background_data'] = $this->Common_model->GetBackgroundImage($this->page_id);
        $this->page_title = 'PRIVACY';
        $this->load_view($view, $data);
    }

    public function termsCondition() {
        $data = [];
        $this->page_id = 'terms_condition';
        $view = 'privacy';
        $data['privacy_policy_data'] = $this->Common_model->getDataById2('privacy_policy', 'page_id', 'terms_condition', 'Live');
        $data['background_data'] = $this->Common_model->GetBackgroundImage($this->page_id);
        $this->page_title = 'TERMS';
        $this->load_view($view, $data);
    }

}
