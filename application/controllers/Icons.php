<?php

class Icons extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'ICON';
    }

    public function index() {
        $data = [];
        $view = 'icons';
        $this->page_title = 'ICONS';
        $this->load_view($view, $data);
    }

}
