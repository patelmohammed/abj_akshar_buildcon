<?php

class About extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'ABOUT';
//        $this->load->model('Common_model');
//        $this->load->model('About_model');
    }

    public function index() {
        $data = [];
        $view = 'about';
        $data['about_us_data'] = $this->Common_model->getDataByIdStatus('about_us', 'del_status', 'Live');
        $data['service_setting_data'] = $this->Common_model->getDataByIdStatus('tbl_service_setting', 'del_status', 'Live');
        $data['service_setting_data']->service_item = $this->Common_model->geAlldataById('tbl_service_item', 'is_active', 1, '');
        $data['testimonial_data'] = $this->Common_model->getDataByIdStatus('tbl_statistic', 'del_status', 'Live');
        $data['testimonial_data']->testimonial_item_data = $this->Common_model->geAlldataById('tbl_testimonial', 'is_active', 1, 'del_status = "Live"');
        $data['faq_data'] = $this->Common_model->getDataByIdStatus('tbl_faq', 'del_status', 'Live');
        if (isset($data['faq_data']) && !empty($data['faq_data'])) {
            $data['faq_data']->faq_item_data = $this->Common_model->geAlldataById('tbl_faq_item', 'ref_faq_id', $data['faq_data']->faq_id, '', 'is_active', 1);
        }
        $data['other_service_data'] = $this->Common_model->getDataByIdStatus('tbl_other_service', 'del_status', 'Live');
        $data['get_in_touch_data'] = $this->Common_model->getDataByIdStatus('tbl_get_in_touch_setting', 'del_status', 'Live');
        $data['client_data'] = $this->Common_model->geAlldataById('tbl_client', 'is_active', 1, 'del_status = "Live"');
        $this->page_title = 'ABOUT';
        $this->load_view($view, $data);
    }

}
