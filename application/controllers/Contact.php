<?php

class Contact extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'CONTACT';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $view = 'contact';
        $data['contact_setting_data'] = $this->Common_model->getDataByIdStatus('contact', 'del_status', 'Live');
        $data['client_data'] = $this->Common_model->geAlldataById('tbl_client', 'is_active', 1, 'del_status = "Live"');
        $data['get_in_touch_data'] = $this->Common_model->getDataByIdStatus('tbl_get_in_touch_setting', 'del_status', 'Live');
        $data['other_service_data'] = $this->Common_model->getDataByIdStatus('tbl_other_service', 'del_status', 'Live');
        $this->page_title = 'CONTACT';
        $this->load_view($view, $data);
    }

    public function contact_us() {
        $data = array();
        if ($this->input->post()) {
            $fname = $this->input->post('fname');
            $insert_data['contact_name'] = isset($fname) && !empty($fname) ? strip_tags($fname) : '';

            $email = $this->input->post('email');
            $insert_data['contact_email'] = isset($email) && !empty($email) ? $email : '';

            $phone = $this->input->post('phone');
            $insert_data['contact_phone'] = isset($phone) && !empty($phone) ? $phone : '';

            $contact_message = $this->input->post('contact_message');
            $insert_data['contact_message'] = isset($contact_message) && !empty($contact_message) ? strip_tags($contact_message) : '';

            $insert_data['InsUser'] = 1;
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');


            if (isset($email) && !empty($email)) {
                $data = array();
                $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
                $data['template'] = $this->Common_model->getEmailTemplate('contact_us_customer');
                $html_template = $this->load->view('email_template', $data, true);

                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                $this->email->from($from_email, $from_name);
                $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{person_name}');
                    $replace = array($insert_data['contact_name']);
                    $body = str_replace($word, $replace, $html_template);
                }
                $this->email->message($body);
                $this->email->to($insert_data['contact_email']);
                $this->email->send();
            }

            $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
            $data['template'] = $this->Common_model->getEmailTemplate('contact_us_admin');
            $html_template = $this->load->view('email_template', $data, true);

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
            $this->email->from($from_email, $from_name);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{contact_name}', '{contact_email}', '{contact_message}', '{contact_phone}');
                $replace = array($insert_data['contact_name'], $insert_data['contact_email'], $insert_data['contact_message'], $insert_data['contact_phone']);
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);

            $this->email->to($data['company_data']->email);
            $this->email->send();

            if (isset($phone) && !empty($phone)) {
                $data = array();
                $data['template'] = $this->Common_model->getSmsTemplate('contact_us_customer');
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{contact_name}', '{contact_phone}', '{contact_email}');
                    $replace = array($insert_data['contact_name'], $insert_data['contact_phone'], $insert_data['contact_email']);
                    $body = str_replace($word, $replace, $data['template']);
                    $this->Common_model->send_message($phone, $body);
                }
            }
            $id = $this->Common_model->insertInformation($insert_data, 'contact_us');
            if (isset($id) && !empty($id)) {
                $data['response'] = 'Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.';
                $data['result'] = TRUE;
            } else {
                $data['response'] = 'Something went wrong. Try again!';
                $data['result'] = FALSE;
            }
        } else {
            $data['response'] = 'Something went wrong. Try again!';
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function get_quote() {
        $data = array();
        if ($this->input->post()) {
            $recipient_name = $this->input->post('recipient_name');
            $insert_data['recipient_name'] = isset($recipient_name) && !empty($recipient_name) ? strip_tags($recipient_name) : '';

            $recipient_mobile = $this->input->post('recipient_mobile');
            $insert_data['recipient_mobile'] = isset($recipient_mobile) && !empty($recipient_mobile) ? $recipient_mobile : '';

            $recipient_email = $this->input->post('recipient_email');
            $insert_data['recipient_email'] = isset($recipient_email) && !empty($recipient_email) ? $recipient_email : '';

            $message_text = $this->input->post('message_text');
            $insert_data['message_text'] = isset($message_text) && !empty($message_text) ? strip_tags($message_text) : '';

            $insert_data['InsUser'] = 1;
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

            if (isset($recipient_email) && empty($recipient_email)) {
                $data = array();
                $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
                $data['template'] = $this->Common_model->getEmailTemplate('get_quote_customer');
                $html_template = $this->load->view('email_template', $data, true);

                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                $this->email->from($from_email, $from_name);
                $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{person_name}');
                    $replace = array($insert_data['recipient_name']);
                    $body = str_replace($word, $replace, $html_template);
                }
                $this->email->message($body);
                $this->email->to($insert_data['recipient_email']);

                $this->email->send();
            }

            $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
            $data['template'] = $this->Common_model->getEmailTemplate('get_quote_admin');
            $html_template = $this->load->view('email_template', $data, true);

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
            $this->email->from($from_email, $from_name);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{contact_name}', '{contact_email}', '{contact_message}', '{contact_phone}');
                $replace = array($insert_data['recipient_name'], $insert_data['recipient_email'], $insert_data['message_text'], $insert_data['recipient_mobile']);
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);

            $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');

            $this->email->to($data['company_data']->email);
            $this->email->send();

            $id = $this->Common_model->insertInformation($insert_data, 'tbl_get_quote');

            if (isset($recipient_mobile) && !empty($recipient_mobile)) {
                $data = array();
                $data['template'] = $this->Common_model->getSmsTemplate('get_quote_customer');
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{contact_name}', '{contact_phone}', '{contact_email}');
                    $replace = array($recipient_name, $recipient_mobile, $recipient_email);
                    $body = str_replace($word, $replace, $data['template']);
                    $this->Common_model->send_message($recipient_mobile, $body);
                }
            }

            if (isset($id) && !empty($id)) {
                $data['response'] = 'Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.';
                $data['result'] = TRUE;
            } else {
                $data['response'] = 'Something went wrong. Try again!';
                $data['result'] = FALSE;
            }
        } else {
            $data['response'] = 'Something went wrong. Try again!';
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
