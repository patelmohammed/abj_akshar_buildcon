<?php

class Team extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'TEAM';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $data['team_data'] = $this->Common_model->getTeamDataWithCategory();
        $data['testimonial_data'] = $this->Common_model->getDataByIdStatus('tbl_statistic', 'del_status', 'Live');
        $data['testimonial_data']->testimonial_item_data = $this->Common_model->geAlldataById('tbl_testimonial', 'is_active', 1, 'del_status = "Live"');
        $data['client_data'] = $this->Common_model->geAlldataById('tbl_client', 'is_active', 1, 'del_status = "Live"'); 
        $data['get_in_touch_data'] = $this->Common_model->getDataByIdStatus('tbl_get_in_touch_setting', 'del_status', 'Live');
        $view = 'team';
        $this->page_title = 'TEAM';
        $this->load_view($view, $data);
    }

}
