<?php

class Common_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertInformation($data, $table_name) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function updateInformation($data, $id, $table_name) {
        $this->db->where('id', $id);
        $this->db->update($table_name, $data);
    }

    public function updateInformation2($data, $column_name, $id, $table_name, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }

        if (!empty($status) && $status == 'Live') {
            $this->db->where('del_status', 'Live');
        }

        $this->db->where($column_name, $id);
        if (!empty($column_name_2) && !empty($id_2)) {
            $this->db->where($column_name_2, $id_2);
        }

        $this->db->update($table_name, $data);
        return true;
    }

    public function deleteStatusChange($id, $table_name, $column_name) {
        $this->db->set('del_status', "Deleted");
        $this->db->where($column_name, $id);
        $this->db->update($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecord($id, $table_name, $column_name, $condition = NULL) {
        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecordUpdateStatus($id, $table_name, $column_name, $condition = NULL, $status = NULL) {
        if (isset($status) && !empty($status)) {
            $set = explode('||', $status);
            $this->db->set($set[0], $set[1]);
        }

        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }

        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getDataById($table_id, $id) {
        $sql = "SELECT * FROM $table_id WHERE del_status = 'Live' AND id = '$id' ";
        return $this->db->query($sql)->row();
    }

    public function getDataById2($table_id, $column_name, $id, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($column_name_2) && !empty($column_name_2) && isset($id_2) && !empty($id_2)) {
            $sql .= " AND $column_name_2 = '$id_2' ";
        }
        if (isset($status) && !empty($status)) {
            if ($status == '1') {
                $sql .= " AND status = '1'";
            } elseif ($status == 'Live') {
                $sql .= " AND del_status = 'Live'";
            }
        }
        return $this->db->query($sql)->row();
    }

    public function getDataByIdStatus($table_id, $column_name, $id, $status = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status";
        }
        return $this->db->query($sql)->row();
    }

    public function geAlldata($table_name) {
        $sql = "SELECT * FROM $table_name WHERE del_status = 'Live'";
        return $this->db->query($sql)->result();
    }

    public function geAlldataById($table_name, $column_name_1 = NULL, $id_1 = NULL, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status ";
        } else {
            $sql .= " AND del_status = 'Live'";
        }

        if (isset($column_name_1) && !empty($column_name_1) && isset($id_1) && !empty($id_1)) {
            $sql .= " AND $column_name_1 = '$id_1' ";
        }
        if (isset($column_name_2) && !empty($column_name_2) && isset($id_2) && !empty($id_2)) {
            $sql .= " AND $column_name_2 = '$id_2' ";
        }
        return $this->db->query($sql)->result();
    }

    public function deleteInformation($column_name, $id, $table_name) {
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        return true;
    }

    public function getCompanyInformation() {
        $sql = "SELECT * FROM company_information";
        return $this->db->query($sql)->row();
    }

    public function generateNoWithCompanyCode($column_name, $table_name) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE status= '1' ";
        return $this->db->query($sql)->row()->Number;
    }

    public function getCompanyCode() {
        $sql = "SELECT company_code FROM company_information WHERE status = '1' LIMIT 1";
        return $this->db->query($sql)->row()->company_code;
    }

    public function chkUniqueCode($table_name, $column_name, $code, $status = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$code");
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }
        if (!empty($status) && $status == 'Live') {
            $this->db->where('status', 'Live');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    public function chkUniqueData($table_name, $column_name, $value, $status = '', $column_name_2 = '', $value_2 = '', $status_2 = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$value");
        if (isset($status) && !empty($status)) {
            $this->db->where('del_status', 'Live');
        }

        if (isset($column_name_2) && !empty($column_name_2) && isset($value_2) && !empty($value_2)) {
            $this->db->where("$column_name_2", "$value_2");
        }
        if (isset($status_2) && !empty($status_2)) {
            if (!empty($status_2) && $status_2 == 1) {
                $this->db->where('status', '1');
            } else if (!empty($status_2) && $status_2 == 'Live') {
                $this->db->where('status', 'Live');
            }
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    public function generateNoIndividual($column_name, $table_name, $primary_id, $status = NULL) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $where = explode('||', $status);
            if (isset($where[0]) && isset($where[1]) && !empty($where[1]) && !empty($where[1])) {
                $sql .= " AND $where[0] = '$where[1]'";
            }
        }
        if (isset($primary_id) && !empty($primary_id)) {
            $sql .= " AND ref_outlet_id = '$primary_id'";
        }
        return $this->db->query($sql)->row()->Number;
    }

    public function GetBackgroundImage($page_id) {
        $sql = "SELECT * FROM background_img WHERE page_id = '$page_id' AND del_status= 'Live' ";
        return $this->db->query($sql)->result();
    }

    public function getMailSmtpSetting() {
        $sql = "SELECT * FROM smtp_setting WHERE del_status = 'Live' LIMIT 1";
        return $this->db->query($sql)->row();
    }

    public function resizeImage($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 80) {
        list($origWidth, $origHeight, $type) = getimagesize($sourceImage);

        if ($type == 1) {
            header('Content-Type: image/gif');
            $image = imagecreatefromgif($sourceImage);
        } elseif ($type == 2) {
            header('Content-Type: image/jpeg');
            $image = imagecreatefromjpeg($sourceImage);
        } elseif ($type == 3) {
            header('Content-Type: image/png');
            $image = imagecreatefrompng($sourceImage);
        } else {
            header('Content-Type: image/x-ms-bmp');
            $image = imagecreatefromwbmp($sourceImage);
        }

        if ($maxWidth == 0) {
            $maxWidth = $origWidth;
        }

        if ($maxHeight == 0) {
            $maxHeight = $origHeight;
        }

// Calculate ratio of desired maximum sizes and original sizes.
        $widthRatio = $maxWidth / $origWidth;
        $heightRatio = $maxHeight / $origHeight;

// Ratio used for calculating new image dimensions.
        $ratio = min($widthRatio, $heightRatio);

// Calculate new image dimensions.
        $newWidth = (int) $origWidth * $ratio;
        $newHeight = (int) $origHeight * $ratio;

// Create final image with new dimensions.
// if($type==1 or $type==3)
// {
//    $newImage = imagefill($newImage,0,0,0x7fff0000);
// }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);

        $transparent = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
        imagefill($newImage, 0, 0, $transparent);
        imagesavealpha($newImage, true);


        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
        imagepng($newImage, $targetImage);

// Free up the memory.
        imagedestroy($image);
        imagedestroy($newImage);

        return true;
    }

    public function check_menu_access($constant, $type) {
        $data = array();
        $user_assigned_menus = $this->session->userdata('side_menu');
        $return = false;
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    if ($value->view_right >= 1 && $type == 'VIEW') {
                        $return = TRUE;
                    }
                    if ($value->add_right >= 1 && $type == 'ADD') {
                        $return = TRUE;
                    }
                    if ($value->edit_right >= 1 && $type == 'EDIT') {
                        $return = TRUE;
                    }
                    if ($value->delete_right >= 1 && $type == 'DELETE') {
                        $return = TRUE;
                    }
                }
            }
        }
        if (!$return) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(['res' => 'error', 'result' => 'error', 'msg' => 'Unauthorized access']);
                die;
            } else {
                redirect('admin/Auth/Unauthorized');
            }
        }
        return $return;
    }

    public function get_menu_rights($constant) {
        $user_assigned_menus = $this->session->userdata('side_menu');
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    return (array) $value;
                }
            }
        } else {
            redirect('admin/Auth/Unauthorized');
        }
    }

    public function getPageDataById() {
        $sql = "SELECT * 
                FROM";
    }

    public function getPageitemData($id, $is_active = NULL) {
        $sql = "SELECT * 
                FROM tbl_coaching_item_page tcip 
                WHERE tcip.ref_coaching_page_id = '$id' ";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND tcip.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function getCountryPageitemData($id, $is_active = NULL) {
        $sql = "SELECT * 
                FROM tbl_country_item_page tcip 
                WHERE tcip.ref_country_page_id = '$id' ";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND tcip.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function getVisaPageitemData($id, $is_active = NULL) {
        $sql = "SELECT * 
                FROM tbl_visa_item_page tcip 
                WHERE tcip.ref_visa_page_id = '$id' ";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND tcip.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function checkCoachingPage($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND coaching_page_name != '$currentName'" : '');
        $sql = "SELECT coaching_page_name FROM tbl_coaching_page 
                WHERE coaching_page_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCoachingPage($id) {
        $sql = "SELECT coaching_page_name FROM tbl_coaching_page
                WHERE del_status = 'Live'
                AND coaching_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->coaching_page_name;
    }

    public function checkCoachingPageShortName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND coaching_page_short_name != '$currentName'" : '');
        $sql = "SELECT coaching_page_short_name FROM tbl_coaching_page 
                WHERE coaching_page_short_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCoachingPageShortName($id) {
        $sql = "SELECT coaching_page_short_name FROM tbl_coaching_page
                WHERE del_status = 'Live'
                AND coaching_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->coaching_page_short_name;
    }

    public function checkCountryPage($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND country_page_name != '$currentName'" : '');
        $sql = "SELECT country_page_name FROM tbl_country_page 
                WHERE country_page_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCountryPage($id) {
        $sql = "SELECT country_page_name FROM tbl_country_page
                WHERE del_status = 'Live'
                AND country_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->country_page_name;
    }

    public function checkCountryPageShortName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND country_page_short_name != '$currentName'" : '');
        $sql = "SELECT country_page_short_name FROM tbl_country_page 
                WHERE country_page_short_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCountryPageShortName($id) {
        $sql = "SELECT country_page_short_name FROM tbl_country_page
                WHERE del_status = 'Live'
                AND country_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->country_page_short_name;
    }

    public function checkVisaPage($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND visa_page_name != '$currentName'" : '');
        $sql = "SELECT visa_page_name FROM tbl_visa_page 
                WHERE visa_page_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getVisaPage($id) {
        $sql = "SELECT visa_page_name FROM tbl_visa_page
                WHERE del_status = 'Live'
                AND visa_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->visa_page_name;
    }

    public function checkVisaPageShortName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND visa_page_short_name != '$currentName'" : '');
        $sql = "SELECT visa_page_short_name FROM tbl_visa_page 
                WHERE visa_page_short_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getVisaPageShortName($id) {
        $sql = "SELECT visa_page_short_name FROM tbl_visa_page
                WHERE del_status = 'Live'
                AND visa_page_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->visa_page_short_name;
    }

    public function getEmailTemplate($type) {
        $sql = "SELECT mt.* 
                FROM tbl_mail_template mt 
                WHERE mt.type = '$type' AND mt.del_status = 'Live' AND mt.active = 1 ";
        return $this->db->query($sql)->row();
    }

    public function getAllCountry() {
        $sql = "SELECT tcp.country_page_id, tcp.country_page_name, tcp.country_page_short_name, tcp.country_page_desc, tcp.home_page_desc, tcp.country_page_image, tcp.home_page_image, tcp.country_flag_image, tcp.background_image, tcp.is_active, tcp.is_show_home_page, tcp.del_status, '' AS continent_id, '' AS continent_name 
                FROM tbl_country_page tcp 
                WHERE tcp.del_status = 'Live' AND tcp.is_active = '1' AND tcp.continent_id IS NULL
                UNION(
                    SELECT '' AS country_page_id, '' AS country_page_name, '' AS country_page_short_name, '' AS country_page_desc, '' AS home_page_desc, '' AS country_page_image, '' AS home_page_image, '' AS country_flag_image, '' AS background_image, '' AS is_active, '' AS is_show_home_page, tc.del_status, tc.continent_id, tc.continent_name
                    FROM tbl_country_page tcp 
                    INNER JOIN tbl_continent tc ON tc.continent_id = tcp.continent_id AND tc.del_status = 'Live' 
                    WHERE tcp.del_status = 'Live' AND tcp.is_active = '1' AND tcp.continent_id IS NOT NULL
                    GROUP BY tcp.continent_id
                )";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->sub_country = $this->getSubCountry($value->continent_id);
            }
        }
        return $result;
    }

    public function getSubCountry($continent_id) {
        $sql = "SELECT * 
                FROM tbl_country_page tcp 
                WHERE tcp.del_status = 'Live' AND tcp.is_active = '1' AND tcp.continent_id = '$continent_id' ";
        return $this->db->query($sql)->result();
    }

    public function insertFaqItem($id) {
        $faq_item_title = $this->input->post('faq_item_title');
        $is_active_faq_item = $this->input->post('is_active_faq_item');
        $faq_item_desc = $this->input->post('faq_item_desc');
        if (isset($faq_item_title) && !empty($faq_item_title)) {
            foreach ($faq_item_title as $key => $value) {
                $insert_data['ref_faq_id'] = $id;
                $insert_data['faq_item_title'] = $value;
                $insert_data['faq_item_desc'] = isset($faq_item_desc[$key]) && !empty($faq_item_desc[$key]) ? $faq_item_desc[$key] : '';
                $insert_data['is_active'] = isset($is_active_faq_item[$key]) && !empty($is_active_faq_item[$key]) ? ( $is_active_faq_item[$key] == 'on' ? 1 : 0) : 0;

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'tbl_faq_item');
            }
        }
    }

    public function insertProjectImage($id, $project_image_info) {
        if (isset($project_image_info) && !empty($project_image_info)) {
            foreach ($project_image_info as $key => $value) {
                if ((isset($value['file_path']) && !empty($value['file_path']))) {
                    $insert_data['ref_project_id'] = $id;
                    $insert_data['project_image'] = $value['file_path'];
                    $insert_data['is_active'] = $value['is_active'];

                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                    $this->Common_model->insertInformation($insert_data, 'tbl_project_image');
                }
            }
        }
    }

    public function getSmsTemplate($type) {
        $sql = "SELECT mt.message 
                FROM tbl_sms_template mt 
                WHERE mt.type = '$type' AND mt.del_status = 'Live' ";
        return $this->db->query($sql)->row()->message;
    }

    public function send_message($contact_number, $message, $country_id = NULL) {
        if (isset($country_id) && !empty($country_id)) {
            $country_code = getCountryCode($country_id);
        } else {
            $country_code = '91';
        }
        $url = "https://api.msg91.com/api/sendhttp.php?authkey=341380A94flRwVU5f5b3930P1&mobiles=" . $contact_number . "&country=" . $country_code . "&message=" . $message . "&sender=WEBOIT&route=4";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: PHPSESSID=3kqe19dtvt86shurc2gu7vnh92"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function getTeamMemberData() {
        $sql = "SELECT tt.*,ttc.team_category_name 
                FROM tbl_team tt 
                INNER JOIN tbl_team_category ttc ON ttc.team_category_id = tt.ref_team_category_id AND ttc.is_active = 1 AND ttc.del_status = 'Live'
                WHERE tt.del_status = 'Live' 
                GROUP BY tt.team_id";
        return $this->db->query($sql)->result();
    }

    public function getTeamDataClient() {
        $sql = "SELECT tt.*,ttc.team_category_name 
                FROM tbl_team tt 
                INNER JOIN tbl_team_category ttc ON ttc.team_category_id = tt.ref_team_category_id AND ttc.is_active = 1 AND ttc.del_status = 'Live'
                WHERE tt.del_status = 'Live' AND tt.is_active = 1 
                GROUP BY tt.team_id";
        return $this->db->query($sql)->result();
    }

    public function getTeamDataWithCategory() {
        $sql = "SELECT * 
                FROM tbl_team_category ttc 
                WHERE ttc.del_status = 'Live' AND ttc.is_active = 1";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->team_member_data = $this->getTeamMemberDataByCategoryId($value->team_category_id);
            }
        }
        return $result;
    }

    public function getTeamMemberDataByCategoryId($id) {
        $sql = "SELECT tt.*
                FROM tbl_team tt 
                WHERE tt.del_status = 'Live' AND tt.is_active = 1 AND tt.ref_team_category_id = $id ";
        return $this->db->query($sql)->result();
    }

}
